-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "01/11/2024 09:08:50"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          DAM
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY DAM_vhd_vec_tst IS
END DAM_vhd_vec_tst;
ARCHITECTURE DAM_arch OF DAM_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL FRQ0 : STD_LOGIC;
SIGNAL FRQ1 : STD_LOGIC;
SIGNAL PHASE : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL RESET : STD_LOGIC;
COMPONENT DAM
	PORT (
	FRQ0 : IN STD_LOGIC;
	FRQ1 : IN STD_LOGIC;
	PHASE : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	RESET : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : DAM
	PORT MAP (
-- list connections between master ports and signals
	FRQ0 => FRQ0,
	FRQ1 => FRQ1,
	PHASE => PHASE,
	RESET => RESET
	);

-- FRQ0
t_prcs_FRQ0: PROCESS
BEGIN
LOOP
	FRQ0 <= '0';
	WAIT FOR 5000 ps;
	FRQ0 <= '1';
	WAIT FOR 5000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_FRQ0;

-- FRQ1
t_prcs_FRQ1: PROCESS
BEGIN
	FRQ1 <= '1';
	WAIT FOR 1000 ps;
	FOR i IN 1 TO 99
	LOOP
		FRQ1 <= '0';
		WAIT FOR 5000 ps;
		FRQ1 <= '1';
		WAIT FOR 5000 ps;
	END LOOP;
	FRQ1 <= '0';
	WAIT FOR 5000 ps;
	FRQ1 <= '1';
WAIT;
END PROCESS t_prcs_FRQ1;
END DAM_arch;
