-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.1 Build 917 02/14/2023 SC Lite Edition"

-- DATE "01/11/2024 09:08:53"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for Questa Intel FPGA (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	DAM IS
    PORT (
	RESET : OUT std_logic;
	FRQ0 : IN std_logic;
	PHASE : OUT std_logic_vector(31 DOWNTO 0);
	FRQ1 : IN std_logic
	);
END DAM;

-- Design Ports Information
-- RESET	=>  Location: PIN_N21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[31]	=>  Location: PIN_T18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[30]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[29]	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[28]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[27]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[26]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[25]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[24]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[23]	=>  Location: PIN_M22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[22]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[21]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[20]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[19]	=>  Location: PIN_R22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[18]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[17]	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[16]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[15]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[14]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[13]	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[12]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[11]	=>  Location: PIN_K22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[10]	=>  Location: PIN_T22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[9]	=>  Location: PIN_K17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[8]	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[7]	=>  Location: PIN_L17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[6]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[4]	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[3]	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[2]	=>  Location: PIN_P22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[1]	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PHASE[0]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- FRQ0	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- FRQ1	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF DAM IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_RESET : std_logic;
SIGNAL ww_FRQ0 : std_logic;
SIGNAL ww_PHASE : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_FRQ1 : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \FRQ0~input_o\ : std_logic;
SIGNAL \FRQ0~inputCLKENA0_outclk\ : std_logic;
SIGNAL \inst7|COUNT[4]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|COUNT~1_combout\ : std_logic;
SIGNAL \inst7|COUNT[2]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|COUNT~0_combout\ : std_logic;
SIGNAL \inst7|COUNT~3_combout\ : std_logic;
SIGNAL \inst7|COUNT~4_combout\ : std_logic;
SIGNAL \inst7|COUNT~2_combout\ : std_logic;
SIGNAL \inst7|COUNT[3]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|COUNT[1]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|DIVD~0_combout\ : std_logic;
SIGNAL \inst7|DIVD~q\ : std_logic;
SIGNAL \FRQ1~input_o\ : std_logic;
SIGNAL \inst21|TMPNEG1~0_combout\ : std_logic;
SIGNAL \inst21|TMPNEG1~q\ : std_logic;
SIGNAL \inst21|TMPNEG0~q\ : std_logic;
SIGNAL \inst2|TSUM[0][31]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][30]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][29]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][28]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][27]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][26]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][25]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][24]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][23]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][22]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][21]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][20]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][19]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][18]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][17]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][16]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][15]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][14]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][13]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][12]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][11]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][10]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][9]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][8]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][7]~combout\ : std_logic;
SIGNAL \inst2|TSUM[0][6]~combout\ : std_logic;
SIGNAL \inst2|PSUM[1][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][0]~7_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][0]~q\ : std_logic;
SIGNAL \inst2|Add16~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][1]~q\ : std_logic;
SIGNAL \inst2|Add16~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][2]~q\ : std_logic;
SIGNAL \inst2|Add16~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][3]~q\ : std_logic;
SIGNAL \inst2|Add16~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][4]~q\ : std_logic;
SIGNAL \inst2|Add16~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[1][5]~q\ : std_logic;
SIGNAL \inst2|PSUM[0][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][5]~8_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][0]~q\ : std_logic;
SIGNAL \inst2|Add17~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][1]~q\ : std_logic;
SIGNAL \inst2|Add17~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][2]~q\ : std_logic;
SIGNAL \inst2|Add17~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][3]~q\ : std_logic;
SIGNAL \inst2|Add17~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][4]~q\ : std_logic;
SIGNAL \inst2|Add17~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[0][5]~q\ : std_logic;
SIGNAL \inst2|Add0~22\ : std_logic;
SIGNAL \inst2|Add0~18\ : std_logic;
SIGNAL \inst2|Add0~14\ : std_logic;
SIGNAL \inst2|Add0~10\ : std_logic;
SIGNAL \inst2|Add0~6\ : std_logic;
SIGNAL \inst2|Add0~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[0][5]~combout\ : std_logic;
SIGNAL \inst2|PSUM[2][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][3]~6_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][0]~q\ : std_logic;
SIGNAL \inst2|Add15~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][1]~q\ : std_logic;
SIGNAL \inst2|Add15~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][2]~q\ : std_logic;
SIGNAL \inst2|Add15~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][3]~q\ : std_logic;
SIGNAL \inst2|Add15~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][4]~q\ : std_logic;
SIGNAL \inst2|Add15~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[2][5]~q\ : std_logic;
SIGNAL \inst2|Add0~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[0][4]~combout\ : std_logic;
SIGNAL \inst2|Add0~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[0][3]~combout\ : std_logic;
SIGNAL \inst2|Add0~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[0][2]~combout\ : std_logic;
SIGNAL \inst2|Add0~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[0][1]~combout\ : std_logic;
SIGNAL \inst2|Add0~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[0][0]~combout\ : std_logic;
SIGNAL \inst2|Add1~126\ : std_logic;
SIGNAL \inst2|Add1~122\ : std_logic;
SIGNAL \inst2|Add1~118\ : std_logic;
SIGNAL \inst2|Add1~114\ : std_logic;
SIGNAL \inst2|Add1~110\ : std_logic;
SIGNAL \inst2|Add1~106\ : std_logic;
SIGNAL \inst2|Add1~102\ : std_logic;
SIGNAL \inst2|Add1~98\ : std_logic;
SIGNAL \inst2|Add1~94\ : std_logic;
SIGNAL \inst2|Add1~90\ : std_logic;
SIGNAL \inst2|Add1~86\ : std_logic;
SIGNAL \inst2|Add1~82\ : std_logic;
SIGNAL \inst2|Add1~78\ : std_logic;
SIGNAL \inst2|Add1~74\ : std_logic;
SIGNAL \inst2|Add1~70\ : std_logic;
SIGNAL \inst2|Add1~66\ : std_logic;
SIGNAL \inst2|Add1~62\ : std_logic;
SIGNAL \inst2|Add1~58\ : std_logic;
SIGNAL \inst2|Add1~54\ : std_logic;
SIGNAL \inst2|Add1~50\ : std_logic;
SIGNAL \inst2|Add1~46\ : std_logic;
SIGNAL \inst2|Add1~42\ : std_logic;
SIGNAL \inst2|Add1~38\ : std_logic;
SIGNAL \inst2|Add1~34\ : std_logic;
SIGNAL \inst2|Add1~30\ : std_logic;
SIGNAL \inst2|Add1~26\ : std_logic;
SIGNAL \inst2|Add1~22\ : std_logic;
SIGNAL \inst2|Add1~18\ : std_logic;
SIGNAL \inst2|Add1~14\ : std_logic;
SIGNAL \inst2|Add1~10\ : std_logic;
SIGNAL \inst2|Add1~6\ : std_logic;
SIGNAL \inst2|Add1~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][31]~combout\ : std_logic;
SIGNAL \inst2|Add1~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][30]~combout\ : std_logic;
SIGNAL \inst2|Add1~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][29]~combout\ : std_logic;
SIGNAL \inst2|Add1~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][28]~combout\ : std_logic;
SIGNAL \inst2|Add1~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][27]~combout\ : std_logic;
SIGNAL \inst2|Add1~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][26]~combout\ : std_logic;
SIGNAL \inst2|Add1~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][25]~combout\ : std_logic;
SIGNAL \inst2|Add1~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][24]~combout\ : std_logic;
SIGNAL \inst2|Add1~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][23]~combout\ : std_logic;
SIGNAL \inst2|Add1~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][22]~combout\ : std_logic;
SIGNAL \inst2|Add1~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][21]~combout\ : std_logic;
SIGNAL \inst2|Add1~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][20]~combout\ : std_logic;
SIGNAL \inst2|Add1~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][19]~combout\ : std_logic;
SIGNAL \inst2|Add1~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][18]~combout\ : std_logic;
SIGNAL \inst2|Add1~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][17]~combout\ : std_logic;
SIGNAL \inst2|Add1~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][16]~combout\ : std_logic;
SIGNAL \inst2|Add1~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][15]~combout\ : std_logic;
SIGNAL \inst2|Add1~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][14]~combout\ : std_logic;
SIGNAL \inst2|Add1~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][13]~combout\ : std_logic;
SIGNAL \inst2|Add1~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][12]~combout\ : std_logic;
SIGNAL \inst2|Add1~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][11]~combout\ : std_logic;
SIGNAL \inst2|Add1~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][10]~combout\ : std_logic;
SIGNAL \inst2|Add1~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][9]~combout\ : std_logic;
SIGNAL \inst2|Add1~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][8]~combout\ : std_logic;
SIGNAL \inst2|Add1~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][7]~combout\ : std_logic;
SIGNAL \inst2|Add1~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][6]~combout\ : std_logic;
SIGNAL \inst2|Add1~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][5]~combout\ : std_logic;
SIGNAL \inst2|PSUM[3][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][5]~5_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][0]~q\ : std_logic;
SIGNAL \inst2|Add14~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][1]~q\ : std_logic;
SIGNAL \inst2|Add14~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][2]~q\ : std_logic;
SIGNAL \inst2|Add14~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][3]~q\ : std_logic;
SIGNAL \inst2|Add14~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][4]~q\ : std_logic;
SIGNAL \inst2|Add14~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[3][5]~q\ : std_logic;
SIGNAL \inst2|Add1~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][4]~combout\ : std_logic;
SIGNAL \inst2|Add1~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][3]~combout\ : std_logic;
SIGNAL \inst2|Add1~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][2]~combout\ : std_logic;
SIGNAL \inst2|Add1~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][1]~combout\ : std_logic;
SIGNAL \inst2|Add1~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[1][0]~combout\ : std_logic;
SIGNAL \inst2|Add2~126\ : std_logic;
SIGNAL \inst2|Add2~122\ : std_logic;
SIGNAL \inst2|Add2~118\ : std_logic;
SIGNAL \inst2|Add2~114\ : std_logic;
SIGNAL \inst2|Add2~110\ : std_logic;
SIGNAL \inst2|Add2~106\ : std_logic;
SIGNAL \inst2|Add2~102\ : std_logic;
SIGNAL \inst2|Add2~98\ : std_logic;
SIGNAL \inst2|Add2~94\ : std_logic;
SIGNAL \inst2|Add2~90\ : std_logic;
SIGNAL \inst2|Add2~86\ : std_logic;
SIGNAL \inst2|Add2~82\ : std_logic;
SIGNAL \inst2|Add2~78\ : std_logic;
SIGNAL \inst2|Add2~74\ : std_logic;
SIGNAL \inst2|Add2~70\ : std_logic;
SIGNAL \inst2|Add2~66\ : std_logic;
SIGNAL \inst2|Add2~62\ : std_logic;
SIGNAL \inst2|Add2~58\ : std_logic;
SIGNAL \inst2|Add2~54\ : std_logic;
SIGNAL \inst2|Add2~50\ : std_logic;
SIGNAL \inst2|Add2~46\ : std_logic;
SIGNAL \inst2|Add2~42\ : std_logic;
SIGNAL \inst2|Add2~38\ : std_logic;
SIGNAL \inst2|Add2~34\ : std_logic;
SIGNAL \inst2|Add2~30\ : std_logic;
SIGNAL \inst2|Add2~26\ : std_logic;
SIGNAL \inst2|Add2~22\ : std_logic;
SIGNAL \inst2|Add2~18\ : std_logic;
SIGNAL \inst2|Add2~14\ : std_logic;
SIGNAL \inst2|Add2~10\ : std_logic;
SIGNAL \inst2|Add2~6\ : std_logic;
SIGNAL \inst2|Add2~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][31]~combout\ : std_logic;
SIGNAL \inst2|Add2~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][30]~combout\ : std_logic;
SIGNAL \inst2|Add2~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][29]~combout\ : std_logic;
SIGNAL \inst2|Add2~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][28]~combout\ : std_logic;
SIGNAL \inst2|Add2~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][27]~combout\ : std_logic;
SIGNAL \inst2|Add2~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][26]~combout\ : std_logic;
SIGNAL \inst2|Add2~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][25]~combout\ : std_logic;
SIGNAL \inst2|Add2~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][24]~combout\ : std_logic;
SIGNAL \inst2|Add2~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][23]~combout\ : std_logic;
SIGNAL \inst2|Add2~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][22]~combout\ : std_logic;
SIGNAL \inst2|Add2~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][21]~combout\ : std_logic;
SIGNAL \inst2|Add2~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][20]~combout\ : std_logic;
SIGNAL \inst2|Add2~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][19]~combout\ : std_logic;
SIGNAL \inst2|Add2~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][18]~combout\ : std_logic;
SIGNAL \inst2|Add2~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][17]~combout\ : std_logic;
SIGNAL \inst2|Add2~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][16]~combout\ : std_logic;
SIGNAL \inst2|Add2~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][15]~combout\ : std_logic;
SIGNAL \inst2|Add2~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][14]~combout\ : std_logic;
SIGNAL \inst2|Add2~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][13]~combout\ : std_logic;
SIGNAL \inst2|Add2~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][12]~combout\ : std_logic;
SIGNAL \inst2|Add2~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][11]~combout\ : std_logic;
SIGNAL \inst2|Add2~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][10]~combout\ : std_logic;
SIGNAL \inst2|Add2~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][9]~combout\ : std_logic;
SIGNAL \inst2|Add2~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][8]~combout\ : std_logic;
SIGNAL \inst2|Add2~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][7]~combout\ : std_logic;
SIGNAL \inst2|Add2~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][6]~combout\ : std_logic;
SIGNAL \inst2|PSUM[4][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][5]~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][0]~q\ : std_logic;
SIGNAL \inst2|Add13~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][1]~q\ : std_logic;
SIGNAL \inst2|Add13~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][2]~q\ : std_logic;
SIGNAL \inst2|Add13~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][3]~q\ : std_logic;
SIGNAL \inst2|Add13~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][4]~q\ : std_logic;
SIGNAL \inst2|Add13~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[4][5]~q\ : std_logic;
SIGNAL \inst2|Add2~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][5]~combout\ : std_logic;
SIGNAL \inst2|Add2~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][4]~combout\ : std_logic;
SIGNAL \inst2|Add2~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][3]~combout\ : std_logic;
SIGNAL \inst2|Add2~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][2]~combout\ : std_logic;
SIGNAL \inst2|Add2~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][1]~combout\ : std_logic;
SIGNAL \inst2|Add2~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[2][0]~combout\ : std_logic;
SIGNAL \inst2|Add3~126\ : std_logic;
SIGNAL \inst2|Add3~122\ : std_logic;
SIGNAL \inst2|Add3~118\ : std_logic;
SIGNAL \inst2|Add3~114\ : std_logic;
SIGNAL \inst2|Add3~110\ : std_logic;
SIGNAL \inst2|Add3~106\ : std_logic;
SIGNAL \inst2|Add3~102\ : std_logic;
SIGNAL \inst2|Add3~98\ : std_logic;
SIGNAL \inst2|Add3~94\ : std_logic;
SIGNAL \inst2|Add3~90\ : std_logic;
SIGNAL \inst2|Add3~86\ : std_logic;
SIGNAL \inst2|Add3~82\ : std_logic;
SIGNAL \inst2|Add3~78\ : std_logic;
SIGNAL \inst2|Add3~74\ : std_logic;
SIGNAL \inst2|Add3~70\ : std_logic;
SIGNAL \inst2|Add3~66\ : std_logic;
SIGNAL \inst2|Add3~62\ : std_logic;
SIGNAL \inst2|Add3~58\ : std_logic;
SIGNAL \inst2|Add3~54\ : std_logic;
SIGNAL \inst2|Add3~50\ : std_logic;
SIGNAL \inst2|Add3~46\ : std_logic;
SIGNAL \inst2|Add3~42\ : std_logic;
SIGNAL \inst2|Add3~38\ : std_logic;
SIGNAL \inst2|Add3~34\ : std_logic;
SIGNAL \inst2|Add3~30\ : std_logic;
SIGNAL \inst2|Add3~26\ : std_logic;
SIGNAL \inst2|Add3~22\ : std_logic;
SIGNAL \inst2|Add3~18\ : std_logic;
SIGNAL \inst2|Add3~14\ : std_logic;
SIGNAL \inst2|Add3~10\ : std_logic;
SIGNAL \inst2|Add3~6\ : std_logic;
SIGNAL \inst2|Add3~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][31]~combout\ : std_logic;
SIGNAL \inst2|Add3~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][30]~combout\ : std_logic;
SIGNAL \inst2|Add3~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][29]~combout\ : std_logic;
SIGNAL \inst2|Add3~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][28]~combout\ : std_logic;
SIGNAL \inst2|Add3~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][27]~combout\ : std_logic;
SIGNAL \inst2|Add3~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][26]~combout\ : std_logic;
SIGNAL \inst2|Add3~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][25]~combout\ : std_logic;
SIGNAL \inst2|Add3~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][24]~combout\ : std_logic;
SIGNAL \inst2|Add3~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][23]~combout\ : std_logic;
SIGNAL \inst2|Add3~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][22]~combout\ : std_logic;
SIGNAL \inst2|Add3~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][21]~combout\ : std_logic;
SIGNAL \inst2|Add3~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][20]~combout\ : std_logic;
SIGNAL \inst2|Add3~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][19]~combout\ : std_logic;
SIGNAL \inst2|Add3~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][18]~combout\ : std_logic;
SIGNAL \inst2|Add3~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][17]~combout\ : std_logic;
SIGNAL \inst2|Add3~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][16]~combout\ : std_logic;
SIGNAL \inst2|Add3~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][15]~combout\ : std_logic;
SIGNAL \inst2|Add3~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][14]~combout\ : std_logic;
SIGNAL \inst2|Add3~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][13]~combout\ : std_logic;
SIGNAL \inst2|Add3~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][12]~combout\ : std_logic;
SIGNAL \inst2|Add3~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][11]~combout\ : std_logic;
SIGNAL \inst2|Add3~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][10]~combout\ : std_logic;
SIGNAL \inst2|Add3~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][9]~combout\ : std_logic;
SIGNAL \inst2|Add3~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][8]~combout\ : std_logic;
SIGNAL \inst2|Add3~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][7]~combout\ : std_logic;
SIGNAL \inst2|Add3~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][6]~combout\ : std_logic;
SIGNAL \inst2|Add3~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][5]~combout\ : std_logic;
SIGNAL \inst2|PSUM[5][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][1]~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][0]~q\ : std_logic;
SIGNAL \inst2|Add12~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][1]~q\ : std_logic;
SIGNAL \inst2|Add12~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][2]~q\ : std_logic;
SIGNAL \inst2|Add12~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][3]~q\ : std_logic;
SIGNAL \inst2|Add12~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][4]~q\ : std_logic;
SIGNAL \inst2|Add12~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[5][5]~q\ : std_logic;
SIGNAL \inst2|Add3~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][4]~combout\ : std_logic;
SIGNAL \inst2|Add3~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][3]~combout\ : std_logic;
SIGNAL \inst2|Add3~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][2]~combout\ : std_logic;
SIGNAL \inst2|Add3~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][1]~combout\ : std_logic;
SIGNAL \inst2|Add3~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[3][0]~combout\ : std_logic;
SIGNAL \inst2|Add4~126\ : std_logic;
SIGNAL \inst2|Add4~122\ : std_logic;
SIGNAL \inst2|Add4~118\ : std_logic;
SIGNAL \inst2|Add4~114\ : std_logic;
SIGNAL \inst2|Add4~110\ : std_logic;
SIGNAL \inst2|Add4~106\ : std_logic;
SIGNAL \inst2|Add4~102\ : std_logic;
SIGNAL \inst2|Add4~98\ : std_logic;
SIGNAL \inst2|Add4~94\ : std_logic;
SIGNAL \inst2|Add4~90\ : std_logic;
SIGNAL \inst2|Add4~86\ : std_logic;
SIGNAL \inst2|Add4~82\ : std_logic;
SIGNAL \inst2|Add4~78\ : std_logic;
SIGNAL \inst2|Add4~74\ : std_logic;
SIGNAL \inst2|Add4~70\ : std_logic;
SIGNAL \inst2|Add4~66\ : std_logic;
SIGNAL \inst2|Add4~62\ : std_logic;
SIGNAL \inst2|Add4~58\ : std_logic;
SIGNAL \inst2|Add4~54\ : std_logic;
SIGNAL \inst2|Add4~50\ : std_logic;
SIGNAL \inst2|Add4~46\ : std_logic;
SIGNAL \inst2|Add4~42\ : std_logic;
SIGNAL \inst2|Add4~38\ : std_logic;
SIGNAL \inst2|Add4~34\ : std_logic;
SIGNAL \inst2|Add4~30\ : std_logic;
SIGNAL \inst2|Add4~26\ : std_logic;
SIGNAL \inst2|Add4~22\ : std_logic;
SIGNAL \inst2|Add4~18\ : std_logic;
SIGNAL \inst2|Add4~14\ : std_logic;
SIGNAL \inst2|Add4~10\ : std_logic;
SIGNAL \inst2|Add4~6\ : std_logic;
SIGNAL \inst2|Add4~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][31]~combout\ : std_logic;
SIGNAL \inst2|Add4~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][30]~combout\ : std_logic;
SIGNAL \inst2|Add4~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][29]~combout\ : std_logic;
SIGNAL \inst2|Add4~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][28]~combout\ : std_logic;
SIGNAL \inst2|Add4~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][27]~combout\ : std_logic;
SIGNAL \inst2|Add4~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][26]~combout\ : std_logic;
SIGNAL \inst2|Add4~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][25]~combout\ : std_logic;
SIGNAL \inst2|Add4~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][24]~combout\ : std_logic;
SIGNAL \inst2|Add4~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][23]~combout\ : std_logic;
SIGNAL \inst2|Add4~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][22]~combout\ : std_logic;
SIGNAL \inst2|Add4~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][21]~combout\ : std_logic;
SIGNAL \inst2|Add4~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][20]~combout\ : std_logic;
SIGNAL \inst2|Add4~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][19]~combout\ : std_logic;
SIGNAL \inst2|Add4~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][18]~combout\ : std_logic;
SIGNAL \inst2|Add4~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][17]~combout\ : std_logic;
SIGNAL \inst2|Add4~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][16]~combout\ : std_logic;
SIGNAL \inst2|Add4~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][15]~combout\ : std_logic;
SIGNAL \inst2|Add4~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][14]~combout\ : std_logic;
SIGNAL \inst2|Add4~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][13]~combout\ : std_logic;
SIGNAL \inst2|Add4~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][12]~combout\ : std_logic;
SIGNAL \inst2|Add4~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][11]~combout\ : std_logic;
SIGNAL \inst2|Add4~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][10]~combout\ : std_logic;
SIGNAL \inst2|Add4~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][9]~combout\ : std_logic;
SIGNAL \inst2|Add4~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][8]~combout\ : std_logic;
SIGNAL \inst2|Add4~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][7]~combout\ : std_logic;
SIGNAL \inst2|Add4~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][6]~combout\ : std_logic;
SIGNAL \inst2|Add4~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][5]~combout\ : std_logic;
SIGNAL \inst2|PSUM[6][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][5]~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][0]~q\ : std_logic;
SIGNAL \inst2|Add11~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][1]~q\ : std_logic;
SIGNAL \inst2|Add11~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][2]~q\ : std_logic;
SIGNAL \inst2|Add11~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][3]~q\ : std_logic;
SIGNAL \inst2|Add11~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][4]~q\ : std_logic;
SIGNAL \inst2|Add11~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[6][5]~q\ : std_logic;
SIGNAL \inst2|Add4~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][4]~combout\ : std_logic;
SIGNAL \inst2|Add4~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][3]~combout\ : std_logic;
SIGNAL \inst2|Add4~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][2]~combout\ : std_logic;
SIGNAL \inst2|Add4~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][1]~combout\ : std_logic;
SIGNAL \inst2|Add4~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[4][0]~combout\ : std_logic;
SIGNAL \inst2|Add5~126\ : std_logic;
SIGNAL \inst2|Add5~122\ : std_logic;
SIGNAL \inst2|Add5~118\ : std_logic;
SIGNAL \inst2|Add5~114\ : std_logic;
SIGNAL \inst2|Add5~110\ : std_logic;
SIGNAL \inst2|Add5~106\ : std_logic;
SIGNAL \inst2|Add5~102\ : std_logic;
SIGNAL \inst2|Add5~98\ : std_logic;
SIGNAL \inst2|Add5~94\ : std_logic;
SIGNAL \inst2|Add5~90\ : std_logic;
SIGNAL \inst2|Add5~86\ : std_logic;
SIGNAL \inst2|Add5~82\ : std_logic;
SIGNAL \inst2|Add5~78\ : std_logic;
SIGNAL \inst2|Add5~74\ : std_logic;
SIGNAL \inst2|Add5~70\ : std_logic;
SIGNAL \inst2|Add5~66\ : std_logic;
SIGNAL \inst2|Add5~62\ : std_logic;
SIGNAL \inst2|Add5~58\ : std_logic;
SIGNAL \inst2|Add5~54\ : std_logic;
SIGNAL \inst2|Add5~50\ : std_logic;
SIGNAL \inst2|Add5~46\ : std_logic;
SIGNAL \inst2|Add5~42\ : std_logic;
SIGNAL \inst2|Add5~38\ : std_logic;
SIGNAL \inst2|Add5~34\ : std_logic;
SIGNAL \inst2|Add5~30\ : std_logic;
SIGNAL \inst2|Add5~26\ : std_logic;
SIGNAL \inst2|Add5~22\ : std_logic;
SIGNAL \inst2|Add5~18\ : std_logic;
SIGNAL \inst2|Add5~14\ : std_logic;
SIGNAL \inst2|Add5~10\ : std_logic;
SIGNAL \inst2|Add5~6\ : std_logic;
SIGNAL \inst2|Add5~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][31]~combout\ : std_logic;
SIGNAL \inst2|Add5~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][30]~combout\ : std_logic;
SIGNAL \inst2|Add5~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][29]~combout\ : std_logic;
SIGNAL \inst2|Add5~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][28]~combout\ : std_logic;
SIGNAL \inst2|Add5~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][27]~combout\ : std_logic;
SIGNAL \inst2|Add5~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][26]~combout\ : std_logic;
SIGNAL \inst2|Add5~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][25]~combout\ : std_logic;
SIGNAL \inst2|Add5~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][24]~combout\ : std_logic;
SIGNAL \inst2|Add5~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][23]~combout\ : std_logic;
SIGNAL \inst2|Add5~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][22]~combout\ : std_logic;
SIGNAL \inst2|Add5~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][21]~combout\ : std_logic;
SIGNAL \inst2|Add5~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][20]~combout\ : std_logic;
SIGNAL \inst2|Add5~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][19]~combout\ : std_logic;
SIGNAL \inst2|Add5~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][18]~combout\ : std_logic;
SIGNAL \inst2|Add5~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][17]~combout\ : std_logic;
SIGNAL \inst2|Add5~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][16]~combout\ : std_logic;
SIGNAL \inst2|Add5~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][15]~combout\ : std_logic;
SIGNAL \inst2|Add5~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][14]~combout\ : std_logic;
SIGNAL \inst2|Add5~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][13]~combout\ : std_logic;
SIGNAL \inst2|Add5~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][12]~combout\ : std_logic;
SIGNAL \inst2|Add5~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][11]~combout\ : std_logic;
SIGNAL \inst2|Add5~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][10]~combout\ : std_logic;
SIGNAL \inst2|Add5~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][9]~combout\ : std_logic;
SIGNAL \inst2|Add5~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][8]~combout\ : std_logic;
SIGNAL \inst2|Add5~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][7]~combout\ : std_logic;
SIGNAL \inst2|Add5~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][6]~combout\ : std_logic;
SIGNAL \inst2|PSUM[7][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][2]~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][0]~q\ : std_logic;
SIGNAL \inst2|Add10~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][1]~q\ : std_logic;
SIGNAL \inst2|Add10~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][2]~q\ : std_logic;
SIGNAL \inst2|Add10~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][3]~q\ : std_logic;
SIGNAL \inst2|Add10~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][4]~q\ : std_logic;
SIGNAL \inst2|Add10~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[7][5]~q\ : std_logic;
SIGNAL \inst2|Add5~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][5]~combout\ : std_logic;
SIGNAL \inst2|Add5~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][4]~combout\ : std_logic;
SIGNAL \inst2|Add5~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][3]~combout\ : std_logic;
SIGNAL \inst2|Add5~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][2]~combout\ : std_logic;
SIGNAL \inst2|Add5~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][1]~combout\ : std_logic;
SIGNAL \inst2|Add5~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[5][0]~combout\ : std_logic;
SIGNAL \inst2|Add6~126\ : std_logic;
SIGNAL \inst2|Add6~122\ : std_logic;
SIGNAL \inst2|Add6~118\ : std_logic;
SIGNAL \inst2|Add6~114\ : std_logic;
SIGNAL \inst2|Add6~110\ : std_logic;
SIGNAL \inst2|Add6~106\ : std_logic;
SIGNAL \inst2|Add6~102\ : std_logic;
SIGNAL \inst2|Add6~98\ : std_logic;
SIGNAL \inst2|Add6~94\ : std_logic;
SIGNAL \inst2|Add6~90\ : std_logic;
SIGNAL \inst2|Add6~86\ : std_logic;
SIGNAL \inst2|Add6~82\ : std_logic;
SIGNAL \inst2|Add6~78\ : std_logic;
SIGNAL \inst2|Add6~74\ : std_logic;
SIGNAL \inst2|Add6~70\ : std_logic;
SIGNAL \inst2|Add6~66\ : std_logic;
SIGNAL \inst2|Add6~62\ : std_logic;
SIGNAL \inst2|Add6~58\ : std_logic;
SIGNAL \inst2|Add6~54\ : std_logic;
SIGNAL \inst2|Add6~50\ : std_logic;
SIGNAL \inst2|Add6~46\ : std_logic;
SIGNAL \inst2|Add6~42\ : std_logic;
SIGNAL \inst2|Add6~38\ : std_logic;
SIGNAL \inst2|Add6~34\ : std_logic;
SIGNAL \inst2|Add6~30\ : std_logic;
SIGNAL \inst2|Add6~26\ : std_logic;
SIGNAL \inst2|Add6~22\ : std_logic;
SIGNAL \inst2|Add6~18\ : std_logic;
SIGNAL \inst2|Add6~14\ : std_logic;
SIGNAL \inst2|Add6~10\ : std_logic;
SIGNAL \inst2|Add6~6\ : std_logic;
SIGNAL \inst2|Add6~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][31]~combout\ : std_logic;
SIGNAL \inst2|Add6~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][30]~combout\ : std_logic;
SIGNAL \inst2|Add6~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][29]~combout\ : std_logic;
SIGNAL \inst2|Add6~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][28]~combout\ : std_logic;
SIGNAL \inst2|Add6~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][27]~combout\ : std_logic;
SIGNAL \inst2|Add6~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][26]~combout\ : std_logic;
SIGNAL \inst2|Add6~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][25]~combout\ : std_logic;
SIGNAL \inst2|Add6~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][24]~combout\ : std_logic;
SIGNAL \inst2|Add6~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][23]~combout\ : std_logic;
SIGNAL \inst2|Add6~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][22]~combout\ : std_logic;
SIGNAL \inst2|Add6~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][21]~combout\ : std_logic;
SIGNAL \inst2|Add6~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][20]~combout\ : std_logic;
SIGNAL \inst2|Add6~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][19]~combout\ : std_logic;
SIGNAL \inst2|Add6~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][18]~combout\ : std_logic;
SIGNAL \inst2|Add6~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][17]~combout\ : std_logic;
SIGNAL \inst2|Add6~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][16]~combout\ : std_logic;
SIGNAL \inst2|Add6~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][15]~combout\ : std_logic;
SIGNAL \inst2|Add6~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][14]~combout\ : std_logic;
SIGNAL \inst2|Add6~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][13]~combout\ : std_logic;
SIGNAL \inst2|Add6~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][12]~combout\ : std_logic;
SIGNAL \inst2|Add6~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][11]~combout\ : std_logic;
SIGNAL \inst2|Add6~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][10]~combout\ : std_logic;
SIGNAL \inst2|Add6~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][9]~combout\ : std_logic;
SIGNAL \inst2|Add6~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][8]~combout\ : std_logic;
SIGNAL \inst2|Add6~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][7]~combout\ : std_logic;
SIGNAL \inst2|Add6~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][6]~combout\ : std_logic;
SIGNAL \inst2|PSUM[8][0]~_wirecell_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][5]~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][0]~q\ : std_logic;
SIGNAL \inst2|Add9~4_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][1]~q\ : std_logic;
SIGNAL \inst2|Add9~3_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][2]~q\ : std_logic;
SIGNAL \inst2|Add9~2_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][3]~q\ : std_logic;
SIGNAL \inst2|Add9~1_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][4]~q\ : std_logic;
SIGNAL \inst2|Add9~0_combout\ : std_logic;
SIGNAL \inst2|PSUM[8][5]~q\ : std_logic;
SIGNAL \inst2|Add6~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][5]~combout\ : std_logic;
SIGNAL \inst2|Add6~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][4]~combout\ : std_logic;
SIGNAL \inst2|Add6~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][3]~combout\ : std_logic;
SIGNAL \inst2|Add6~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][2]~combout\ : std_logic;
SIGNAL \inst2|Add6~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][1]~combout\ : std_logic;
SIGNAL \inst2|Add6~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[6][0]~combout\ : std_logic;
SIGNAL \inst2|Add7~126\ : std_logic;
SIGNAL \inst2|Add7~122\ : std_logic;
SIGNAL \inst2|Add7~118\ : std_logic;
SIGNAL \inst2|Add7~114\ : std_logic;
SIGNAL \inst2|Add7~110\ : std_logic;
SIGNAL \inst2|Add7~106\ : std_logic;
SIGNAL \inst2|Add7~102\ : std_logic;
SIGNAL \inst2|Add7~98\ : std_logic;
SIGNAL \inst2|Add7~94\ : std_logic;
SIGNAL \inst2|Add7~90\ : std_logic;
SIGNAL \inst2|Add7~86\ : std_logic;
SIGNAL \inst2|Add7~82\ : std_logic;
SIGNAL \inst2|Add7~78\ : std_logic;
SIGNAL \inst2|Add7~74\ : std_logic;
SIGNAL \inst2|Add7~70\ : std_logic;
SIGNAL \inst2|Add7~66\ : std_logic;
SIGNAL \inst2|Add7~62\ : std_logic;
SIGNAL \inst2|Add7~58\ : std_logic;
SIGNAL \inst2|Add7~54\ : std_logic;
SIGNAL \inst2|Add7~50\ : std_logic;
SIGNAL \inst2|Add7~46\ : std_logic;
SIGNAL \inst2|Add7~42\ : std_logic;
SIGNAL \inst2|Add7~38\ : std_logic;
SIGNAL \inst2|Add7~34\ : std_logic;
SIGNAL \inst2|Add7~30\ : std_logic;
SIGNAL \inst2|Add7~26\ : std_logic;
SIGNAL \inst2|Add7~22\ : std_logic;
SIGNAL \inst2|Add7~18\ : std_logic;
SIGNAL \inst2|Add7~14\ : std_logic;
SIGNAL \inst2|Add7~10\ : std_logic;
SIGNAL \inst2|Add7~6\ : std_logic;
SIGNAL \inst2|Add7~1_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][31]~combout\ : std_logic;
SIGNAL \inst2|Add7~5_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][30]~combout\ : std_logic;
SIGNAL \inst2|Add7~9_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][29]~combout\ : std_logic;
SIGNAL \inst2|Add7~13_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][28]~combout\ : std_logic;
SIGNAL \inst2|Add7~17_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][27]~combout\ : std_logic;
SIGNAL \inst2|Add7~21_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][26]~combout\ : std_logic;
SIGNAL \inst2|Add7~25_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][25]~combout\ : std_logic;
SIGNAL \inst2|Add7~29_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][24]~combout\ : std_logic;
SIGNAL \inst2|Add7~33_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][23]~combout\ : std_logic;
SIGNAL \inst2|Add7~37_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][22]~combout\ : std_logic;
SIGNAL \inst2|Add7~41_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][21]~combout\ : std_logic;
SIGNAL \inst2|Add7~45_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][20]~combout\ : std_logic;
SIGNAL \inst2|Add7~49_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][19]~combout\ : std_logic;
SIGNAL \inst2|Add7~53_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][18]~combout\ : std_logic;
SIGNAL \inst2|Add7~57_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][17]~combout\ : std_logic;
SIGNAL \inst2|Add7~61_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][16]~combout\ : std_logic;
SIGNAL \inst2|Add7~65_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][15]~combout\ : std_logic;
SIGNAL \inst2|Add7~69_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][14]~combout\ : std_logic;
SIGNAL \inst2|Add7~73_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][13]~combout\ : std_logic;
SIGNAL \inst2|Add7~77_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][12]~combout\ : std_logic;
SIGNAL \inst2|Add7~81_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][11]~combout\ : std_logic;
SIGNAL \inst2|Add7~85_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][10]~combout\ : std_logic;
SIGNAL \inst2|Add7~89_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][9]~combout\ : std_logic;
SIGNAL \inst2|Add7~93_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][8]~combout\ : std_logic;
SIGNAL \inst2|Add7~97_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][7]~combout\ : std_logic;
SIGNAL \inst2|Add7~101_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][6]~combout\ : std_logic;
SIGNAL \inst2|Add7~105_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][5]~combout\ : std_logic;
SIGNAL \inst2|Add7~109_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][4]~combout\ : std_logic;
SIGNAL \inst2|Add7~113_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][3]~combout\ : std_logic;
SIGNAL \inst2|Add7~117_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][2]~combout\ : std_logic;
SIGNAL \inst2|Add7~121_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][1]~combout\ : std_logic;
SIGNAL \inst2|Add7~125_sumout\ : std_logic;
SIGNAL \inst2|TSUM[7][0]~combout\ : std_logic;
SIGNAL \inst2|Add8~126\ : std_logic;
SIGNAL \inst2|Add8~122\ : std_logic;
SIGNAL \inst2|Add8~118\ : std_logic;
SIGNAL \inst2|Add8~114\ : std_logic;
SIGNAL \inst2|Add8~110\ : std_logic;
SIGNAL \inst2|Add8~106\ : std_logic;
SIGNAL \inst2|Add8~102\ : std_logic;
SIGNAL \inst2|Add8~98\ : std_logic;
SIGNAL \inst2|Add8~94\ : std_logic;
SIGNAL \inst2|Add8~90\ : std_logic;
SIGNAL \inst2|Add8~86\ : std_logic;
SIGNAL \inst2|Add8~82\ : std_logic;
SIGNAL \inst2|Add8~78\ : std_logic;
SIGNAL \inst2|Add8~74\ : std_logic;
SIGNAL \inst2|Add8~70\ : std_logic;
SIGNAL \inst2|Add8~66\ : std_logic;
SIGNAL \inst2|Add8~62\ : std_logic;
SIGNAL \inst2|Add8~58\ : std_logic;
SIGNAL \inst2|Add8~54\ : std_logic;
SIGNAL \inst2|Add8~50\ : std_logic;
SIGNAL \inst2|Add8~46\ : std_logic;
SIGNAL \inst2|Add8~42\ : std_logic;
SIGNAL \inst2|Add8~38\ : std_logic;
SIGNAL \inst2|Add8~34\ : std_logic;
SIGNAL \inst2|Add8~30\ : std_logic;
SIGNAL \inst2|Add8~26\ : std_logic;
SIGNAL \inst2|Add8~22\ : std_logic;
SIGNAL \inst2|Add8~18\ : std_logic;
SIGNAL \inst2|Add8~14\ : std_logic;
SIGNAL \inst2|Add8~10\ : std_logic;
SIGNAL \inst2|Add8~6\ : std_logic;
SIGNAL \inst2|Add8~130_cout\ : std_logic;
SIGNAL \inst2|Add8~1_sumout\ : std_logic;
SIGNAL \inst2|ADD[31]~0_combout\ : std_logic;
SIGNAL \inst2|Add8~5_sumout\ : std_logic;
SIGNAL \inst2|ADD[30]~1_combout\ : std_logic;
SIGNAL \inst2|Add8~9_sumout\ : std_logic;
SIGNAL \inst2|ADD[29]~2_combout\ : std_logic;
SIGNAL \inst2|Add8~13_sumout\ : std_logic;
SIGNAL \inst2|ADD[28]~3_combout\ : std_logic;
SIGNAL \inst2|Add8~17_sumout\ : std_logic;
SIGNAL \inst2|ADD[27]~4_combout\ : std_logic;
SIGNAL \inst2|Add8~21_sumout\ : std_logic;
SIGNAL \inst2|ADD[26]~5_combout\ : std_logic;
SIGNAL \inst2|Add8~25_sumout\ : std_logic;
SIGNAL \inst2|ADD[25]~6_combout\ : std_logic;
SIGNAL \inst2|Add8~29_sumout\ : std_logic;
SIGNAL \inst2|ADD[24]~7_combout\ : std_logic;
SIGNAL \inst2|Add8~33_sumout\ : std_logic;
SIGNAL \inst2|ADD[23]~8_combout\ : std_logic;
SIGNAL \inst2|Add8~37_sumout\ : std_logic;
SIGNAL \inst2|ADD[22]~9_combout\ : std_logic;
SIGNAL \inst2|Add8~41_sumout\ : std_logic;
SIGNAL \inst2|ADD[21]~10_combout\ : std_logic;
SIGNAL \inst2|Add8~45_sumout\ : std_logic;
SIGNAL \inst2|ADD[20]~11_combout\ : std_logic;
SIGNAL \inst2|Add8~49_sumout\ : std_logic;
SIGNAL \inst2|ADD[19]~12_combout\ : std_logic;
SIGNAL \inst2|Add8~53_sumout\ : std_logic;
SIGNAL \inst2|ADD[18]~13_combout\ : std_logic;
SIGNAL \inst2|Add8~57_sumout\ : std_logic;
SIGNAL \inst2|ADD[17]~14_combout\ : std_logic;
SIGNAL \inst2|Add8~61_sumout\ : std_logic;
SIGNAL \inst2|ADD[16]~15_combout\ : std_logic;
SIGNAL \inst2|Add8~65_sumout\ : std_logic;
SIGNAL \inst2|ADD[15]~16_combout\ : std_logic;
SIGNAL \inst2|Add8~69_sumout\ : std_logic;
SIGNAL \inst2|ADD[14]~17_combout\ : std_logic;
SIGNAL \inst2|Add8~73_sumout\ : std_logic;
SIGNAL \inst2|ADD[13]~18_combout\ : std_logic;
SIGNAL \inst2|Add8~77_sumout\ : std_logic;
SIGNAL \inst2|ADD[12]~19_combout\ : std_logic;
SIGNAL \inst2|Add8~81_sumout\ : std_logic;
SIGNAL \inst2|ADD[11]~20_combout\ : std_logic;
SIGNAL \inst2|Add8~85_sumout\ : std_logic;
SIGNAL \inst2|ADD[10]~21_combout\ : std_logic;
SIGNAL \inst2|Add8~89_sumout\ : std_logic;
SIGNAL \inst2|ADD[9]~22_combout\ : std_logic;
SIGNAL \inst2|Add8~93_sumout\ : std_logic;
SIGNAL \inst2|ADD[8]~23_combout\ : std_logic;
SIGNAL \inst2|Add8~97_sumout\ : std_logic;
SIGNAL \inst2|ADD[7]~24_combout\ : std_logic;
SIGNAL \inst2|Add8~101_sumout\ : std_logic;
SIGNAL \inst2|ADD[6]~25_combout\ : std_logic;
SIGNAL \inst2|Add8~105_sumout\ : std_logic;
SIGNAL \inst2|ADD[5]~26_combout\ : std_logic;
SIGNAL \inst2|Add8~109_sumout\ : std_logic;
SIGNAL \inst2|ADD[4]~27_combout\ : std_logic;
SIGNAL \inst2|Add8~113_sumout\ : std_logic;
SIGNAL \inst2|ADD[3]~28_combout\ : std_logic;
SIGNAL \inst2|Add8~117_sumout\ : std_logic;
SIGNAL \inst2|ADD[2]~29_combout\ : std_logic;
SIGNAL \inst2|Add8~121_sumout\ : std_logic;
SIGNAL \inst2|ADD[1]~30_combout\ : std_logic;
SIGNAL \inst2|Add8~125_sumout\ : std_logic;
SIGNAL \inst2|ADD[0]~31_combout\ : std_logic;
SIGNAL \inst2|SUM\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst7|COUNT\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst6|STEMP\ : std_logic_vector(18 DOWNTO 0);
SIGNAL \inst7|ALT_INV_COUNT[4]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|ALT_INV_COUNT[3]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|ALT_INV_COUNT[2]~DUPLICATE_q\ : std_logic;
SIGNAL \inst7|ALT_INV_COUNT[1]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_FRQ1~input_o\ : std_logic;
SIGNAL \ALT_INV_FRQ0~input_o\ : std_logic;
SIGNAL \inst6|ALT_INV_STEMP\ : std_logic_vector(18 DOWNTO 0);
SIGNAL \inst2|ALT_INV_TSUM[0][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[0][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[1][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[2][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[3][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[4][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[5][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[6][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][0]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][1]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][2]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][3]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][4]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][5]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][6]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][7]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][8]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][9]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][10]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][11]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][12]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][13]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][14]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][15]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][16]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][17]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][18]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][19]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][20]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][21]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][22]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][23]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][24]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][25]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][26]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][27]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][28]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][29]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][30]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_TSUM[7][31]~combout\ : std_logic;
SIGNAL \inst2|ALT_INV_SUM\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst7|ALT_INV_COUNT\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \inst21|ALT_INV_TMPNEG0~q\ : std_logic;
SIGNAL \inst21|ALT_INV_TMPNEG1~q\ : std_logic;
SIGNAL \inst7|ALT_INV_DIVD~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[0][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[1][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[0][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[1][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[0][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[1][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[0][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[1][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[0][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[1][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[0][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[1][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[2][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[2][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[2][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[2][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[2][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[2][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add1~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[3][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[3][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[3][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[3][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[3][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[3][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add2~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[4][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[4][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[4][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[4][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[4][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[4][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add3~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[5][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[5][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[5][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[5][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[5][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[5][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add4~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[6][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[6][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[6][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[6][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[6][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[6][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add5~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[7][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[7][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[7][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[7][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[7][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[7][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add6~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[8][0]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[8][1]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[8][2]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[8][3]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[8][4]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_PSUM[8][5]~q\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add7~1_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~125_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~121_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~117_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~113_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~109_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~105_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~101_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~97_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~93_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~89_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~85_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~81_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~77_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~73_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~69_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~65_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~61_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~57_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~53_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~49_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~45_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~41_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~37_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~33_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~29_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~25_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~21_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~17_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~13_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~9_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~5_sumout\ : std_logic;
SIGNAL \inst2|ALT_INV_Add8~1_sumout\ : std_logic;

BEGIN

RESET <= ww_RESET;
ww_FRQ0 <= FRQ0;
PHASE <= ww_PHASE;
ww_FRQ1 <= FRQ1;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\inst7|ALT_INV_COUNT[4]~DUPLICATE_q\ <= NOT \inst7|COUNT[4]~DUPLICATE_q\;
\inst7|ALT_INV_COUNT[3]~DUPLICATE_q\ <= NOT \inst7|COUNT[3]~DUPLICATE_q\;
\inst7|ALT_INV_COUNT[2]~DUPLICATE_q\ <= NOT \inst7|COUNT[2]~DUPLICATE_q\;
\inst7|ALT_INV_COUNT[1]~DUPLICATE_q\ <= NOT \inst7|COUNT[1]~DUPLICATE_q\;
\ALT_INV_FRQ1~input_o\ <= NOT \FRQ1~input_o\;
\ALT_INV_FRQ0~input_o\ <= NOT \FRQ0~input_o\;
\inst6|ALT_INV_STEMP\(1) <= NOT \inst6|STEMP\(1);
\inst6|ALT_INV_STEMP\(3) <= NOT \inst6|STEMP\(3);
\inst6|ALT_INV_STEMP\(0) <= NOT \inst6|STEMP\(0);
\inst6|ALT_INV_STEMP\(2) <= NOT \inst6|STEMP\(2);
\inst6|ALT_INV_STEMP\(5) <= NOT \inst6|STEMP\(5);
\inst6|ALT_INV_STEMP\(4) <= NOT \inst6|STEMP\(4);
\inst2|ALT_INV_TSUM[0][0]~combout\ <= NOT \inst2|TSUM[0][0]~combout\;
\inst2|ALT_INV_TSUM[0][1]~combout\ <= NOT \inst2|TSUM[0][1]~combout\;
\inst2|ALT_INV_TSUM[0][2]~combout\ <= NOT \inst2|TSUM[0][2]~combout\;
\inst2|ALT_INV_TSUM[0][3]~combout\ <= NOT \inst2|TSUM[0][3]~combout\;
\inst2|ALT_INV_TSUM[0][4]~combout\ <= NOT \inst2|TSUM[0][4]~combout\;
\inst2|ALT_INV_TSUM[0][5]~combout\ <= NOT \inst2|TSUM[0][5]~combout\;
\inst6|ALT_INV_STEMP\(7) <= NOT \inst6|STEMP\(7);
\inst2|ALT_INV_TSUM[0][6]~combout\ <= NOT \inst2|TSUM[0][6]~combout\;
\inst2|ALT_INV_TSUM[0][7]~combout\ <= NOT \inst2|TSUM[0][7]~combout\;
\inst2|ALT_INV_TSUM[0][8]~combout\ <= NOT \inst2|TSUM[0][8]~combout\;
\inst2|ALT_INV_TSUM[0][9]~combout\ <= NOT \inst2|TSUM[0][9]~combout\;
\inst2|ALT_INV_TSUM[0][10]~combout\ <= NOT \inst2|TSUM[0][10]~combout\;
\inst2|ALT_INV_TSUM[0][11]~combout\ <= NOT \inst2|TSUM[0][11]~combout\;
\inst2|ALT_INV_TSUM[0][12]~combout\ <= NOT \inst2|TSUM[0][12]~combout\;
\inst2|ALT_INV_TSUM[0][13]~combout\ <= NOT \inst2|TSUM[0][13]~combout\;
\inst2|ALT_INV_TSUM[0][14]~combout\ <= NOT \inst2|TSUM[0][14]~combout\;
\inst2|ALT_INV_TSUM[0][15]~combout\ <= NOT \inst2|TSUM[0][15]~combout\;
\inst2|ALT_INV_TSUM[0][16]~combout\ <= NOT \inst2|TSUM[0][16]~combout\;
\inst2|ALT_INV_TSUM[0][17]~combout\ <= NOT \inst2|TSUM[0][17]~combout\;
\inst2|ALT_INV_TSUM[0][18]~combout\ <= NOT \inst2|TSUM[0][18]~combout\;
\inst2|ALT_INV_TSUM[0][19]~combout\ <= NOT \inst2|TSUM[0][19]~combout\;
\inst2|ALT_INV_TSUM[0][20]~combout\ <= NOT \inst2|TSUM[0][20]~combout\;
\inst2|ALT_INV_TSUM[0][21]~combout\ <= NOT \inst2|TSUM[0][21]~combout\;
\inst2|ALT_INV_TSUM[0][22]~combout\ <= NOT \inst2|TSUM[0][22]~combout\;
\inst2|ALT_INV_TSUM[0][23]~combout\ <= NOT \inst2|TSUM[0][23]~combout\;
\inst2|ALT_INV_TSUM[0][24]~combout\ <= NOT \inst2|TSUM[0][24]~combout\;
\inst2|ALT_INV_TSUM[0][25]~combout\ <= NOT \inst2|TSUM[0][25]~combout\;
\inst2|ALT_INV_TSUM[0][26]~combout\ <= NOT \inst2|TSUM[0][26]~combout\;
\inst2|ALT_INV_TSUM[0][27]~combout\ <= NOT \inst2|TSUM[0][27]~combout\;
\inst2|ALT_INV_TSUM[0][28]~combout\ <= NOT \inst2|TSUM[0][28]~combout\;
\inst2|ALT_INV_TSUM[0][29]~combout\ <= NOT \inst2|TSUM[0][29]~combout\;
\inst2|ALT_INV_TSUM[0][30]~combout\ <= NOT \inst2|TSUM[0][30]~combout\;
\inst2|ALT_INV_TSUM[0][31]~combout\ <= NOT \inst2|TSUM[0][31]~combout\;
\inst6|ALT_INV_STEMP\(6) <= NOT \inst6|STEMP\(6);
\inst2|ALT_INV_TSUM[1][0]~combout\ <= NOT \inst2|TSUM[1][0]~combout\;
\inst2|ALT_INV_TSUM[1][1]~combout\ <= NOT \inst2|TSUM[1][1]~combout\;
\inst2|ALT_INV_TSUM[1][2]~combout\ <= NOT \inst2|TSUM[1][2]~combout\;
\inst2|ALT_INV_TSUM[1][3]~combout\ <= NOT \inst2|TSUM[1][3]~combout\;
\inst2|ALT_INV_TSUM[1][4]~combout\ <= NOT \inst2|TSUM[1][4]~combout\;
\inst2|ALT_INV_TSUM[1][5]~combout\ <= NOT \inst2|TSUM[1][5]~combout\;
\inst6|ALT_INV_STEMP\(9) <= NOT \inst6|STEMP\(9);
\inst2|ALT_INV_TSUM[1][6]~combout\ <= NOT \inst2|TSUM[1][6]~combout\;
\inst2|ALT_INV_TSUM[1][7]~combout\ <= NOT \inst2|TSUM[1][7]~combout\;
\inst2|ALT_INV_TSUM[1][8]~combout\ <= NOT \inst2|TSUM[1][8]~combout\;
\inst2|ALT_INV_TSUM[1][9]~combout\ <= NOT \inst2|TSUM[1][9]~combout\;
\inst2|ALT_INV_TSUM[1][10]~combout\ <= NOT \inst2|TSUM[1][10]~combout\;
\inst2|ALT_INV_TSUM[1][11]~combout\ <= NOT \inst2|TSUM[1][11]~combout\;
\inst2|ALT_INV_TSUM[1][12]~combout\ <= NOT \inst2|TSUM[1][12]~combout\;
\inst2|ALT_INV_TSUM[1][13]~combout\ <= NOT \inst2|TSUM[1][13]~combout\;
\inst2|ALT_INV_TSUM[1][14]~combout\ <= NOT \inst2|TSUM[1][14]~combout\;
\inst2|ALT_INV_TSUM[1][15]~combout\ <= NOT \inst2|TSUM[1][15]~combout\;
\inst2|ALT_INV_TSUM[1][16]~combout\ <= NOT \inst2|TSUM[1][16]~combout\;
\inst2|ALT_INV_TSUM[1][17]~combout\ <= NOT \inst2|TSUM[1][17]~combout\;
\inst2|ALT_INV_TSUM[1][18]~combout\ <= NOT \inst2|TSUM[1][18]~combout\;
\inst2|ALT_INV_TSUM[1][19]~combout\ <= NOT \inst2|TSUM[1][19]~combout\;
\inst2|ALT_INV_TSUM[1][20]~combout\ <= NOT \inst2|TSUM[1][20]~combout\;
\inst2|ALT_INV_TSUM[1][21]~combout\ <= NOT \inst2|TSUM[1][21]~combout\;
\inst2|ALT_INV_TSUM[1][22]~combout\ <= NOT \inst2|TSUM[1][22]~combout\;
\inst2|ALT_INV_TSUM[1][23]~combout\ <= NOT \inst2|TSUM[1][23]~combout\;
\inst2|ALT_INV_TSUM[1][24]~combout\ <= NOT \inst2|TSUM[1][24]~combout\;
\inst2|ALT_INV_TSUM[1][25]~combout\ <= NOT \inst2|TSUM[1][25]~combout\;
\inst2|ALT_INV_TSUM[1][26]~combout\ <= NOT \inst2|TSUM[1][26]~combout\;
\inst2|ALT_INV_TSUM[1][27]~combout\ <= NOT \inst2|TSUM[1][27]~combout\;
\inst2|ALT_INV_TSUM[1][28]~combout\ <= NOT \inst2|TSUM[1][28]~combout\;
\inst2|ALT_INV_TSUM[1][29]~combout\ <= NOT \inst2|TSUM[1][29]~combout\;
\inst2|ALT_INV_TSUM[1][30]~combout\ <= NOT \inst2|TSUM[1][30]~combout\;
\inst2|ALT_INV_TSUM[1][31]~combout\ <= NOT \inst2|TSUM[1][31]~combout\;
\inst6|ALT_INV_STEMP\(8) <= NOT \inst6|STEMP\(8);
\inst2|ALT_INV_TSUM[2][0]~combout\ <= NOT \inst2|TSUM[2][0]~combout\;
\inst2|ALT_INV_TSUM[2][1]~combout\ <= NOT \inst2|TSUM[2][1]~combout\;
\inst2|ALT_INV_TSUM[2][2]~combout\ <= NOT \inst2|TSUM[2][2]~combout\;
\inst2|ALT_INV_TSUM[2][3]~combout\ <= NOT \inst2|TSUM[2][3]~combout\;
\inst2|ALT_INV_TSUM[2][4]~combout\ <= NOT \inst2|TSUM[2][4]~combout\;
\inst2|ALT_INV_TSUM[2][5]~combout\ <= NOT \inst2|TSUM[2][5]~combout\;
\inst6|ALT_INV_STEMP\(11) <= NOT \inst6|STEMP\(11);
\inst2|ALT_INV_TSUM[2][6]~combout\ <= NOT \inst2|TSUM[2][6]~combout\;
\inst2|ALT_INV_TSUM[2][7]~combout\ <= NOT \inst2|TSUM[2][7]~combout\;
\inst2|ALT_INV_TSUM[2][8]~combout\ <= NOT \inst2|TSUM[2][8]~combout\;
\inst2|ALT_INV_TSUM[2][9]~combout\ <= NOT \inst2|TSUM[2][9]~combout\;
\inst2|ALT_INV_TSUM[2][10]~combout\ <= NOT \inst2|TSUM[2][10]~combout\;
\inst2|ALT_INV_TSUM[2][11]~combout\ <= NOT \inst2|TSUM[2][11]~combout\;
\inst2|ALT_INV_TSUM[2][12]~combout\ <= NOT \inst2|TSUM[2][12]~combout\;
\inst2|ALT_INV_TSUM[2][13]~combout\ <= NOT \inst2|TSUM[2][13]~combout\;
\inst2|ALT_INV_TSUM[2][14]~combout\ <= NOT \inst2|TSUM[2][14]~combout\;
\inst2|ALT_INV_TSUM[2][15]~combout\ <= NOT \inst2|TSUM[2][15]~combout\;
\inst2|ALT_INV_TSUM[2][16]~combout\ <= NOT \inst2|TSUM[2][16]~combout\;
\inst2|ALT_INV_TSUM[2][17]~combout\ <= NOT \inst2|TSUM[2][17]~combout\;
\inst2|ALT_INV_TSUM[2][18]~combout\ <= NOT \inst2|TSUM[2][18]~combout\;
\inst2|ALT_INV_TSUM[2][19]~combout\ <= NOT \inst2|TSUM[2][19]~combout\;
\inst2|ALT_INV_TSUM[2][20]~combout\ <= NOT \inst2|TSUM[2][20]~combout\;
\inst2|ALT_INV_TSUM[2][21]~combout\ <= NOT \inst2|TSUM[2][21]~combout\;
\inst2|ALT_INV_TSUM[2][22]~combout\ <= NOT \inst2|TSUM[2][22]~combout\;
\inst2|ALT_INV_TSUM[2][23]~combout\ <= NOT \inst2|TSUM[2][23]~combout\;
\inst2|ALT_INV_TSUM[2][24]~combout\ <= NOT \inst2|TSUM[2][24]~combout\;
\inst2|ALT_INV_TSUM[2][25]~combout\ <= NOT \inst2|TSUM[2][25]~combout\;
\inst2|ALT_INV_TSUM[2][26]~combout\ <= NOT \inst2|TSUM[2][26]~combout\;
\inst2|ALT_INV_TSUM[2][27]~combout\ <= NOT \inst2|TSUM[2][27]~combout\;
\inst2|ALT_INV_TSUM[2][28]~combout\ <= NOT \inst2|TSUM[2][28]~combout\;
\inst2|ALT_INV_TSUM[2][29]~combout\ <= NOT \inst2|TSUM[2][29]~combout\;
\inst2|ALT_INV_TSUM[2][30]~combout\ <= NOT \inst2|TSUM[2][30]~combout\;
\inst2|ALT_INV_TSUM[2][31]~combout\ <= NOT \inst2|TSUM[2][31]~combout\;
\inst6|ALT_INV_STEMP\(10) <= NOT \inst6|STEMP\(10);
\inst2|ALT_INV_TSUM[3][0]~combout\ <= NOT \inst2|TSUM[3][0]~combout\;
\inst2|ALT_INV_TSUM[3][1]~combout\ <= NOT \inst2|TSUM[3][1]~combout\;
\inst2|ALT_INV_TSUM[3][2]~combout\ <= NOT \inst2|TSUM[3][2]~combout\;
\inst2|ALT_INV_TSUM[3][3]~combout\ <= NOT \inst2|TSUM[3][3]~combout\;
\inst2|ALT_INV_TSUM[3][4]~combout\ <= NOT \inst2|TSUM[3][4]~combout\;
\inst2|ALT_INV_TSUM[3][5]~combout\ <= NOT \inst2|TSUM[3][5]~combout\;
\inst6|ALT_INV_STEMP\(13) <= NOT \inst6|STEMP\(13);
\inst2|ALT_INV_TSUM[3][6]~combout\ <= NOT \inst2|TSUM[3][6]~combout\;
\inst2|ALT_INV_TSUM[3][7]~combout\ <= NOT \inst2|TSUM[3][7]~combout\;
\inst2|ALT_INV_TSUM[3][8]~combout\ <= NOT \inst2|TSUM[3][8]~combout\;
\inst2|ALT_INV_TSUM[3][9]~combout\ <= NOT \inst2|TSUM[3][9]~combout\;
\inst2|ALT_INV_TSUM[3][10]~combout\ <= NOT \inst2|TSUM[3][10]~combout\;
\inst2|ALT_INV_TSUM[3][11]~combout\ <= NOT \inst2|TSUM[3][11]~combout\;
\inst2|ALT_INV_TSUM[3][12]~combout\ <= NOT \inst2|TSUM[3][12]~combout\;
\inst2|ALT_INV_TSUM[3][13]~combout\ <= NOT \inst2|TSUM[3][13]~combout\;
\inst2|ALT_INV_TSUM[3][14]~combout\ <= NOT \inst2|TSUM[3][14]~combout\;
\inst2|ALT_INV_TSUM[3][15]~combout\ <= NOT \inst2|TSUM[3][15]~combout\;
\inst2|ALT_INV_TSUM[3][16]~combout\ <= NOT \inst2|TSUM[3][16]~combout\;
\inst2|ALT_INV_TSUM[3][17]~combout\ <= NOT \inst2|TSUM[3][17]~combout\;
\inst2|ALT_INV_TSUM[3][18]~combout\ <= NOT \inst2|TSUM[3][18]~combout\;
\inst2|ALT_INV_TSUM[3][19]~combout\ <= NOT \inst2|TSUM[3][19]~combout\;
\inst2|ALT_INV_TSUM[3][20]~combout\ <= NOT \inst2|TSUM[3][20]~combout\;
\inst2|ALT_INV_TSUM[3][21]~combout\ <= NOT \inst2|TSUM[3][21]~combout\;
\inst2|ALT_INV_TSUM[3][22]~combout\ <= NOT \inst2|TSUM[3][22]~combout\;
\inst2|ALT_INV_TSUM[3][23]~combout\ <= NOT \inst2|TSUM[3][23]~combout\;
\inst2|ALT_INV_TSUM[3][24]~combout\ <= NOT \inst2|TSUM[3][24]~combout\;
\inst2|ALT_INV_TSUM[3][25]~combout\ <= NOT \inst2|TSUM[3][25]~combout\;
\inst2|ALT_INV_TSUM[3][26]~combout\ <= NOT \inst2|TSUM[3][26]~combout\;
\inst2|ALT_INV_TSUM[3][27]~combout\ <= NOT \inst2|TSUM[3][27]~combout\;
\inst2|ALT_INV_TSUM[3][28]~combout\ <= NOT \inst2|TSUM[3][28]~combout\;
\inst2|ALT_INV_TSUM[3][29]~combout\ <= NOT \inst2|TSUM[3][29]~combout\;
\inst2|ALT_INV_TSUM[3][30]~combout\ <= NOT \inst2|TSUM[3][30]~combout\;
\inst2|ALT_INV_TSUM[3][31]~combout\ <= NOT \inst2|TSUM[3][31]~combout\;
\inst6|ALT_INV_STEMP\(12) <= NOT \inst6|STEMP\(12);
\inst2|ALT_INV_TSUM[4][0]~combout\ <= NOT \inst2|TSUM[4][0]~combout\;
\inst2|ALT_INV_TSUM[4][1]~combout\ <= NOT \inst2|TSUM[4][1]~combout\;
\inst2|ALT_INV_TSUM[4][2]~combout\ <= NOT \inst2|TSUM[4][2]~combout\;
\inst2|ALT_INV_TSUM[4][3]~combout\ <= NOT \inst2|TSUM[4][3]~combout\;
\inst2|ALT_INV_TSUM[4][4]~combout\ <= NOT \inst2|TSUM[4][4]~combout\;
\inst2|ALT_INV_TSUM[4][5]~combout\ <= NOT \inst2|TSUM[4][5]~combout\;
\inst6|ALT_INV_STEMP\(15) <= NOT \inst6|STEMP\(15);
\inst2|ALT_INV_TSUM[4][6]~combout\ <= NOT \inst2|TSUM[4][6]~combout\;
\inst2|ALT_INV_TSUM[4][7]~combout\ <= NOT \inst2|TSUM[4][7]~combout\;
\inst2|ALT_INV_TSUM[4][8]~combout\ <= NOT \inst2|TSUM[4][8]~combout\;
\inst2|ALT_INV_TSUM[4][9]~combout\ <= NOT \inst2|TSUM[4][9]~combout\;
\inst2|ALT_INV_TSUM[4][10]~combout\ <= NOT \inst2|TSUM[4][10]~combout\;
\inst2|ALT_INV_TSUM[4][11]~combout\ <= NOT \inst2|TSUM[4][11]~combout\;
\inst2|ALT_INV_TSUM[4][12]~combout\ <= NOT \inst2|TSUM[4][12]~combout\;
\inst2|ALT_INV_TSUM[4][13]~combout\ <= NOT \inst2|TSUM[4][13]~combout\;
\inst2|ALT_INV_TSUM[4][14]~combout\ <= NOT \inst2|TSUM[4][14]~combout\;
\inst2|ALT_INV_TSUM[4][15]~combout\ <= NOT \inst2|TSUM[4][15]~combout\;
\inst2|ALT_INV_TSUM[4][16]~combout\ <= NOT \inst2|TSUM[4][16]~combout\;
\inst2|ALT_INV_TSUM[4][17]~combout\ <= NOT \inst2|TSUM[4][17]~combout\;
\inst2|ALT_INV_TSUM[4][18]~combout\ <= NOT \inst2|TSUM[4][18]~combout\;
\inst2|ALT_INV_TSUM[4][19]~combout\ <= NOT \inst2|TSUM[4][19]~combout\;
\inst2|ALT_INV_TSUM[4][20]~combout\ <= NOT \inst2|TSUM[4][20]~combout\;
\inst2|ALT_INV_TSUM[4][21]~combout\ <= NOT \inst2|TSUM[4][21]~combout\;
\inst2|ALT_INV_TSUM[4][22]~combout\ <= NOT \inst2|TSUM[4][22]~combout\;
\inst2|ALT_INV_TSUM[4][23]~combout\ <= NOT \inst2|TSUM[4][23]~combout\;
\inst2|ALT_INV_TSUM[4][24]~combout\ <= NOT \inst2|TSUM[4][24]~combout\;
\inst2|ALT_INV_TSUM[4][25]~combout\ <= NOT \inst2|TSUM[4][25]~combout\;
\inst2|ALT_INV_TSUM[4][26]~combout\ <= NOT \inst2|TSUM[4][26]~combout\;
\inst2|ALT_INV_TSUM[4][27]~combout\ <= NOT \inst2|TSUM[4][27]~combout\;
\inst2|ALT_INV_TSUM[4][28]~combout\ <= NOT \inst2|TSUM[4][28]~combout\;
\inst2|ALT_INV_TSUM[4][29]~combout\ <= NOT \inst2|TSUM[4][29]~combout\;
\inst2|ALT_INV_TSUM[4][30]~combout\ <= NOT \inst2|TSUM[4][30]~combout\;
\inst2|ALT_INV_TSUM[4][31]~combout\ <= NOT \inst2|TSUM[4][31]~combout\;
\inst6|ALT_INV_STEMP\(14) <= NOT \inst6|STEMP\(14);
\inst2|ALT_INV_TSUM[5][0]~combout\ <= NOT \inst2|TSUM[5][0]~combout\;
\inst2|ALT_INV_TSUM[5][1]~combout\ <= NOT \inst2|TSUM[5][1]~combout\;
\inst2|ALT_INV_TSUM[5][2]~combout\ <= NOT \inst2|TSUM[5][2]~combout\;
\inst2|ALT_INV_TSUM[5][3]~combout\ <= NOT \inst2|TSUM[5][3]~combout\;
\inst2|ALT_INV_TSUM[5][4]~combout\ <= NOT \inst2|TSUM[5][4]~combout\;
\inst2|ALT_INV_TSUM[5][5]~combout\ <= NOT \inst2|TSUM[5][5]~combout\;
\inst6|ALT_INV_STEMP\(17) <= NOT \inst6|STEMP\(17);
\inst2|ALT_INV_TSUM[5][6]~combout\ <= NOT \inst2|TSUM[5][6]~combout\;
\inst2|ALT_INV_TSUM[5][7]~combout\ <= NOT \inst2|TSUM[5][7]~combout\;
\inst2|ALT_INV_TSUM[5][8]~combout\ <= NOT \inst2|TSUM[5][8]~combout\;
\inst2|ALT_INV_TSUM[5][9]~combout\ <= NOT \inst2|TSUM[5][9]~combout\;
\inst2|ALT_INV_TSUM[5][10]~combout\ <= NOT \inst2|TSUM[5][10]~combout\;
\inst2|ALT_INV_TSUM[5][11]~combout\ <= NOT \inst2|TSUM[5][11]~combout\;
\inst2|ALT_INV_TSUM[5][12]~combout\ <= NOT \inst2|TSUM[5][12]~combout\;
\inst2|ALT_INV_TSUM[5][13]~combout\ <= NOT \inst2|TSUM[5][13]~combout\;
\inst2|ALT_INV_TSUM[5][14]~combout\ <= NOT \inst2|TSUM[5][14]~combout\;
\inst2|ALT_INV_TSUM[5][15]~combout\ <= NOT \inst2|TSUM[5][15]~combout\;
\inst2|ALT_INV_TSUM[5][16]~combout\ <= NOT \inst2|TSUM[5][16]~combout\;
\inst2|ALT_INV_TSUM[5][17]~combout\ <= NOT \inst2|TSUM[5][17]~combout\;
\inst2|ALT_INV_TSUM[5][18]~combout\ <= NOT \inst2|TSUM[5][18]~combout\;
\inst2|ALT_INV_TSUM[5][19]~combout\ <= NOT \inst2|TSUM[5][19]~combout\;
\inst2|ALT_INV_TSUM[5][20]~combout\ <= NOT \inst2|TSUM[5][20]~combout\;
\inst2|ALT_INV_TSUM[5][21]~combout\ <= NOT \inst2|TSUM[5][21]~combout\;
\inst2|ALT_INV_TSUM[5][22]~combout\ <= NOT \inst2|TSUM[5][22]~combout\;
\inst2|ALT_INV_TSUM[5][23]~combout\ <= NOT \inst2|TSUM[5][23]~combout\;
\inst2|ALT_INV_TSUM[5][24]~combout\ <= NOT \inst2|TSUM[5][24]~combout\;
\inst2|ALT_INV_TSUM[5][25]~combout\ <= NOT \inst2|TSUM[5][25]~combout\;
\inst2|ALT_INV_TSUM[5][26]~combout\ <= NOT \inst2|TSUM[5][26]~combout\;
\inst2|ALT_INV_TSUM[5][27]~combout\ <= NOT \inst2|TSUM[5][27]~combout\;
\inst2|ALT_INV_TSUM[5][28]~combout\ <= NOT \inst2|TSUM[5][28]~combout\;
\inst2|ALT_INV_TSUM[5][29]~combout\ <= NOT \inst2|TSUM[5][29]~combout\;
\inst2|ALT_INV_TSUM[5][30]~combout\ <= NOT \inst2|TSUM[5][30]~combout\;
\inst2|ALT_INV_TSUM[5][31]~combout\ <= NOT \inst2|TSUM[5][31]~combout\;
\inst6|ALT_INV_STEMP\(16) <= NOT \inst6|STEMP\(16);
\inst2|ALT_INV_TSUM[6][0]~combout\ <= NOT \inst2|TSUM[6][0]~combout\;
\inst2|ALT_INV_TSUM[6][1]~combout\ <= NOT \inst2|TSUM[6][1]~combout\;
\inst2|ALT_INV_TSUM[6][2]~combout\ <= NOT \inst2|TSUM[6][2]~combout\;
\inst2|ALT_INV_TSUM[6][3]~combout\ <= NOT \inst2|TSUM[6][3]~combout\;
\inst2|ALT_INV_TSUM[6][4]~combout\ <= NOT \inst2|TSUM[6][4]~combout\;
\inst2|ALT_INV_TSUM[6][5]~combout\ <= NOT \inst2|TSUM[6][5]~combout\;
\inst2|ALT_INV_TSUM[6][6]~combout\ <= NOT \inst2|TSUM[6][6]~combout\;
\inst2|ALT_INV_TSUM[6][7]~combout\ <= NOT \inst2|TSUM[6][7]~combout\;
\inst2|ALT_INV_TSUM[6][8]~combout\ <= NOT \inst2|TSUM[6][8]~combout\;
\inst2|ALT_INV_TSUM[6][9]~combout\ <= NOT \inst2|TSUM[6][9]~combout\;
\inst2|ALT_INV_TSUM[6][10]~combout\ <= NOT \inst2|TSUM[6][10]~combout\;
\inst2|ALT_INV_TSUM[6][11]~combout\ <= NOT \inst2|TSUM[6][11]~combout\;
\inst2|ALT_INV_TSUM[6][12]~combout\ <= NOT \inst2|TSUM[6][12]~combout\;
\inst2|ALT_INV_TSUM[6][13]~combout\ <= NOT \inst2|TSUM[6][13]~combout\;
\inst2|ALT_INV_TSUM[6][14]~combout\ <= NOT \inst2|TSUM[6][14]~combout\;
\inst2|ALT_INV_TSUM[6][15]~combout\ <= NOT \inst2|TSUM[6][15]~combout\;
\inst2|ALT_INV_TSUM[6][16]~combout\ <= NOT \inst2|TSUM[6][16]~combout\;
\inst2|ALT_INV_TSUM[6][17]~combout\ <= NOT \inst2|TSUM[6][17]~combout\;
\inst2|ALT_INV_TSUM[6][18]~combout\ <= NOT \inst2|TSUM[6][18]~combout\;
\inst2|ALT_INV_TSUM[6][19]~combout\ <= NOT \inst2|TSUM[6][19]~combout\;
\inst2|ALT_INV_TSUM[6][20]~combout\ <= NOT \inst2|TSUM[6][20]~combout\;
\inst2|ALT_INV_TSUM[6][21]~combout\ <= NOT \inst2|TSUM[6][21]~combout\;
\inst2|ALT_INV_TSUM[6][22]~combout\ <= NOT \inst2|TSUM[6][22]~combout\;
\inst2|ALT_INV_TSUM[6][23]~combout\ <= NOT \inst2|TSUM[6][23]~combout\;
\inst2|ALT_INV_TSUM[6][24]~combout\ <= NOT \inst2|TSUM[6][24]~combout\;
\inst2|ALT_INV_TSUM[6][25]~combout\ <= NOT \inst2|TSUM[6][25]~combout\;
\inst2|ALT_INV_TSUM[6][26]~combout\ <= NOT \inst2|TSUM[6][26]~combout\;
\inst2|ALT_INV_TSUM[6][27]~combout\ <= NOT \inst2|TSUM[6][27]~combout\;
\inst2|ALT_INV_TSUM[6][28]~combout\ <= NOT \inst2|TSUM[6][28]~combout\;
\inst2|ALT_INV_TSUM[6][29]~combout\ <= NOT \inst2|TSUM[6][29]~combout\;
\inst2|ALT_INV_TSUM[6][30]~combout\ <= NOT \inst2|TSUM[6][30]~combout\;
\inst2|ALT_INV_TSUM[6][31]~combout\ <= NOT \inst2|TSUM[6][31]~combout\;
\inst2|ALT_INV_TSUM[7][0]~combout\ <= NOT \inst2|TSUM[7][0]~combout\;
\inst2|ALT_INV_TSUM[7][1]~combout\ <= NOT \inst2|TSUM[7][1]~combout\;
\inst2|ALT_INV_TSUM[7][2]~combout\ <= NOT \inst2|TSUM[7][2]~combout\;
\inst2|ALT_INV_TSUM[7][3]~combout\ <= NOT \inst2|TSUM[7][3]~combout\;
\inst2|ALT_INV_TSUM[7][4]~combout\ <= NOT \inst2|TSUM[7][4]~combout\;
\inst2|ALT_INV_TSUM[7][5]~combout\ <= NOT \inst2|TSUM[7][5]~combout\;
\inst2|ALT_INV_TSUM[7][6]~combout\ <= NOT \inst2|TSUM[7][6]~combout\;
\inst2|ALT_INV_TSUM[7][7]~combout\ <= NOT \inst2|TSUM[7][7]~combout\;
\inst2|ALT_INV_TSUM[7][8]~combout\ <= NOT \inst2|TSUM[7][8]~combout\;
\inst2|ALT_INV_TSUM[7][9]~combout\ <= NOT \inst2|TSUM[7][9]~combout\;
\inst2|ALT_INV_TSUM[7][10]~combout\ <= NOT \inst2|TSUM[7][10]~combout\;
\inst2|ALT_INV_TSUM[7][11]~combout\ <= NOT \inst2|TSUM[7][11]~combout\;
\inst2|ALT_INV_TSUM[7][12]~combout\ <= NOT \inst2|TSUM[7][12]~combout\;
\inst2|ALT_INV_TSUM[7][13]~combout\ <= NOT \inst2|TSUM[7][13]~combout\;
\inst2|ALT_INV_TSUM[7][14]~combout\ <= NOT \inst2|TSUM[7][14]~combout\;
\inst2|ALT_INV_TSUM[7][15]~combout\ <= NOT \inst2|TSUM[7][15]~combout\;
\inst2|ALT_INV_TSUM[7][16]~combout\ <= NOT \inst2|TSUM[7][16]~combout\;
\inst2|ALT_INV_TSUM[7][17]~combout\ <= NOT \inst2|TSUM[7][17]~combout\;
\inst2|ALT_INV_TSUM[7][18]~combout\ <= NOT \inst2|TSUM[7][18]~combout\;
\inst2|ALT_INV_TSUM[7][19]~combout\ <= NOT \inst2|TSUM[7][19]~combout\;
\inst2|ALT_INV_TSUM[7][20]~combout\ <= NOT \inst2|TSUM[7][20]~combout\;
\inst2|ALT_INV_TSUM[7][21]~combout\ <= NOT \inst2|TSUM[7][21]~combout\;
\inst2|ALT_INV_TSUM[7][22]~combout\ <= NOT \inst2|TSUM[7][22]~combout\;
\inst2|ALT_INV_TSUM[7][23]~combout\ <= NOT \inst2|TSUM[7][23]~combout\;
\inst2|ALT_INV_TSUM[7][24]~combout\ <= NOT \inst2|TSUM[7][24]~combout\;
\inst2|ALT_INV_TSUM[7][25]~combout\ <= NOT \inst2|TSUM[7][25]~combout\;
\inst2|ALT_INV_TSUM[7][26]~combout\ <= NOT \inst2|TSUM[7][26]~combout\;
\inst2|ALT_INV_TSUM[7][27]~combout\ <= NOT \inst2|TSUM[7][27]~combout\;
\inst2|ALT_INV_TSUM[7][28]~combout\ <= NOT \inst2|TSUM[7][28]~combout\;
\inst2|ALT_INV_TSUM[7][29]~combout\ <= NOT \inst2|TSUM[7][29]~combout\;
\inst2|ALT_INV_TSUM[7][30]~combout\ <= NOT \inst2|TSUM[7][30]~combout\;
\inst2|ALT_INV_TSUM[7][31]~combout\ <= NOT \inst2|TSUM[7][31]~combout\;
\inst2|ALT_INV_SUM\(0) <= NOT \inst2|SUM\(0);
\inst2|ALT_INV_SUM\(1) <= NOT \inst2|SUM\(1);
\inst2|ALT_INV_SUM\(2) <= NOT \inst2|SUM\(2);
\inst2|ALT_INV_SUM\(3) <= NOT \inst2|SUM\(3);
\inst2|ALT_INV_SUM\(4) <= NOT \inst2|SUM\(4);
\inst2|ALT_INV_SUM\(5) <= NOT \inst2|SUM\(5);
\inst2|ALT_INV_SUM\(6) <= NOT \inst2|SUM\(6);
\inst2|ALT_INV_SUM\(7) <= NOT \inst2|SUM\(7);
\inst2|ALT_INV_SUM\(8) <= NOT \inst2|SUM\(8);
\inst2|ALT_INV_SUM\(9) <= NOT \inst2|SUM\(9);
\inst2|ALT_INV_SUM\(10) <= NOT \inst2|SUM\(10);
\inst2|ALT_INV_SUM\(11) <= NOT \inst2|SUM\(11);
\inst2|ALT_INV_SUM\(12) <= NOT \inst2|SUM\(12);
\inst2|ALT_INV_SUM\(13) <= NOT \inst2|SUM\(13);
\inst2|ALT_INV_SUM\(14) <= NOT \inst2|SUM\(14);
\inst2|ALT_INV_SUM\(15) <= NOT \inst2|SUM\(15);
\inst2|ALT_INV_SUM\(16) <= NOT \inst2|SUM\(16);
\inst2|ALT_INV_SUM\(17) <= NOT \inst2|SUM\(17);
\inst2|ALT_INV_SUM\(18) <= NOT \inst2|SUM\(18);
\inst2|ALT_INV_SUM\(19) <= NOT \inst2|SUM\(19);
\inst2|ALT_INV_SUM\(20) <= NOT \inst2|SUM\(20);
\inst2|ALT_INV_SUM\(21) <= NOT \inst2|SUM\(21);
\inst2|ALT_INV_SUM\(22) <= NOT \inst2|SUM\(22);
\inst2|ALT_INV_SUM\(23) <= NOT \inst2|SUM\(23);
\inst2|ALT_INV_SUM\(24) <= NOT \inst2|SUM\(24);
\inst2|ALT_INV_SUM\(25) <= NOT \inst2|SUM\(25);
\inst2|ALT_INV_SUM\(26) <= NOT \inst2|SUM\(26);
\inst2|ALT_INV_SUM\(27) <= NOT \inst2|SUM\(27);
\inst2|ALT_INV_SUM\(28) <= NOT \inst2|SUM\(28);
\inst2|ALT_INV_SUM\(29) <= NOT \inst2|SUM\(29);
\inst2|ALT_INV_SUM\(30) <= NOT \inst2|SUM\(30);
\inst2|ALT_INV_SUM\(31) <= NOT \inst2|SUM\(31);
\inst6|ALT_INV_STEMP\(18) <= NOT \inst6|STEMP\(18);
\inst7|ALT_INV_COUNT\(0) <= NOT \inst7|COUNT\(0);
\inst7|ALT_INV_COUNT\(4) <= NOT \inst7|COUNT\(4);
\inst7|ALT_INV_COUNT\(3) <= NOT \inst7|COUNT\(3);
\inst7|ALT_INV_COUNT\(2) <= NOT \inst7|COUNT\(2);
\inst7|ALT_INV_COUNT\(1) <= NOT \inst7|COUNT\(1);
\inst21|ALT_INV_TMPNEG0~q\ <= NOT \inst21|TMPNEG0~q\;
\inst21|ALT_INV_TMPNEG1~q\ <= NOT \inst21|TMPNEG1~q\;
\inst7|ALT_INV_DIVD~q\ <= NOT \inst7|DIVD~q\;
\inst2|ALT_INV_PSUM[0][0]~q\ <= NOT \inst2|PSUM[0][0]~q\;
\inst2|ALT_INV_PSUM[1][0]~q\ <= NOT \inst2|PSUM[1][0]~q\;
\inst2|ALT_INV_PSUM[0][1]~q\ <= NOT \inst2|PSUM[0][1]~q\;
\inst2|ALT_INV_PSUM[1][1]~q\ <= NOT \inst2|PSUM[1][1]~q\;
\inst2|ALT_INV_PSUM[0][2]~q\ <= NOT \inst2|PSUM[0][2]~q\;
\inst2|ALT_INV_PSUM[1][2]~q\ <= NOT \inst2|PSUM[1][2]~q\;
\inst2|ALT_INV_PSUM[0][3]~q\ <= NOT \inst2|PSUM[0][3]~q\;
\inst2|ALT_INV_PSUM[1][3]~q\ <= NOT \inst2|PSUM[1][3]~q\;
\inst2|ALT_INV_PSUM[0][4]~q\ <= NOT \inst2|PSUM[0][4]~q\;
\inst2|ALT_INV_PSUM[1][4]~q\ <= NOT \inst2|PSUM[1][4]~q\;
\inst2|ALT_INV_PSUM[0][5]~q\ <= NOT \inst2|PSUM[0][5]~q\;
\inst2|ALT_INV_PSUM[1][5]~q\ <= NOT \inst2|PSUM[1][5]~q\;
\inst2|ALT_INV_Add0~21_sumout\ <= NOT \inst2|Add0~21_sumout\;
\inst2|ALT_INV_Add0~17_sumout\ <= NOT \inst2|Add0~17_sumout\;
\inst2|ALT_INV_Add0~13_sumout\ <= NOT \inst2|Add0~13_sumout\;
\inst2|ALT_INV_Add0~9_sumout\ <= NOT \inst2|Add0~9_sumout\;
\inst2|ALT_INV_Add0~5_sumout\ <= NOT \inst2|Add0~5_sumout\;
\inst2|ALT_INV_Add0~1_sumout\ <= NOT \inst2|Add0~1_sumout\;
\inst2|ALT_INV_PSUM[2][0]~q\ <= NOT \inst2|PSUM[2][0]~q\;
\inst2|ALT_INV_PSUM[2][1]~q\ <= NOT \inst2|PSUM[2][1]~q\;
\inst2|ALT_INV_PSUM[2][2]~q\ <= NOT \inst2|PSUM[2][2]~q\;
\inst2|ALT_INV_PSUM[2][3]~q\ <= NOT \inst2|PSUM[2][3]~q\;
\inst2|ALT_INV_PSUM[2][4]~q\ <= NOT \inst2|PSUM[2][4]~q\;
\inst2|ALT_INV_PSUM[2][5]~q\ <= NOT \inst2|PSUM[2][5]~q\;
\inst2|ALT_INV_Add1~125_sumout\ <= NOT \inst2|Add1~125_sumout\;
\inst2|ALT_INV_Add1~121_sumout\ <= NOT \inst2|Add1~121_sumout\;
\inst2|ALT_INV_Add1~117_sumout\ <= NOT \inst2|Add1~117_sumout\;
\inst2|ALT_INV_Add1~113_sumout\ <= NOT \inst2|Add1~113_sumout\;
\inst2|ALT_INV_Add1~109_sumout\ <= NOT \inst2|Add1~109_sumout\;
\inst2|ALT_INV_Add1~105_sumout\ <= NOT \inst2|Add1~105_sumout\;
\inst2|ALT_INV_Add1~101_sumout\ <= NOT \inst2|Add1~101_sumout\;
\inst2|ALT_INV_Add1~97_sumout\ <= NOT \inst2|Add1~97_sumout\;
\inst2|ALT_INV_Add1~93_sumout\ <= NOT \inst2|Add1~93_sumout\;
\inst2|ALT_INV_Add1~89_sumout\ <= NOT \inst2|Add1~89_sumout\;
\inst2|ALT_INV_Add1~85_sumout\ <= NOT \inst2|Add1~85_sumout\;
\inst2|ALT_INV_Add1~81_sumout\ <= NOT \inst2|Add1~81_sumout\;
\inst2|ALT_INV_Add1~77_sumout\ <= NOT \inst2|Add1~77_sumout\;
\inst2|ALT_INV_Add1~73_sumout\ <= NOT \inst2|Add1~73_sumout\;
\inst2|ALT_INV_Add1~69_sumout\ <= NOT \inst2|Add1~69_sumout\;
\inst2|ALT_INV_Add1~65_sumout\ <= NOT \inst2|Add1~65_sumout\;
\inst2|ALT_INV_Add1~61_sumout\ <= NOT \inst2|Add1~61_sumout\;
\inst2|ALT_INV_Add1~57_sumout\ <= NOT \inst2|Add1~57_sumout\;
\inst2|ALT_INV_Add1~53_sumout\ <= NOT \inst2|Add1~53_sumout\;
\inst2|ALT_INV_Add1~49_sumout\ <= NOT \inst2|Add1~49_sumout\;
\inst2|ALT_INV_Add1~45_sumout\ <= NOT \inst2|Add1~45_sumout\;
\inst2|ALT_INV_Add1~41_sumout\ <= NOT \inst2|Add1~41_sumout\;
\inst2|ALT_INV_Add1~37_sumout\ <= NOT \inst2|Add1~37_sumout\;
\inst2|ALT_INV_Add1~33_sumout\ <= NOT \inst2|Add1~33_sumout\;
\inst2|ALT_INV_Add1~29_sumout\ <= NOT \inst2|Add1~29_sumout\;
\inst2|ALT_INV_Add1~25_sumout\ <= NOT \inst2|Add1~25_sumout\;
\inst2|ALT_INV_Add1~21_sumout\ <= NOT \inst2|Add1~21_sumout\;
\inst2|ALT_INV_Add1~17_sumout\ <= NOT \inst2|Add1~17_sumout\;
\inst2|ALT_INV_Add1~13_sumout\ <= NOT \inst2|Add1~13_sumout\;
\inst2|ALT_INV_Add1~9_sumout\ <= NOT \inst2|Add1~9_sumout\;
\inst2|ALT_INV_Add1~5_sumout\ <= NOT \inst2|Add1~5_sumout\;
\inst2|ALT_INV_Add1~1_sumout\ <= NOT \inst2|Add1~1_sumout\;
\inst2|ALT_INV_PSUM[3][0]~q\ <= NOT \inst2|PSUM[3][0]~q\;
\inst2|ALT_INV_PSUM[3][1]~q\ <= NOT \inst2|PSUM[3][1]~q\;
\inst2|ALT_INV_PSUM[3][2]~q\ <= NOT \inst2|PSUM[3][2]~q\;
\inst2|ALT_INV_PSUM[3][3]~q\ <= NOT \inst2|PSUM[3][3]~q\;
\inst2|ALT_INV_PSUM[3][4]~q\ <= NOT \inst2|PSUM[3][4]~q\;
\inst2|ALT_INV_PSUM[3][5]~q\ <= NOT \inst2|PSUM[3][5]~q\;
\inst2|ALT_INV_Add2~125_sumout\ <= NOT \inst2|Add2~125_sumout\;
\inst2|ALT_INV_Add2~121_sumout\ <= NOT \inst2|Add2~121_sumout\;
\inst2|ALT_INV_Add2~117_sumout\ <= NOT \inst2|Add2~117_sumout\;
\inst2|ALT_INV_Add2~113_sumout\ <= NOT \inst2|Add2~113_sumout\;
\inst2|ALT_INV_Add2~109_sumout\ <= NOT \inst2|Add2~109_sumout\;
\inst2|ALT_INV_Add2~105_sumout\ <= NOT \inst2|Add2~105_sumout\;
\inst2|ALT_INV_Add2~101_sumout\ <= NOT \inst2|Add2~101_sumout\;
\inst2|ALT_INV_Add2~97_sumout\ <= NOT \inst2|Add2~97_sumout\;
\inst2|ALT_INV_Add2~93_sumout\ <= NOT \inst2|Add2~93_sumout\;
\inst2|ALT_INV_Add2~89_sumout\ <= NOT \inst2|Add2~89_sumout\;
\inst2|ALT_INV_Add2~85_sumout\ <= NOT \inst2|Add2~85_sumout\;
\inst2|ALT_INV_Add2~81_sumout\ <= NOT \inst2|Add2~81_sumout\;
\inst2|ALT_INV_Add2~77_sumout\ <= NOT \inst2|Add2~77_sumout\;
\inst2|ALT_INV_Add2~73_sumout\ <= NOT \inst2|Add2~73_sumout\;
\inst2|ALT_INV_Add2~69_sumout\ <= NOT \inst2|Add2~69_sumout\;
\inst2|ALT_INV_Add2~65_sumout\ <= NOT \inst2|Add2~65_sumout\;
\inst2|ALT_INV_Add2~61_sumout\ <= NOT \inst2|Add2~61_sumout\;
\inst2|ALT_INV_Add2~57_sumout\ <= NOT \inst2|Add2~57_sumout\;
\inst2|ALT_INV_Add2~53_sumout\ <= NOT \inst2|Add2~53_sumout\;
\inst2|ALT_INV_Add2~49_sumout\ <= NOT \inst2|Add2~49_sumout\;
\inst2|ALT_INV_Add2~45_sumout\ <= NOT \inst2|Add2~45_sumout\;
\inst2|ALT_INV_Add2~41_sumout\ <= NOT \inst2|Add2~41_sumout\;
\inst2|ALT_INV_Add2~37_sumout\ <= NOT \inst2|Add2~37_sumout\;
\inst2|ALT_INV_Add2~33_sumout\ <= NOT \inst2|Add2~33_sumout\;
\inst2|ALT_INV_Add2~29_sumout\ <= NOT \inst2|Add2~29_sumout\;
\inst2|ALT_INV_Add2~25_sumout\ <= NOT \inst2|Add2~25_sumout\;
\inst2|ALT_INV_Add2~21_sumout\ <= NOT \inst2|Add2~21_sumout\;
\inst2|ALT_INV_Add2~17_sumout\ <= NOT \inst2|Add2~17_sumout\;
\inst2|ALT_INV_Add2~13_sumout\ <= NOT \inst2|Add2~13_sumout\;
\inst2|ALT_INV_Add2~9_sumout\ <= NOT \inst2|Add2~9_sumout\;
\inst2|ALT_INV_Add2~5_sumout\ <= NOT \inst2|Add2~5_sumout\;
\inst2|ALT_INV_Add2~1_sumout\ <= NOT \inst2|Add2~1_sumout\;
\inst2|ALT_INV_PSUM[4][0]~q\ <= NOT \inst2|PSUM[4][0]~q\;
\inst2|ALT_INV_PSUM[4][1]~q\ <= NOT \inst2|PSUM[4][1]~q\;
\inst2|ALT_INV_PSUM[4][2]~q\ <= NOT \inst2|PSUM[4][2]~q\;
\inst2|ALT_INV_PSUM[4][3]~q\ <= NOT \inst2|PSUM[4][3]~q\;
\inst2|ALT_INV_PSUM[4][4]~q\ <= NOT \inst2|PSUM[4][4]~q\;
\inst2|ALT_INV_PSUM[4][5]~q\ <= NOT \inst2|PSUM[4][5]~q\;
\inst2|ALT_INV_Add3~125_sumout\ <= NOT \inst2|Add3~125_sumout\;
\inst2|ALT_INV_Add3~121_sumout\ <= NOT \inst2|Add3~121_sumout\;
\inst2|ALT_INV_Add3~117_sumout\ <= NOT \inst2|Add3~117_sumout\;
\inst2|ALT_INV_Add3~113_sumout\ <= NOT \inst2|Add3~113_sumout\;
\inst2|ALT_INV_Add3~109_sumout\ <= NOT \inst2|Add3~109_sumout\;
\inst2|ALT_INV_Add3~105_sumout\ <= NOT \inst2|Add3~105_sumout\;
\inst2|ALT_INV_Add3~101_sumout\ <= NOT \inst2|Add3~101_sumout\;
\inst2|ALT_INV_Add3~97_sumout\ <= NOT \inst2|Add3~97_sumout\;
\inst2|ALT_INV_Add3~93_sumout\ <= NOT \inst2|Add3~93_sumout\;
\inst2|ALT_INV_Add3~89_sumout\ <= NOT \inst2|Add3~89_sumout\;
\inst2|ALT_INV_Add3~85_sumout\ <= NOT \inst2|Add3~85_sumout\;
\inst2|ALT_INV_Add3~81_sumout\ <= NOT \inst2|Add3~81_sumout\;
\inst2|ALT_INV_Add3~77_sumout\ <= NOT \inst2|Add3~77_sumout\;
\inst2|ALT_INV_Add3~73_sumout\ <= NOT \inst2|Add3~73_sumout\;
\inst2|ALT_INV_Add3~69_sumout\ <= NOT \inst2|Add3~69_sumout\;
\inst2|ALT_INV_Add3~65_sumout\ <= NOT \inst2|Add3~65_sumout\;
\inst2|ALT_INV_Add3~61_sumout\ <= NOT \inst2|Add3~61_sumout\;
\inst2|ALT_INV_Add3~57_sumout\ <= NOT \inst2|Add3~57_sumout\;
\inst2|ALT_INV_Add3~53_sumout\ <= NOT \inst2|Add3~53_sumout\;
\inst2|ALT_INV_Add3~49_sumout\ <= NOT \inst2|Add3~49_sumout\;
\inst2|ALT_INV_Add3~45_sumout\ <= NOT \inst2|Add3~45_sumout\;
\inst2|ALT_INV_Add3~41_sumout\ <= NOT \inst2|Add3~41_sumout\;
\inst2|ALT_INV_Add3~37_sumout\ <= NOT \inst2|Add3~37_sumout\;
\inst2|ALT_INV_Add3~33_sumout\ <= NOT \inst2|Add3~33_sumout\;
\inst2|ALT_INV_Add3~29_sumout\ <= NOT \inst2|Add3~29_sumout\;
\inst2|ALT_INV_Add3~25_sumout\ <= NOT \inst2|Add3~25_sumout\;
\inst2|ALT_INV_Add3~21_sumout\ <= NOT \inst2|Add3~21_sumout\;
\inst2|ALT_INV_Add3~17_sumout\ <= NOT \inst2|Add3~17_sumout\;
\inst2|ALT_INV_Add3~13_sumout\ <= NOT \inst2|Add3~13_sumout\;
\inst2|ALT_INV_Add3~9_sumout\ <= NOT \inst2|Add3~9_sumout\;
\inst2|ALT_INV_Add3~5_sumout\ <= NOT \inst2|Add3~5_sumout\;
\inst2|ALT_INV_Add3~1_sumout\ <= NOT \inst2|Add3~1_sumout\;
\inst2|ALT_INV_PSUM[5][0]~q\ <= NOT \inst2|PSUM[5][0]~q\;
\inst2|ALT_INV_PSUM[5][1]~q\ <= NOT \inst2|PSUM[5][1]~q\;
\inst2|ALT_INV_PSUM[5][2]~q\ <= NOT \inst2|PSUM[5][2]~q\;
\inst2|ALT_INV_PSUM[5][3]~q\ <= NOT \inst2|PSUM[5][3]~q\;
\inst2|ALT_INV_PSUM[5][4]~q\ <= NOT \inst2|PSUM[5][4]~q\;
\inst2|ALT_INV_PSUM[5][5]~q\ <= NOT \inst2|PSUM[5][5]~q\;
\inst2|ALT_INV_Add4~125_sumout\ <= NOT \inst2|Add4~125_sumout\;
\inst2|ALT_INV_Add4~121_sumout\ <= NOT \inst2|Add4~121_sumout\;
\inst2|ALT_INV_Add4~117_sumout\ <= NOT \inst2|Add4~117_sumout\;
\inst2|ALT_INV_Add4~113_sumout\ <= NOT \inst2|Add4~113_sumout\;
\inst2|ALT_INV_Add4~109_sumout\ <= NOT \inst2|Add4~109_sumout\;
\inst2|ALT_INV_Add4~105_sumout\ <= NOT \inst2|Add4~105_sumout\;
\inst2|ALT_INV_Add4~101_sumout\ <= NOT \inst2|Add4~101_sumout\;
\inst2|ALT_INV_Add4~97_sumout\ <= NOT \inst2|Add4~97_sumout\;
\inst2|ALT_INV_Add4~93_sumout\ <= NOT \inst2|Add4~93_sumout\;
\inst2|ALT_INV_Add4~89_sumout\ <= NOT \inst2|Add4~89_sumout\;
\inst2|ALT_INV_Add4~85_sumout\ <= NOT \inst2|Add4~85_sumout\;
\inst2|ALT_INV_Add4~81_sumout\ <= NOT \inst2|Add4~81_sumout\;
\inst2|ALT_INV_Add4~77_sumout\ <= NOT \inst2|Add4~77_sumout\;
\inst2|ALT_INV_Add4~73_sumout\ <= NOT \inst2|Add4~73_sumout\;
\inst2|ALT_INV_Add4~69_sumout\ <= NOT \inst2|Add4~69_sumout\;
\inst2|ALT_INV_Add4~65_sumout\ <= NOT \inst2|Add4~65_sumout\;
\inst2|ALT_INV_Add4~61_sumout\ <= NOT \inst2|Add4~61_sumout\;
\inst2|ALT_INV_Add4~57_sumout\ <= NOT \inst2|Add4~57_sumout\;
\inst2|ALT_INV_Add4~53_sumout\ <= NOT \inst2|Add4~53_sumout\;
\inst2|ALT_INV_Add4~49_sumout\ <= NOT \inst2|Add4~49_sumout\;
\inst2|ALT_INV_Add4~45_sumout\ <= NOT \inst2|Add4~45_sumout\;
\inst2|ALT_INV_Add4~41_sumout\ <= NOT \inst2|Add4~41_sumout\;
\inst2|ALT_INV_Add4~37_sumout\ <= NOT \inst2|Add4~37_sumout\;
\inst2|ALT_INV_Add4~33_sumout\ <= NOT \inst2|Add4~33_sumout\;
\inst2|ALT_INV_Add4~29_sumout\ <= NOT \inst2|Add4~29_sumout\;
\inst2|ALT_INV_Add4~25_sumout\ <= NOT \inst2|Add4~25_sumout\;
\inst2|ALT_INV_Add4~21_sumout\ <= NOT \inst2|Add4~21_sumout\;
\inst2|ALT_INV_Add4~17_sumout\ <= NOT \inst2|Add4~17_sumout\;
\inst2|ALT_INV_Add4~13_sumout\ <= NOT \inst2|Add4~13_sumout\;
\inst2|ALT_INV_Add4~9_sumout\ <= NOT \inst2|Add4~9_sumout\;
\inst2|ALT_INV_Add4~5_sumout\ <= NOT \inst2|Add4~5_sumout\;
\inst2|ALT_INV_Add4~1_sumout\ <= NOT \inst2|Add4~1_sumout\;
\inst2|ALT_INV_PSUM[6][0]~q\ <= NOT \inst2|PSUM[6][0]~q\;
\inst2|ALT_INV_PSUM[6][1]~q\ <= NOT \inst2|PSUM[6][1]~q\;
\inst2|ALT_INV_PSUM[6][2]~q\ <= NOT \inst2|PSUM[6][2]~q\;
\inst2|ALT_INV_PSUM[6][3]~q\ <= NOT \inst2|PSUM[6][3]~q\;
\inst2|ALT_INV_PSUM[6][4]~q\ <= NOT \inst2|PSUM[6][4]~q\;
\inst2|ALT_INV_PSUM[6][5]~q\ <= NOT \inst2|PSUM[6][5]~q\;
\inst2|ALT_INV_Add5~125_sumout\ <= NOT \inst2|Add5~125_sumout\;
\inst2|ALT_INV_Add5~121_sumout\ <= NOT \inst2|Add5~121_sumout\;
\inst2|ALT_INV_Add5~117_sumout\ <= NOT \inst2|Add5~117_sumout\;
\inst2|ALT_INV_Add5~113_sumout\ <= NOT \inst2|Add5~113_sumout\;
\inst2|ALT_INV_Add5~109_sumout\ <= NOT \inst2|Add5~109_sumout\;
\inst2|ALT_INV_Add5~105_sumout\ <= NOT \inst2|Add5~105_sumout\;
\inst2|ALT_INV_Add5~101_sumout\ <= NOT \inst2|Add5~101_sumout\;
\inst2|ALT_INV_Add5~97_sumout\ <= NOT \inst2|Add5~97_sumout\;
\inst2|ALT_INV_Add5~93_sumout\ <= NOT \inst2|Add5~93_sumout\;
\inst2|ALT_INV_Add5~89_sumout\ <= NOT \inst2|Add5~89_sumout\;
\inst2|ALT_INV_Add5~85_sumout\ <= NOT \inst2|Add5~85_sumout\;
\inst2|ALT_INV_Add5~81_sumout\ <= NOT \inst2|Add5~81_sumout\;
\inst2|ALT_INV_Add5~77_sumout\ <= NOT \inst2|Add5~77_sumout\;
\inst2|ALT_INV_Add5~73_sumout\ <= NOT \inst2|Add5~73_sumout\;
\inst2|ALT_INV_Add5~69_sumout\ <= NOT \inst2|Add5~69_sumout\;
\inst2|ALT_INV_Add5~65_sumout\ <= NOT \inst2|Add5~65_sumout\;
\inst2|ALT_INV_Add5~61_sumout\ <= NOT \inst2|Add5~61_sumout\;
\inst2|ALT_INV_Add5~57_sumout\ <= NOT \inst2|Add5~57_sumout\;
\inst2|ALT_INV_Add5~53_sumout\ <= NOT \inst2|Add5~53_sumout\;
\inst2|ALT_INV_Add5~49_sumout\ <= NOT \inst2|Add5~49_sumout\;
\inst2|ALT_INV_Add5~45_sumout\ <= NOT \inst2|Add5~45_sumout\;
\inst2|ALT_INV_Add5~41_sumout\ <= NOT \inst2|Add5~41_sumout\;
\inst2|ALT_INV_Add5~37_sumout\ <= NOT \inst2|Add5~37_sumout\;
\inst2|ALT_INV_Add5~33_sumout\ <= NOT \inst2|Add5~33_sumout\;
\inst2|ALT_INV_Add5~29_sumout\ <= NOT \inst2|Add5~29_sumout\;
\inst2|ALT_INV_Add5~25_sumout\ <= NOT \inst2|Add5~25_sumout\;
\inst2|ALT_INV_Add5~21_sumout\ <= NOT \inst2|Add5~21_sumout\;
\inst2|ALT_INV_Add5~17_sumout\ <= NOT \inst2|Add5~17_sumout\;
\inst2|ALT_INV_Add5~13_sumout\ <= NOT \inst2|Add5~13_sumout\;
\inst2|ALT_INV_Add5~9_sumout\ <= NOT \inst2|Add5~9_sumout\;
\inst2|ALT_INV_Add5~5_sumout\ <= NOT \inst2|Add5~5_sumout\;
\inst2|ALT_INV_Add5~1_sumout\ <= NOT \inst2|Add5~1_sumout\;
\inst2|ALT_INV_PSUM[7][0]~q\ <= NOT \inst2|PSUM[7][0]~q\;
\inst2|ALT_INV_PSUM[7][1]~q\ <= NOT \inst2|PSUM[7][1]~q\;
\inst2|ALT_INV_PSUM[7][2]~q\ <= NOT \inst2|PSUM[7][2]~q\;
\inst2|ALT_INV_PSUM[7][3]~q\ <= NOT \inst2|PSUM[7][3]~q\;
\inst2|ALT_INV_PSUM[7][4]~q\ <= NOT \inst2|PSUM[7][4]~q\;
\inst2|ALT_INV_PSUM[7][5]~q\ <= NOT \inst2|PSUM[7][5]~q\;
\inst2|ALT_INV_Add6~125_sumout\ <= NOT \inst2|Add6~125_sumout\;
\inst2|ALT_INV_Add6~121_sumout\ <= NOT \inst2|Add6~121_sumout\;
\inst2|ALT_INV_Add6~117_sumout\ <= NOT \inst2|Add6~117_sumout\;
\inst2|ALT_INV_Add6~113_sumout\ <= NOT \inst2|Add6~113_sumout\;
\inst2|ALT_INV_Add6~109_sumout\ <= NOT \inst2|Add6~109_sumout\;
\inst2|ALT_INV_Add6~105_sumout\ <= NOT \inst2|Add6~105_sumout\;
\inst2|ALT_INV_Add6~101_sumout\ <= NOT \inst2|Add6~101_sumout\;
\inst2|ALT_INV_Add6~97_sumout\ <= NOT \inst2|Add6~97_sumout\;
\inst2|ALT_INV_Add6~93_sumout\ <= NOT \inst2|Add6~93_sumout\;
\inst2|ALT_INV_Add6~89_sumout\ <= NOT \inst2|Add6~89_sumout\;
\inst2|ALT_INV_Add6~85_sumout\ <= NOT \inst2|Add6~85_sumout\;
\inst2|ALT_INV_Add6~81_sumout\ <= NOT \inst2|Add6~81_sumout\;
\inst2|ALT_INV_Add6~77_sumout\ <= NOT \inst2|Add6~77_sumout\;
\inst2|ALT_INV_Add6~73_sumout\ <= NOT \inst2|Add6~73_sumout\;
\inst2|ALT_INV_Add6~69_sumout\ <= NOT \inst2|Add6~69_sumout\;
\inst2|ALT_INV_Add6~65_sumout\ <= NOT \inst2|Add6~65_sumout\;
\inst2|ALT_INV_Add6~61_sumout\ <= NOT \inst2|Add6~61_sumout\;
\inst2|ALT_INV_Add6~57_sumout\ <= NOT \inst2|Add6~57_sumout\;
\inst2|ALT_INV_Add6~53_sumout\ <= NOT \inst2|Add6~53_sumout\;
\inst2|ALT_INV_Add6~49_sumout\ <= NOT \inst2|Add6~49_sumout\;
\inst2|ALT_INV_Add6~45_sumout\ <= NOT \inst2|Add6~45_sumout\;
\inst2|ALT_INV_Add6~41_sumout\ <= NOT \inst2|Add6~41_sumout\;
\inst2|ALT_INV_Add6~37_sumout\ <= NOT \inst2|Add6~37_sumout\;
\inst2|ALT_INV_Add6~33_sumout\ <= NOT \inst2|Add6~33_sumout\;
\inst2|ALT_INV_Add6~29_sumout\ <= NOT \inst2|Add6~29_sumout\;
\inst2|ALT_INV_Add6~25_sumout\ <= NOT \inst2|Add6~25_sumout\;
\inst2|ALT_INV_Add6~21_sumout\ <= NOT \inst2|Add6~21_sumout\;
\inst2|ALT_INV_Add6~17_sumout\ <= NOT \inst2|Add6~17_sumout\;
\inst2|ALT_INV_Add6~13_sumout\ <= NOT \inst2|Add6~13_sumout\;
\inst2|ALT_INV_Add6~9_sumout\ <= NOT \inst2|Add6~9_sumout\;
\inst2|ALT_INV_Add6~5_sumout\ <= NOT \inst2|Add6~5_sumout\;
\inst2|ALT_INV_Add6~1_sumout\ <= NOT \inst2|Add6~1_sumout\;
\inst2|ALT_INV_PSUM[8][0]~q\ <= NOT \inst2|PSUM[8][0]~q\;
\inst2|ALT_INV_PSUM[8][1]~q\ <= NOT \inst2|PSUM[8][1]~q\;
\inst2|ALT_INV_PSUM[8][2]~q\ <= NOT \inst2|PSUM[8][2]~q\;
\inst2|ALT_INV_PSUM[8][3]~q\ <= NOT \inst2|PSUM[8][3]~q\;
\inst2|ALT_INV_PSUM[8][4]~q\ <= NOT \inst2|PSUM[8][4]~q\;
\inst2|ALT_INV_PSUM[8][5]~q\ <= NOT \inst2|PSUM[8][5]~q\;
\inst2|ALT_INV_Add7~125_sumout\ <= NOT \inst2|Add7~125_sumout\;
\inst2|ALT_INV_Add7~121_sumout\ <= NOT \inst2|Add7~121_sumout\;
\inst2|ALT_INV_Add7~117_sumout\ <= NOT \inst2|Add7~117_sumout\;
\inst2|ALT_INV_Add7~113_sumout\ <= NOT \inst2|Add7~113_sumout\;
\inst2|ALT_INV_Add7~109_sumout\ <= NOT \inst2|Add7~109_sumout\;
\inst2|ALT_INV_Add7~105_sumout\ <= NOT \inst2|Add7~105_sumout\;
\inst2|ALT_INV_Add7~101_sumout\ <= NOT \inst2|Add7~101_sumout\;
\inst2|ALT_INV_Add7~97_sumout\ <= NOT \inst2|Add7~97_sumout\;
\inst2|ALT_INV_Add7~93_sumout\ <= NOT \inst2|Add7~93_sumout\;
\inst2|ALT_INV_Add7~89_sumout\ <= NOT \inst2|Add7~89_sumout\;
\inst2|ALT_INV_Add7~85_sumout\ <= NOT \inst2|Add7~85_sumout\;
\inst2|ALT_INV_Add7~81_sumout\ <= NOT \inst2|Add7~81_sumout\;
\inst2|ALT_INV_Add7~77_sumout\ <= NOT \inst2|Add7~77_sumout\;
\inst2|ALT_INV_Add7~73_sumout\ <= NOT \inst2|Add7~73_sumout\;
\inst2|ALT_INV_Add7~69_sumout\ <= NOT \inst2|Add7~69_sumout\;
\inst2|ALT_INV_Add7~65_sumout\ <= NOT \inst2|Add7~65_sumout\;
\inst2|ALT_INV_Add7~61_sumout\ <= NOT \inst2|Add7~61_sumout\;
\inst2|ALT_INV_Add7~57_sumout\ <= NOT \inst2|Add7~57_sumout\;
\inst2|ALT_INV_Add7~53_sumout\ <= NOT \inst2|Add7~53_sumout\;
\inst2|ALT_INV_Add7~49_sumout\ <= NOT \inst2|Add7~49_sumout\;
\inst2|ALT_INV_Add7~45_sumout\ <= NOT \inst2|Add7~45_sumout\;
\inst2|ALT_INV_Add7~41_sumout\ <= NOT \inst2|Add7~41_sumout\;
\inst2|ALT_INV_Add7~37_sumout\ <= NOT \inst2|Add7~37_sumout\;
\inst2|ALT_INV_Add7~33_sumout\ <= NOT \inst2|Add7~33_sumout\;
\inst2|ALT_INV_Add7~29_sumout\ <= NOT \inst2|Add7~29_sumout\;
\inst2|ALT_INV_Add7~25_sumout\ <= NOT \inst2|Add7~25_sumout\;
\inst2|ALT_INV_Add7~21_sumout\ <= NOT \inst2|Add7~21_sumout\;
\inst2|ALT_INV_Add7~17_sumout\ <= NOT \inst2|Add7~17_sumout\;
\inst2|ALT_INV_Add7~13_sumout\ <= NOT \inst2|Add7~13_sumout\;
\inst2|ALT_INV_Add7~9_sumout\ <= NOT \inst2|Add7~9_sumout\;
\inst2|ALT_INV_Add7~5_sumout\ <= NOT \inst2|Add7~5_sumout\;
\inst2|ALT_INV_Add7~1_sumout\ <= NOT \inst2|Add7~1_sumout\;
\inst2|ALT_INV_Add8~125_sumout\ <= NOT \inst2|Add8~125_sumout\;
\inst2|ALT_INV_Add8~121_sumout\ <= NOT \inst2|Add8~121_sumout\;
\inst2|ALT_INV_Add8~117_sumout\ <= NOT \inst2|Add8~117_sumout\;
\inst2|ALT_INV_Add8~113_sumout\ <= NOT \inst2|Add8~113_sumout\;
\inst2|ALT_INV_Add8~109_sumout\ <= NOT \inst2|Add8~109_sumout\;
\inst2|ALT_INV_Add8~105_sumout\ <= NOT \inst2|Add8~105_sumout\;
\inst2|ALT_INV_Add8~101_sumout\ <= NOT \inst2|Add8~101_sumout\;
\inst2|ALT_INV_Add8~97_sumout\ <= NOT \inst2|Add8~97_sumout\;
\inst2|ALT_INV_Add8~93_sumout\ <= NOT \inst2|Add8~93_sumout\;
\inst2|ALT_INV_Add8~89_sumout\ <= NOT \inst2|Add8~89_sumout\;
\inst2|ALT_INV_Add8~85_sumout\ <= NOT \inst2|Add8~85_sumout\;
\inst2|ALT_INV_Add8~81_sumout\ <= NOT \inst2|Add8~81_sumout\;
\inst2|ALT_INV_Add8~77_sumout\ <= NOT \inst2|Add8~77_sumout\;
\inst2|ALT_INV_Add8~73_sumout\ <= NOT \inst2|Add8~73_sumout\;
\inst2|ALT_INV_Add8~69_sumout\ <= NOT \inst2|Add8~69_sumout\;
\inst2|ALT_INV_Add8~65_sumout\ <= NOT \inst2|Add8~65_sumout\;
\inst2|ALT_INV_Add8~61_sumout\ <= NOT \inst2|Add8~61_sumout\;
\inst2|ALT_INV_Add8~57_sumout\ <= NOT \inst2|Add8~57_sumout\;
\inst2|ALT_INV_Add8~53_sumout\ <= NOT \inst2|Add8~53_sumout\;
\inst2|ALT_INV_Add8~49_sumout\ <= NOT \inst2|Add8~49_sumout\;
\inst2|ALT_INV_Add8~45_sumout\ <= NOT \inst2|Add8~45_sumout\;
\inst2|ALT_INV_Add8~41_sumout\ <= NOT \inst2|Add8~41_sumout\;
\inst2|ALT_INV_Add8~37_sumout\ <= NOT \inst2|Add8~37_sumout\;
\inst2|ALT_INV_Add8~33_sumout\ <= NOT \inst2|Add8~33_sumout\;
\inst2|ALT_INV_Add8~29_sumout\ <= NOT \inst2|Add8~29_sumout\;
\inst2|ALT_INV_Add8~25_sumout\ <= NOT \inst2|Add8~25_sumout\;
\inst2|ALT_INV_Add8~21_sumout\ <= NOT \inst2|Add8~21_sumout\;
\inst2|ALT_INV_Add8~17_sumout\ <= NOT \inst2|Add8~17_sumout\;
\inst2|ALT_INV_Add8~13_sumout\ <= NOT \inst2|Add8~13_sumout\;
\inst2|ALT_INV_Add8~9_sumout\ <= NOT \inst2|Add8~9_sumout\;
\inst2|ALT_INV_Add8~5_sumout\ <= NOT \inst2|Add8~5_sumout\;
\inst2|ALT_INV_Add8~1_sumout\ <= NOT \inst2|Add8~1_sumout\;

-- Location: IOOBUF_X89_Y35_N96
\RESET~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|DIVD~q\,
	devoe => ww_devoe,
	o => ww_RESET);

-- Location: IOOBUF_X89_Y4_N45
\PHASE[31]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[31]~0_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(31));

-- Location: IOOBUF_X89_Y38_N5
\PHASE[30]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[30]~1_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(30));

-- Location: IOOBUF_X70_Y0_N53
\PHASE[29]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[29]~2_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(29));

-- Location: IOOBUF_X89_Y38_N39
\PHASE[28]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[28]~3_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(28));

-- Location: IOOBUF_X89_Y8_N22
\PHASE[27]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[27]~4_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(27));

-- Location: IOOBUF_X89_Y38_N22
\PHASE[26]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[26]~5_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(26));

-- Location: IOOBUF_X82_Y81_N59
\PHASE[25]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[25]~6_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(25));

-- Location: IOOBUF_X84_Y81_N19
\PHASE[24]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[24]~7_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(24));

-- Location: IOOBUF_X89_Y36_N39
\PHASE[23]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[23]~8_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(23));

-- Location: IOOBUF_X89_Y36_N22
\PHASE[22]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[22]~9_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(22));

-- Location: IOOBUF_X89_Y4_N62
\PHASE[21]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[21]~10_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(21));

-- Location: IOOBUF_X89_Y36_N5
\PHASE[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[20]~11_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(20));

-- Location: IOOBUF_X89_Y6_N56
\PHASE[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[19]~12_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(19));

-- Location: IOOBUF_X89_Y9_N56
\PHASE[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[18]~13_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(18));

-- Location: IOOBUF_X89_Y37_N56
\PHASE[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[17]~14_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(17));

-- Location: IOOBUF_X89_Y8_N5
\PHASE[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[16]~15_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(16));

-- Location: IOOBUF_X82_Y81_N93
\PHASE[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[15]~16_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(15));

-- Location: IOOBUF_X89_Y35_N45
\PHASE[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[14]~17_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(14));

-- Location: IOOBUF_X89_Y9_N22
\PHASE[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[13]~18_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(13));

-- Location: IOOBUF_X89_Y37_N39
\PHASE[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[12]~19_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(12));

-- Location: IOOBUF_X89_Y38_N56
\PHASE[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[11]~20_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(11));

-- Location: IOOBUF_X89_Y6_N39
\PHASE[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[10]~21_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(10));

-- Location: IOOBUF_X89_Y37_N5
\PHASE[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[9]~22_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(9));

-- Location: IOOBUF_X72_Y0_N53
\PHASE[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[8]~23_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(8));

-- Location: IOOBUF_X89_Y37_N22
\PHASE[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[7]~24_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(7));

-- Location: IOOBUF_X84_Y81_N36
\PHASE[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[6]~25_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(6));

-- Location: IOOBUF_X84_Y81_N53
\PHASE[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[5]~26_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(5));

-- Location: IOOBUF_X89_Y8_N39
\PHASE[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[4]~27_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(4));

-- Location: IOOBUF_X89_Y9_N39
\PHASE[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[3]~28_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(3));

-- Location: IOOBUF_X89_Y8_N56
\PHASE[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[2]~29_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(2));

-- Location: IOOBUF_X89_Y36_N56
\PHASE[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[1]~30_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(1));

-- Location: IOOBUF_X89_Y9_N5
\PHASE[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|ADD[0]~31_combout\,
	devoe => ww_devoe,
	o => ww_PHASE(0));

-- Location: IOIBUF_X89_Y35_N61
\FRQ0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_FRQ0,
	o => \FRQ0~input_o\);

-- Location: CLKCTRL_G10
\FRQ0~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \FRQ0~input_o\,
	outclk => \FRQ0~inputCLKENA0_outclk\);

-- Location: FF_X87_Y35_N17
\inst7|COUNT[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(3));

-- Location: FF_X87_Y35_N7
\inst7|COUNT[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(2));

-- Location: FF_X87_Y35_N31
\inst7|COUNT[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT[4]~DUPLICATE_q\);

-- Location: MLABCELL_X87_Y35_N6
\inst7|COUNT~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|COUNT~1_combout\ = ( \inst7|COUNT\(2) & ( \inst7|COUNT[4]~DUPLICATE_q\ & ( (!\inst7|COUNT\(1)) # ((!\inst7|COUNT\(0) & !\inst7|COUNT\(3))) ) ) ) # ( !\inst7|COUNT\(2) & ( \inst7|COUNT[4]~DUPLICATE_q\ & ( (\inst7|COUNT\(0) & \inst7|COUNT\(1)) ) ) ) 
-- # ( \inst7|COUNT\(2) & ( !\inst7|COUNT[4]~DUPLICATE_q\ & ( (!\inst7|COUNT\(0)) # (!\inst7|COUNT\(1)) ) ) ) # ( !\inst7|COUNT\(2) & ( !\inst7|COUNT[4]~DUPLICATE_q\ & ( (\inst7|COUNT\(0) & \inst7|COUNT\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101111110101111101000000101000001011111101011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ALT_INV_COUNT\(0),
	datac => \inst7|ALT_INV_COUNT\(1),
	datad => \inst7|ALT_INV_COUNT\(3),
	datae => \inst7|ALT_INV_COUNT\(2),
	dataf => \inst7|ALT_INV_COUNT[4]~DUPLICATE_q\,
	combout => \inst7|COUNT~1_combout\);

-- Location: FF_X87_Y35_N8
\inst7|COUNT[2]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT[2]~DUPLICATE_q\);

-- Location: MLABCELL_X87_Y35_N24
\inst7|COUNT~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|COUNT~0_combout\ = ( \inst7|COUNT\(1) & ( \inst7|COUNT[3]~DUPLICATE_q\ & ( (!\inst7|COUNT\(0) & ((!\inst7|COUNT\(4)) # (!\inst7|COUNT[2]~DUPLICATE_q\))) ) ) ) # ( !\inst7|COUNT\(1) & ( \inst7|COUNT[3]~DUPLICATE_q\ & ( \inst7|COUNT\(0) ) ) ) # ( 
-- \inst7|COUNT\(1) & ( !\inst7|COUNT[3]~DUPLICATE_q\ & ( !\inst7|COUNT\(0) ) ) ) # ( !\inst7|COUNT\(1) & ( !\inst7|COUNT[3]~DUPLICATE_q\ & ( \inst7|COUNT\(0) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101101010101010101001010101010101011010101010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ALT_INV_COUNT\(0),
	datab => \inst7|ALT_INV_COUNT\(4),
	datad => \inst7|ALT_INV_COUNT[2]~DUPLICATE_q\,
	datae => \inst7|ALT_INV_COUNT\(1),
	dataf => \inst7|ALT_INV_COUNT[3]~DUPLICATE_q\,
	combout => \inst7|COUNT~0_combout\);

-- Location: FF_X87_Y35_N26
\inst7|COUNT[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(1));

-- Location: MLABCELL_X87_Y35_N30
\inst7|COUNT~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|COUNT~3_combout\ = ( \inst7|COUNT\(4) & ( \inst7|COUNT[3]~DUPLICATE_q\ & ( (!\inst7|COUNT\(1)) # (!\inst7|COUNT[2]~DUPLICATE_q\) ) ) ) # ( !\inst7|COUNT\(4) & ( \inst7|COUNT[3]~DUPLICATE_q\ & ( (\inst7|COUNT\(0) & (\inst7|COUNT\(1) & 
-- \inst7|COUNT[2]~DUPLICATE_q\)) ) ) ) # ( \inst7|COUNT\(4) & ( !\inst7|COUNT[3]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000001011111111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ALT_INV_COUNT\(0),
	datac => \inst7|ALT_INV_COUNT\(1),
	datad => \inst7|ALT_INV_COUNT[2]~DUPLICATE_q\,
	datae => \inst7|ALT_INV_COUNT\(4),
	dataf => \inst7|ALT_INV_COUNT[3]~DUPLICATE_q\,
	combout => \inst7|COUNT~3_combout\);

-- Location: FF_X87_Y35_N32
\inst7|COUNT[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(4));

-- Location: MLABCELL_X87_Y35_N3
\inst7|COUNT~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|COUNT~4_combout\ = ( !\inst7|COUNT\(0) & ( \inst7|COUNT[3]~DUPLICATE_q\ & ( (!\inst7|COUNT\(4)) # ((!\inst7|COUNT[2]~DUPLICATE_q\) # (!\inst7|COUNT\(1))) ) ) ) # ( !\inst7|COUNT\(0) & ( !\inst7|COUNT[3]~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst7|ALT_INV_COUNT\(4),
	datac => \inst7|ALT_INV_COUNT[2]~DUPLICATE_q\,
	datad => \inst7|ALT_INV_COUNT\(1),
	datae => \inst7|ALT_INV_COUNT\(0),
	dataf => \inst7|ALT_INV_COUNT[3]~DUPLICATE_q\,
	combout => \inst7|COUNT~4_combout\);

-- Location: FF_X87_Y35_N5
\inst7|COUNT[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(0));

-- Location: MLABCELL_X87_Y35_N15
\inst7|COUNT~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|COUNT~2_combout\ = ( \inst7|COUNT\(3) & ( \inst7|COUNT[4]~DUPLICATE_q\ & ( (!\inst7|COUNT[2]~DUPLICATE_q\) # (!\inst7|COUNT\(1)) ) ) ) # ( !\inst7|COUNT\(3) & ( \inst7|COUNT[4]~DUPLICATE_q\ & ( (\inst7|COUNT\(0) & (\inst7|COUNT[2]~DUPLICATE_q\ & 
-- \inst7|COUNT\(1))) ) ) ) # ( \inst7|COUNT\(3) & ( !\inst7|COUNT[4]~DUPLICATE_q\ & ( (!\inst7|COUNT\(0)) # ((!\inst7|COUNT[2]~DUPLICATE_q\) # (!\inst7|COUNT\(1))) ) ) ) # ( !\inst7|COUNT\(3) & ( !\inst7|COUNT[4]~DUPLICATE_q\ & ( (\inst7|COUNT\(0) & 
-- (\inst7|COUNT[2]~DUPLICATE_q\ & \inst7|COUNT\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101111111111111101000000000000001011111111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ALT_INV_COUNT\(0),
	datac => \inst7|ALT_INV_COUNT[2]~DUPLICATE_q\,
	datad => \inst7|ALT_INV_COUNT\(1),
	datae => \inst7|ALT_INV_COUNT\(3),
	dataf => \inst7|ALT_INV_COUNT[4]~DUPLICATE_q\,
	combout => \inst7|COUNT~2_combout\);

-- Location: FF_X87_Y35_N16
\inst7|COUNT[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT[3]~DUPLICATE_q\);

-- Location: FF_X87_Y35_N25
\inst7|COUNT[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|COUNT~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT[1]~DUPLICATE_q\);

-- Location: LABCELL_X83_Y35_N27
\inst7|DIVD~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst7|DIVD~0_combout\ = ( \inst7|COUNT[1]~DUPLICATE_q\ & ( !\inst7|DIVD~q\ $ (((!\inst7|COUNT[3]~DUPLICATE_q\) # ((!\inst7|COUNT[4]~DUPLICATE_q\) # (!\inst7|COUNT\(2))))) ) ) # ( !\inst7|COUNT[1]~DUPLICATE_q\ & ( \inst7|DIVD~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|ALT_INV_COUNT[3]~DUPLICATE_q\,
	datab => \inst7|ALT_INV_COUNT[4]~DUPLICATE_q\,
	datac => \inst7|ALT_INV_COUNT\(2),
	datad => \inst7|ALT_INV_DIVD~q\,
	dataf => \inst7|ALT_INV_COUNT[1]~DUPLICATE_q\,
	combout => \inst7|DIVD~0_combout\);

-- Location: FF_X83_Y35_N29
\inst7|DIVD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst7|DIVD~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|DIVD~q\);

-- Location: IOIBUF_X89_Y35_N78
\FRQ1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_FRQ1,
	o => \FRQ1~input_o\);

-- Location: LABCELL_X88_Y35_N33
\inst21|TMPNEG1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst21|TMPNEG1~0_combout\ = ( !\FRQ0~input_o\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \ALT_INV_FRQ0~input_o\,
	combout => \inst21|TMPNEG1~0_combout\);

-- Location: FF_X88_Y35_N35
\inst21|TMPNEG1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ1~input_o\,
	d => \inst21|TMPNEG1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst21|TMPNEG1~q\);

-- Location: FF_X88_Y35_N20
\inst21|TMPNEG0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \FRQ1~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst21|TMPNEG0~q\);

-- Location: LABCELL_X77_Y36_N42
\inst2|TSUM[0][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][31]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][31]~combout\);

-- Location: MLABCELL_X78_Y36_N45
\inst2|TSUM[0][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][30]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][30]~combout\);

-- Location: LABCELL_X77_Y36_N27
\inst2|TSUM[0][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][29]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][29]~combout\);

-- Location: MLABCELL_X78_Y36_N42
\inst2|TSUM[0][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][28]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][28]~combout\);

-- Location: LABCELL_X77_Y36_N6
\inst2|TSUM[0][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][27]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][27]~combout\);

-- Location: MLABCELL_X78_Y36_N51
\inst2|TSUM[0][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][26]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][26]~combout\);

-- Location: MLABCELL_X78_Y36_N54
\inst2|TSUM[0][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][25]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][25]~combout\);

-- Location: LABCELL_X77_Y36_N0
\inst2|TSUM[0][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][24]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][24]~combout\);

-- Location: LABCELL_X77_Y36_N54
\inst2|TSUM[0][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][23]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][23]~combout\);

-- Location: MLABCELL_X78_Y36_N48
\inst2|TSUM[0][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][22]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][22]~combout\);

-- Location: MLABCELL_X78_Y36_N57
\inst2|TSUM[0][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][21]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][21]~combout\);

-- Location: MLABCELL_X78_Y36_N39
\inst2|TSUM[0][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][20]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][20]~combout\);

-- Location: LABCELL_X79_Y37_N21
\inst2|TSUM[0][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][19]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][19]~combout\);

-- Location: LABCELL_X77_Y37_N57
\inst2|TSUM[0][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][18]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][18]~combout\);

-- Location: LABCELL_X77_Y37_N3
\inst2|TSUM[0][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][17]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][17]~combout\);

-- Location: LABCELL_X77_Y37_N12
\inst2|TSUM[0][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][16]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][16]~combout\);

-- Location: LABCELL_X77_Y37_N9
\inst2|TSUM[0][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][15]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][15]~combout\);

-- Location: MLABCELL_X78_Y38_N51
\inst2|TSUM[0][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][14]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][14]~combout\);

-- Location: LABCELL_X77_Y37_N24
\inst2|TSUM[0][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][13]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][13]~combout\);

-- Location: LABCELL_X77_Y37_N42
\inst2|TSUM[0][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][12]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][12]~combout\);

-- Location: LABCELL_X77_Y37_N51
\inst2|TSUM[0][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][11]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][11]~combout\);

-- Location: LABCELL_X77_Y37_N30
\inst2|TSUM[0][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][10]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][10]~combout\);

-- Location: LABCELL_X77_Y37_N33
\inst2|TSUM[0][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][9]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][9]~combout\);

-- Location: LABCELL_X77_Y37_N36
\inst2|TSUM[0][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][8]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][8]~combout\);

-- Location: LABCELL_X77_Y37_N39
\inst2|TSUM[0][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][7]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][7]~combout\);

-- Location: LABCELL_X77_Y37_N18
\inst2|TSUM[0][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][6]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \inst2|TSUM[0][6]~combout\);

-- Location: MLABCELL_X84_Y35_N45
\inst2|PSUM[1][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[1][0]~_wirecell_combout\ = !\inst2|PSUM[1][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[1][0]~q\,
	combout => \inst2|PSUM[1][0]~_wirecell_combout\);

-- Location: LABCELL_X88_Y35_N6
\inst6|STEMP[18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(18) = LCELL(( !\FRQ1~input_o\ & ( \FRQ0~input_o\ ) ) # ( \FRQ1~input_o\ & ( !\FRQ0~input_o\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \ALT_INV_FRQ1~input_o\,
	dataf => \ALT_INV_FRQ0~input_o\,
	combout => \inst6|STEMP\(18));

-- Location: LABCELL_X88_Y35_N51
\inst6|STEMP[17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(17) = LCELL(( !\inst6|STEMP\(18) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(18),
	combout => \inst6|STEMP\(17));

-- Location: LABCELL_X88_Y35_N30
\inst6|STEMP[16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(16) = LCELL(( !\inst6|STEMP\(17) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(17),
	combout => \inst6|STEMP\(16));

-- Location: MLABCELL_X84_Y35_N33
\inst6|STEMP[15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(15) = LCELL(( !\inst6|STEMP\(16) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(16),
	combout => \inst6|STEMP\(15));

-- Location: MLABCELL_X84_Y35_N9
\inst6|STEMP[14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(14) = LCELL(( !\inst6|STEMP\(15) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(15),
	combout => \inst6|STEMP\(14));

-- Location: MLABCELL_X84_Y35_N42
\inst6|STEMP[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(13) = LCELL(( !\inst6|STEMP\(14) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(14),
	combout => \inst6|STEMP\(13));

-- Location: MLABCELL_X84_Y35_N18
\inst6|STEMP[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(12) = LCELL(( !\inst6|STEMP\(13) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(13),
	combout => \inst6|STEMP\(12));

-- Location: MLABCELL_X84_Y35_N51
\inst6|STEMP[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(11) = LCELL(( !\inst6|STEMP\(12) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(12),
	combout => \inst6|STEMP\(11));

-- Location: LABCELL_X83_Y35_N42
\inst6|STEMP[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(10) = LCELL(( !\inst6|STEMP\(11) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(11),
	combout => \inst6|STEMP\(10));

-- Location: LABCELL_X83_Y35_N12
\inst6|STEMP[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(9) = LCELL(( !\inst6|STEMP\(10) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(10),
	combout => \inst6|STEMP\(9));

-- Location: LABCELL_X83_Y35_N33
\inst6|STEMP[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(8) = LCELL(( !\inst6|STEMP\(9) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(9),
	combout => \inst6|STEMP\(8));

-- Location: LABCELL_X83_Y35_N6
\inst6|STEMP[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(7) = LCELL(( !\inst6|STEMP\(8) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(8),
	combout => \inst6|STEMP\(7));

-- Location: LABCELL_X83_Y35_N57
\inst6|STEMP[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(6) = LCELL(( !\inst6|STEMP\(7) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(7),
	combout => \inst6|STEMP\(6));

-- Location: LABCELL_X83_Y35_N39
\inst6|STEMP[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(5) = LCELL(( !\inst6|STEMP\(6) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(6),
	combout => \inst6|STEMP\(5));

-- Location: LABCELL_X83_Y35_N51
\inst6|STEMP[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(4) = LCELL(( !\inst6|STEMP\(5) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(5),
	combout => \inst6|STEMP\(4));

-- Location: LABCELL_X83_Y35_N18
\inst6|STEMP[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(3) = LCELL(( !\inst6|STEMP\(4) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(4),
	combout => \inst6|STEMP\(3));

-- Location: LABCELL_X83_Y35_N9
\inst6|STEMP[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(2) = LCELL(( !\inst6|STEMP\(3) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(3),
	combout => \inst6|STEMP\(2));

-- Location: MLABCELL_X84_Y35_N0
\inst2|PSUM[1][0]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[1][0]~7_combout\ = ( \inst7|DIVD~q\ ) # ( !\inst7|DIVD~q\ & ( !\inst6|STEMP\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst6|ALT_INV_STEMP\(2),
	dataf => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[1][0]~7_combout\);

-- Location: FF_X84_Y35_N47
\inst2|PSUM[1][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|PSUM[1][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[1][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[1][0]~q\);

-- Location: MLABCELL_X84_Y35_N39
\inst2|Add16~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add16~4_combout\ = !\inst2|PSUM[1][0]~q\ $ (!\inst2|PSUM[1][1]~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110000000011111111000000001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[1][0]~q\,
	datad => \inst2|ALT_INV_PSUM[1][1]~q\,
	combout => \inst2|Add16~4_combout\);

-- Location: FF_X84_Y35_N41
\inst2|PSUM[1][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add16~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[1][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[1][1]~q\);

-- Location: LABCELL_X85_Y35_N39
\inst2|Add16~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add16~3_combout\ = ( \inst2|PSUM[1][1]~q\ & ( !\inst2|PSUM[1][0]~q\ $ (!\inst2|PSUM[1][2]~q\) ) ) # ( !\inst2|PSUM[1][1]~q\ & ( \inst2|PSUM[1][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[1][0]~q\,
	datad => \inst2|ALT_INV_PSUM[1][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[1][1]~q\,
	combout => \inst2|Add16~3_combout\);

-- Location: FF_X84_Y35_N56
\inst2|PSUM[1][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add16~3_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[1][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[1][2]~q\);

-- Location: MLABCELL_X84_Y35_N21
\inst2|Add16~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add16~2_combout\ = ( \inst2|PSUM[1][2]~q\ & ( !\inst2|PSUM[1][3]~q\ $ (((!\inst2|PSUM[1][0]~q\) # (!\inst2|PSUM[1][1]~q\))) ) ) # ( !\inst2|PSUM[1][2]~q\ & ( \inst2|PSUM[1][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000011111111000000001111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[1][0]~q\,
	datac => \inst2|ALT_INV_PSUM[1][1]~q\,
	datad => \inst2|ALT_INV_PSUM[1][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[1][2]~q\,
	combout => \inst2|Add16~2_combout\);

-- Location: FF_X84_Y35_N23
\inst2|PSUM[1][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add16~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[1][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[1][3]~q\);

-- Location: MLABCELL_X84_Y35_N36
\inst2|Add16~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add16~1_combout\ = ( \inst2|PSUM[1][1]~q\ & ( !\inst2|PSUM[1][4]~q\ $ (((!\inst2|PSUM[1][3]~q\) # ((!\inst2|PSUM[1][0]~q\) # (!\inst2|PSUM[1][2]~q\)))) ) ) # ( !\inst2|PSUM[1][1]~q\ & ( \inst2|PSUM[1][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[1][3]~q\,
	datab => \inst2|ALT_INV_PSUM[1][0]~q\,
	datac => \inst2|ALT_INV_PSUM[1][2]~q\,
	datad => \inst2|ALT_INV_PSUM[1][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[1][1]~q\,
	combout => \inst2|Add16~1_combout\);

-- Location: FF_X84_Y35_N38
\inst2|PSUM[1][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add16~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[1][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[1][4]~q\);

-- Location: LABCELL_X85_Y35_N3
\inst2|Add16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add16~0_combout\ = ( \inst2|PSUM[1][0]~q\ & ( \inst2|PSUM[1][3]~q\ & ( !\inst2|PSUM[1][5]~q\ $ (((!\inst2|PSUM[1][2]~q\) # ((!\inst2|PSUM[1][1]~q\) # (!\inst2|PSUM[1][4]~q\)))) ) ) ) # ( !\inst2|PSUM[1][0]~q\ & ( \inst2|PSUM[1][3]~q\ & ( 
-- \inst2|PSUM[1][5]~q\ ) ) ) # ( \inst2|PSUM[1][0]~q\ & ( !\inst2|PSUM[1][3]~q\ & ( \inst2|PSUM[1][5]~q\ ) ) ) # ( !\inst2|PSUM[1][0]~q\ & ( !\inst2|PSUM[1][3]~q\ & ( \inst2|PSUM[1][5]~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100011110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[1][2]~q\,
	datab => \inst2|ALT_INV_PSUM[1][1]~q\,
	datac => \inst2|ALT_INV_PSUM[1][5]~q\,
	datad => \inst2|ALT_INV_PSUM[1][4]~q\,
	datae => \inst2|ALT_INV_PSUM[1][0]~q\,
	dataf => \inst2|ALT_INV_PSUM[1][3]~q\,
	combout => \inst2|Add16~0_combout\);

-- Location: FF_X84_Y35_N59
\inst2|PSUM[1][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add16~0_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[1][0]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[1][5]~q\);

-- Location: MLABCELL_X82_Y35_N3
\inst2|PSUM[0][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[0][0]~_wirecell_combout\ = !\inst2|PSUM[0][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[0][0]~q\,
	combout => \inst2|PSUM[0][0]~_wirecell_combout\);

-- Location: LABCELL_X83_Y35_N24
\inst6|STEMP[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(1) = LCELL(( !\inst6|STEMP\(2) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(2),
	combout => \inst6|STEMP\(1));

-- Location: LABCELL_X83_Y35_N45
\inst6|STEMP[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(0) = LCELL(( !\inst6|STEMP\(1) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst6|ALT_INV_STEMP\(1),
	combout => \inst6|STEMP\(0));

-- Location: MLABCELL_X82_Y35_N0
\inst2|PSUM[0][5]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[0][5]~8_combout\ = ( \inst6|STEMP\(0) & ( \inst7|DIVD~q\ ) ) # ( !\inst6|STEMP\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst7|ALT_INV_DIVD~q\,
	dataf => \inst6|ALT_INV_STEMP\(0),
	combout => \inst2|PSUM[0][5]~8_combout\);

-- Location: FF_X82_Y35_N5
\inst2|PSUM[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|PSUM[0][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[0][5]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[0][0]~q\);

-- Location: MLABCELL_X82_Y35_N33
\inst2|Add17~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add17~4_combout\ = ( \inst2|PSUM[0][0]~q\ & ( !\inst2|PSUM[0][1]~q\ ) ) # ( !\inst2|PSUM[0][0]~q\ & ( \inst2|PSUM[0][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[0][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][0]~q\,
	combout => \inst2|Add17~4_combout\);

-- Location: FF_X82_Y35_N35
\inst2|PSUM[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add17~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[0][5]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[0][1]~q\);

-- Location: MLABCELL_X82_Y35_N30
\inst2|Add17~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add17~3_combout\ = ( \inst2|PSUM[0][1]~q\ & ( !\inst2|PSUM[0][0]~q\ $ (!\inst2|PSUM[0][2]~q\) ) ) # ( !\inst2|PSUM[0][1]~q\ & ( \inst2|PSUM[0][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[0][0]~q\,
	datad => \inst2|ALT_INV_PSUM[0][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][1]~q\,
	combout => \inst2|Add17~3_combout\);

-- Location: FF_X82_Y35_N32
\inst2|PSUM[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add17~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[0][5]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[0][2]~q\);

-- Location: MLABCELL_X82_Y35_N21
\inst2|Add17~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add17~2_combout\ = ( \inst2|PSUM[0][1]~q\ & ( !\inst2|PSUM[0][3]~q\ $ (((!\inst2|PSUM[0][0]~q\) # (!\inst2|PSUM[0][2]~q\))) ) ) # ( !\inst2|PSUM[0][1]~q\ & ( \inst2|PSUM[0][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[0][0]~q\,
	datac => \inst2|ALT_INV_PSUM[0][2]~q\,
	datad => \inst2|ALT_INV_PSUM[0][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][1]~q\,
	combout => \inst2|Add17~2_combout\);

-- Location: FF_X82_Y35_N23
\inst2|PSUM[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add17~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[0][5]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[0][3]~q\);

-- Location: MLABCELL_X82_Y35_N18
\inst2|Add17~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add17~1_combout\ = ( \inst2|PSUM[0][3]~q\ & ( !\inst2|PSUM[0][4]~q\ $ (((!\inst2|PSUM[0][0]~q\) # ((!\inst2|PSUM[0][2]~q\) # (!\inst2|PSUM[0][1]~q\)))) ) ) # ( !\inst2|PSUM[0][3]~q\ & ( \inst2|PSUM[0][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[0][0]~q\,
	datab => \inst2|ALT_INV_PSUM[0][2]~q\,
	datac => \inst2|ALT_INV_PSUM[0][1]~q\,
	datad => \inst2|ALT_INV_PSUM[0][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][3]~q\,
	combout => \inst2|Add17~1_combout\);

-- Location: FF_X82_Y35_N20
\inst2|PSUM[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add17~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[0][5]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[0][4]~q\);

-- Location: MLABCELL_X82_Y35_N24
\inst2|Add17~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add17~0_combout\ = ( \inst2|PSUM[0][5]~q\ & ( \inst2|PSUM[0][3]~q\ & ( (!\inst2|PSUM[0][1]~q\) # ((!\inst2|PSUM[0][0]~q\) # ((!\inst2|PSUM[0][4]~q\) # (!\inst2|PSUM[0][2]~q\))) ) ) ) # ( !\inst2|PSUM[0][5]~q\ & ( \inst2|PSUM[0][3]~q\ & ( 
-- (\inst2|PSUM[0][1]~q\ & (\inst2|PSUM[0][0]~q\ & (\inst2|PSUM[0][4]~q\ & \inst2|PSUM[0][2]~q\))) ) ) ) # ( \inst2|PSUM[0][5]~q\ & ( !\inst2|PSUM[0][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[0][1]~q\,
	datab => \inst2|ALT_INV_PSUM[0][0]~q\,
	datac => \inst2|ALT_INV_PSUM[0][4]~q\,
	datad => \inst2|ALT_INV_PSUM[0][2]~q\,
	datae => \inst2|ALT_INV_PSUM[0][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][3]~q\,
	combout => \inst2|Add17~0_combout\);

-- Location: FF_X82_Y35_N26
\inst2|PSUM[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add17~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[0][5]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[0][5]~q\);

-- Location: LABCELL_X81_Y35_N0
\inst2|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add0~21_sumout\ = SUM(( \inst2|PSUM[1][0]~q\ ) + ( \inst2|PSUM[0][0]~q\ ) + ( !VCC ))
-- \inst2|Add0~22\ = CARRY(( \inst2|PSUM[1][0]~q\ ) + ( \inst2|PSUM[0][0]~q\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[1][0]~q\,
	datac => \inst2|ALT_INV_PSUM[0][0]~q\,
	cin => GND,
	sumout => \inst2|Add0~21_sumout\,
	cout => \inst2|Add0~22\);

-- Location: LABCELL_X81_Y35_N3
\inst2|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add0~17_sumout\ = SUM(( \inst2|PSUM[1][1]~q\ ) + ( \inst2|PSUM[0][1]~q\ ) + ( \inst2|Add0~22\ ))
-- \inst2|Add0~18\ = CARRY(( \inst2|PSUM[1][1]~q\ ) + ( \inst2|PSUM[0][1]~q\ ) + ( \inst2|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[1][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][1]~q\,
	cin => \inst2|Add0~22\,
	sumout => \inst2|Add0~17_sumout\,
	cout => \inst2|Add0~18\);

-- Location: LABCELL_X81_Y35_N6
\inst2|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add0~13_sumout\ = SUM(( \inst2|PSUM[1][2]~q\ ) + ( \inst2|PSUM[0][2]~q\ ) + ( \inst2|Add0~18\ ))
-- \inst2|Add0~14\ = CARRY(( \inst2|PSUM[1][2]~q\ ) + ( \inst2|PSUM[0][2]~q\ ) + ( \inst2|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[0][2]~q\,
	datac => \inst2|ALT_INV_PSUM[1][2]~q\,
	cin => \inst2|Add0~18\,
	sumout => \inst2|Add0~13_sumout\,
	cout => \inst2|Add0~14\);

-- Location: LABCELL_X81_Y35_N9
\inst2|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add0~9_sumout\ = SUM(( \inst2|PSUM[1][3]~q\ ) + ( \inst2|PSUM[0][3]~q\ ) + ( \inst2|Add0~14\ ))
-- \inst2|Add0~10\ = CARRY(( \inst2|PSUM[1][3]~q\ ) + ( \inst2|PSUM[0][3]~q\ ) + ( \inst2|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[0][3]~q\,
	datac => \inst2|ALT_INV_PSUM[1][3]~q\,
	cin => \inst2|Add0~14\,
	sumout => \inst2|Add0~9_sumout\,
	cout => \inst2|Add0~10\);

-- Location: LABCELL_X81_Y35_N12
\inst2|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add0~5_sumout\ = SUM(( \inst2|PSUM[1][4]~q\ ) + ( \inst2|PSUM[0][4]~q\ ) + ( \inst2|Add0~10\ ))
-- \inst2|Add0~6\ = CARRY(( \inst2|PSUM[1][4]~q\ ) + ( \inst2|PSUM[0][4]~q\ ) + ( \inst2|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[0][4]~q\,
	datac => \inst2|ALT_INV_PSUM[1][4]~q\,
	cin => \inst2|Add0~10\,
	sumout => \inst2|Add0~5_sumout\,
	cout => \inst2|Add0~6\);

-- Location: LABCELL_X81_Y35_N15
\inst2|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add0~1_sumout\ = SUM(( \inst2|PSUM[1][5]~q\ ) + ( \inst2|PSUM[0][5]~q\ ) + ( \inst2|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[1][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[0][5]~q\,
	cin => \inst2|Add0~6\,
	sumout => \inst2|Add0~1_sumout\);

-- Location: LABCELL_X80_Y35_N6
\inst2|TSUM[0][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][5]~combout\ = LCELL(( \inst2|Add0~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add0~1_sumout\,
	combout => \inst2|TSUM[0][5]~combout\);

-- Location: LABCELL_X85_Y35_N21
\inst2|PSUM[2][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[2][0]~_wirecell_combout\ = ( !\inst2|PSUM[2][0]~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_PSUM[2][0]~q\,
	combout => \inst2|PSUM[2][0]~_wirecell_combout\);

-- Location: MLABCELL_X84_Y35_N15
\inst2|PSUM[2][3]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[2][3]~6_combout\ = ( \inst6|STEMP\(4) & ( \inst7|DIVD~q\ ) ) # ( !\inst6|STEMP\(4) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst7|ALT_INV_DIVD~q\,
	dataf => \inst6|ALT_INV_STEMP\(4),
	combout => \inst2|PSUM[2][3]~6_combout\);

-- Location: FF_X84_Y35_N17
\inst2|PSUM[2][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|PSUM[2][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[2][3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[2][0]~q\);

-- Location: LABCELL_X85_Y35_N57
\inst2|Add15~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add15~4_combout\ = ( \inst2|PSUM[2][0]~q\ & ( !\inst2|PSUM[2][1]~q\ ) ) # ( !\inst2|PSUM[2][0]~q\ & ( \inst2|PSUM[2][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[2][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[2][0]~q\,
	combout => \inst2|Add15~4_combout\);

-- Location: FF_X84_Y35_N26
\inst2|PSUM[2][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add15~4_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[2][3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[2][1]~q\);

-- Location: MLABCELL_X84_Y35_N3
\inst2|Add15~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add15~3_combout\ = !\inst2|PSUM[2][2]~q\ $ (((!\inst2|PSUM[2][1]~q\) # (!\inst2|PSUM[2][0]~q\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111111010000001011111101000000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[2][1]~q\,
	datac => \inst2|ALT_INV_PSUM[2][0]~q\,
	datad => \inst2|ALT_INV_PSUM[2][2]~q\,
	combout => \inst2|Add15~3_combout\);

-- Location: FF_X84_Y35_N5
\inst2|PSUM[2][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add15~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[2][3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[2][2]~q\);

-- Location: LABCELL_X85_Y35_N45
\inst2|Add15~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add15~2_combout\ = ( \inst2|PSUM[2][3]~q\ & ( (!\inst2|PSUM[2][2]~q\) # ((!\inst2|PSUM[2][0]~q\) # (!\inst2|PSUM[2][1]~q\)) ) ) # ( !\inst2|PSUM[2][3]~q\ & ( (\inst2|PSUM[2][2]~q\ & (\inst2|PSUM[2][0]~q\ & \inst2|PSUM[2][1]~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001111111111111111001111111111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[2][2]~q\,
	datac => \inst2|ALT_INV_PSUM[2][0]~q\,
	datad => \inst2|ALT_INV_PSUM[2][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[2][3]~q\,
	combout => \inst2|Add15~2_combout\);

-- Location: FF_X84_Y35_N32
\inst2|PSUM[2][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add15~2_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[2][3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[2][3]~q\);

-- Location: MLABCELL_X84_Y35_N24
\inst2|Add15~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add15~1_combout\ = ( \inst2|PSUM[2][3]~q\ & ( !\inst2|PSUM[2][4]~q\ $ (((!\inst2|PSUM[2][0]~q\) # ((!\inst2|PSUM[2][2]~q\) # (!\inst2|PSUM[2][1]~q\)))) ) ) # ( !\inst2|PSUM[2][3]~q\ & ( \inst2|PSUM[2][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001101100011001100110110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[2][0]~q\,
	datab => \inst2|ALT_INV_PSUM[2][4]~q\,
	datac => \inst2|ALT_INV_PSUM[2][2]~q\,
	datad => \inst2|ALT_INV_PSUM[2][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[2][3]~q\,
	combout => \inst2|Add15~1_combout\);

-- Location: FF_X84_Y35_N14
\inst2|PSUM[2][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add15~1_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[2][3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[2][4]~q\);

-- Location: MLABCELL_X84_Y35_N54
\inst2|Add15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add15~0_combout\ = ( \inst2|PSUM[2][5]~q\ & ( \inst2|PSUM[2][4]~q\ & ( (!\inst2|PSUM[2][1]~q\) # ((!\inst2|PSUM[2][0]~q\) # ((!\inst2|PSUM[2][2]~q\) # (!\inst2|PSUM[2][3]~q\))) ) ) ) # ( !\inst2|PSUM[2][5]~q\ & ( \inst2|PSUM[2][4]~q\ & ( 
-- (\inst2|PSUM[2][1]~q\ & (\inst2|PSUM[2][0]~q\ & (\inst2|PSUM[2][2]~q\ & \inst2|PSUM[2][3]~q\))) ) ) ) # ( \inst2|PSUM[2][5]~q\ & ( !\inst2|PSUM[2][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[2][1]~q\,
	datab => \inst2|ALT_INV_PSUM[2][0]~q\,
	datac => \inst2|ALT_INV_PSUM[2][2]~q\,
	datad => \inst2|ALT_INV_PSUM[2][3]~q\,
	datae => \inst2|ALT_INV_PSUM[2][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[2][4]~q\,
	combout => \inst2|Add15~0_combout\);

-- Location: FF_X84_Y35_N2
\inst2|PSUM[2][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add15~0_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[2][3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[2][5]~q\);

-- Location: LABCELL_X80_Y35_N27
\inst2|TSUM[0][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][4]~combout\ = LCELL(( \inst2|Add0~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add0~5_sumout\,
	combout => \inst2|TSUM[0][4]~combout\);

-- Location: LABCELL_X80_Y35_N33
\inst2|TSUM[0][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][3]~combout\ = LCELL(( \inst2|Add0~9_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add0~9_sumout\,
	combout => \inst2|TSUM[0][3]~combout\);

-- Location: LABCELL_X80_Y35_N0
\inst2|TSUM[0][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][2]~combout\ = LCELL(( \inst2|Add0~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add0~13_sumout\,
	combout => \inst2|TSUM[0][2]~combout\);

-- Location: LABCELL_X80_Y35_N54
\inst2|TSUM[0][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][1]~combout\ = LCELL(( \inst2|Add0~17_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add0~17_sumout\,
	combout => \inst2|TSUM[0][1]~combout\);

-- Location: LABCELL_X80_Y35_N51
\inst2|TSUM[0][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[0][0]~combout\ = LCELL(( \inst2|Add0~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add0~21_sumout\,
	combout => \inst2|TSUM[0][0]~combout\);

-- Location: MLABCELL_X78_Y37_N0
\inst2|Add1~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~125_sumout\ = SUM(( \inst2|PSUM[2][0]~q\ ) + ( \inst2|TSUM[0][0]~combout\ ) + ( !VCC ))
-- \inst2|Add1~126\ = CARRY(( \inst2|PSUM[2][0]~q\ ) + ( \inst2|TSUM[0][0]~combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[2][0]~q\,
	datac => \inst2|ALT_INV_TSUM[0][0]~combout\,
	cin => GND,
	sumout => \inst2|Add1~125_sumout\,
	cout => \inst2|Add1~126\);

-- Location: MLABCELL_X78_Y37_N3
\inst2|Add1~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~121_sumout\ = SUM(( \inst2|PSUM[2][1]~q\ ) + ( \inst2|TSUM[0][1]~combout\ ) + ( \inst2|Add1~126\ ))
-- \inst2|Add1~122\ = CARRY(( \inst2|PSUM[2][1]~q\ ) + ( \inst2|TSUM[0][1]~combout\ ) + ( \inst2|Add1~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[2][1]~q\,
	datac => \inst2|ALT_INV_TSUM[0][1]~combout\,
	cin => \inst2|Add1~126\,
	sumout => \inst2|Add1~121_sumout\,
	cout => \inst2|Add1~122\);

-- Location: MLABCELL_X78_Y37_N6
\inst2|Add1~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~117_sumout\ = SUM(( \inst2|PSUM[2][2]~q\ ) + ( \inst2|TSUM[0][2]~combout\ ) + ( \inst2|Add1~122\ ))
-- \inst2|Add1~118\ = CARRY(( \inst2|PSUM[2][2]~q\ ) + ( \inst2|TSUM[0][2]~combout\ ) + ( \inst2|Add1~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][2]~combout\,
	datad => \inst2|ALT_INV_PSUM[2][2]~q\,
	cin => \inst2|Add1~122\,
	sumout => \inst2|Add1~117_sumout\,
	cout => \inst2|Add1~118\);

-- Location: MLABCELL_X78_Y37_N9
\inst2|Add1~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~113_sumout\ = SUM(( \inst2|PSUM[2][3]~q\ ) + ( \inst2|TSUM[0][3]~combout\ ) + ( \inst2|Add1~118\ ))
-- \inst2|Add1~114\ = CARRY(( \inst2|PSUM[2][3]~q\ ) + ( \inst2|TSUM[0][3]~combout\ ) + ( \inst2|Add1~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][3]~combout\,
	datac => \inst2|ALT_INV_PSUM[2][3]~q\,
	cin => \inst2|Add1~118\,
	sumout => \inst2|Add1~113_sumout\,
	cout => \inst2|Add1~114\);

-- Location: MLABCELL_X78_Y37_N12
\inst2|Add1~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~109_sumout\ = SUM(( \inst2|PSUM[2][4]~q\ ) + ( \inst2|TSUM[0][4]~combout\ ) + ( \inst2|Add1~114\ ))
-- \inst2|Add1~110\ = CARRY(( \inst2|PSUM[2][4]~q\ ) + ( \inst2|TSUM[0][4]~combout\ ) + ( \inst2|Add1~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[2][4]~q\,
	dataf => \inst2|ALT_INV_TSUM[0][4]~combout\,
	cin => \inst2|Add1~114\,
	sumout => \inst2|Add1~109_sumout\,
	cout => \inst2|Add1~110\);

-- Location: MLABCELL_X78_Y37_N15
\inst2|Add1~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~105_sumout\ = SUM(( \inst2|TSUM[0][5]~combout\ ) + ( \inst2|PSUM[2][5]~q\ ) + ( \inst2|Add1~110\ ))
-- \inst2|Add1~106\ = CARRY(( \inst2|TSUM[0][5]~combout\ ) + ( \inst2|PSUM[2][5]~q\ ) + ( \inst2|Add1~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][5]~combout\,
	dataf => \inst2|ALT_INV_PSUM[2][5]~q\,
	cin => \inst2|Add1~110\,
	sumout => \inst2|Add1~105_sumout\,
	cout => \inst2|Add1~106\);

-- Location: MLABCELL_X78_Y37_N18
\inst2|Add1~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~101_sumout\ = SUM(( \inst2|TSUM[0][6]~combout\ ) + ( GND ) + ( \inst2|Add1~106\ ))
-- \inst2|Add1~102\ = CARRY(( \inst2|TSUM[0][6]~combout\ ) + ( GND ) + ( \inst2|Add1~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][6]~combout\,
	cin => \inst2|Add1~106\,
	sumout => \inst2|Add1~101_sumout\,
	cout => \inst2|Add1~102\);

-- Location: MLABCELL_X78_Y37_N21
\inst2|Add1~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~97_sumout\ = SUM(( \inst2|TSUM[0][7]~combout\ ) + ( GND ) + ( \inst2|Add1~102\ ))
-- \inst2|Add1~98\ = CARRY(( \inst2|TSUM[0][7]~combout\ ) + ( GND ) + ( \inst2|Add1~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][7]~combout\,
	cin => \inst2|Add1~102\,
	sumout => \inst2|Add1~97_sumout\,
	cout => \inst2|Add1~98\);

-- Location: MLABCELL_X78_Y37_N24
\inst2|Add1~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~93_sumout\ = SUM(( \inst2|TSUM[0][8]~combout\ ) + ( GND ) + ( \inst2|Add1~98\ ))
-- \inst2|Add1~94\ = CARRY(( \inst2|TSUM[0][8]~combout\ ) + ( GND ) + ( \inst2|Add1~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][8]~combout\,
	cin => \inst2|Add1~98\,
	sumout => \inst2|Add1~93_sumout\,
	cout => \inst2|Add1~94\);

-- Location: MLABCELL_X78_Y37_N27
\inst2|Add1~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~89_sumout\ = SUM(( \inst2|TSUM[0][9]~combout\ ) + ( GND ) + ( \inst2|Add1~94\ ))
-- \inst2|Add1~90\ = CARRY(( \inst2|TSUM[0][9]~combout\ ) + ( GND ) + ( \inst2|Add1~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][9]~combout\,
	cin => \inst2|Add1~94\,
	sumout => \inst2|Add1~89_sumout\,
	cout => \inst2|Add1~90\);

-- Location: MLABCELL_X78_Y37_N30
\inst2|Add1~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~85_sumout\ = SUM(( \inst2|TSUM[0][10]~combout\ ) + ( GND ) + ( \inst2|Add1~90\ ))
-- \inst2|Add1~86\ = CARRY(( \inst2|TSUM[0][10]~combout\ ) + ( GND ) + ( \inst2|Add1~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][10]~combout\,
	cin => \inst2|Add1~90\,
	sumout => \inst2|Add1~85_sumout\,
	cout => \inst2|Add1~86\);

-- Location: MLABCELL_X78_Y37_N33
\inst2|Add1~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~81_sumout\ = SUM(( \inst2|TSUM[0][11]~combout\ ) + ( GND ) + ( \inst2|Add1~86\ ))
-- \inst2|Add1~82\ = CARRY(( \inst2|TSUM[0][11]~combout\ ) + ( GND ) + ( \inst2|Add1~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][11]~combout\,
	cin => \inst2|Add1~86\,
	sumout => \inst2|Add1~81_sumout\,
	cout => \inst2|Add1~82\);

-- Location: MLABCELL_X78_Y37_N36
\inst2|Add1~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~77_sumout\ = SUM(( \inst2|TSUM[0][12]~combout\ ) + ( GND ) + ( \inst2|Add1~82\ ))
-- \inst2|Add1~78\ = CARRY(( \inst2|TSUM[0][12]~combout\ ) + ( GND ) + ( \inst2|Add1~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][12]~combout\,
	cin => \inst2|Add1~82\,
	sumout => \inst2|Add1~77_sumout\,
	cout => \inst2|Add1~78\);

-- Location: MLABCELL_X78_Y37_N39
\inst2|Add1~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~73_sumout\ = SUM(( \inst2|TSUM[0][13]~combout\ ) + ( GND ) + ( \inst2|Add1~78\ ))
-- \inst2|Add1~74\ = CARRY(( \inst2|TSUM[0][13]~combout\ ) + ( GND ) + ( \inst2|Add1~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][13]~combout\,
	cin => \inst2|Add1~78\,
	sumout => \inst2|Add1~73_sumout\,
	cout => \inst2|Add1~74\);

-- Location: MLABCELL_X78_Y37_N42
\inst2|Add1~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~69_sumout\ = SUM(( \inst2|TSUM[0][14]~combout\ ) + ( GND ) + ( \inst2|Add1~74\ ))
-- \inst2|Add1~70\ = CARRY(( \inst2|TSUM[0][14]~combout\ ) + ( GND ) + ( \inst2|Add1~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][14]~combout\,
	cin => \inst2|Add1~74\,
	sumout => \inst2|Add1~69_sumout\,
	cout => \inst2|Add1~70\);

-- Location: MLABCELL_X78_Y37_N45
\inst2|Add1~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~65_sumout\ = SUM(( \inst2|TSUM[0][15]~combout\ ) + ( GND ) + ( \inst2|Add1~70\ ))
-- \inst2|Add1~66\ = CARRY(( \inst2|TSUM[0][15]~combout\ ) + ( GND ) + ( \inst2|Add1~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][15]~combout\,
	cin => \inst2|Add1~70\,
	sumout => \inst2|Add1~65_sumout\,
	cout => \inst2|Add1~66\);

-- Location: MLABCELL_X78_Y37_N48
\inst2|Add1~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~61_sumout\ = SUM(( \inst2|TSUM[0][16]~combout\ ) + ( GND ) + ( \inst2|Add1~66\ ))
-- \inst2|Add1~62\ = CARRY(( \inst2|TSUM[0][16]~combout\ ) + ( GND ) + ( \inst2|Add1~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][16]~combout\,
	cin => \inst2|Add1~66\,
	sumout => \inst2|Add1~61_sumout\,
	cout => \inst2|Add1~62\);

-- Location: MLABCELL_X78_Y37_N51
\inst2|Add1~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~57_sumout\ = SUM(( \inst2|TSUM[0][17]~combout\ ) + ( GND ) + ( \inst2|Add1~62\ ))
-- \inst2|Add1~58\ = CARRY(( \inst2|TSUM[0][17]~combout\ ) + ( GND ) + ( \inst2|Add1~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][17]~combout\,
	cin => \inst2|Add1~62\,
	sumout => \inst2|Add1~57_sumout\,
	cout => \inst2|Add1~58\);

-- Location: MLABCELL_X78_Y37_N54
\inst2|Add1~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~53_sumout\ = SUM(( \inst2|TSUM[0][18]~combout\ ) + ( GND ) + ( \inst2|Add1~58\ ))
-- \inst2|Add1~54\ = CARRY(( \inst2|TSUM[0][18]~combout\ ) + ( GND ) + ( \inst2|Add1~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][18]~combout\,
	cin => \inst2|Add1~58\,
	sumout => \inst2|Add1~53_sumout\,
	cout => \inst2|Add1~54\);

-- Location: MLABCELL_X78_Y37_N57
\inst2|Add1~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~49_sumout\ = SUM(( \inst2|TSUM[0][19]~combout\ ) + ( GND ) + ( \inst2|Add1~54\ ))
-- \inst2|Add1~50\ = CARRY(( \inst2|TSUM[0][19]~combout\ ) + ( GND ) + ( \inst2|Add1~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][19]~combout\,
	cin => \inst2|Add1~54\,
	sumout => \inst2|Add1~49_sumout\,
	cout => \inst2|Add1~50\);

-- Location: MLABCELL_X78_Y36_N0
\inst2|Add1~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~45_sumout\ = SUM(( \inst2|TSUM[0][20]~combout\ ) + ( GND ) + ( \inst2|Add1~50\ ))
-- \inst2|Add1~46\ = CARRY(( \inst2|TSUM[0][20]~combout\ ) + ( GND ) + ( \inst2|Add1~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][20]~combout\,
	cin => \inst2|Add1~50\,
	sumout => \inst2|Add1~45_sumout\,
	cout => \inst2|Add1~46\);

-- Location: MLABCELL_X78_Y36_N3
\inst2|Add1~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~41_sumout\ = SUM(( \inst2|TSUM[0][21]~combout\ ) + ( GND ) + ( \inst2|Add1~46\ ))
-- \inst2|Add1~42\ = CARRY(( \inst2|TSUM[0][21]~combout\ ) + ( GND ) + ( \inst2|Add1~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][21]~combout\,
	cin => \inst2|Add1~46\,
	sumout => \inst2|Add1~41_sumout\,
	cout => \inst2|Add1~42\);

-- Location: MLABCELL_X78_Y36_N6
\inst2|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~37_sumout\ = SUM(( \inst2|TSUM[0][22]~combout\ ) + ( GND ) + ( \inst2|Add1~42\ ))
-- \inst2|Add1~38\ = CARRY(( \inst2|TSUM[0][22]~combout\ ) + ( GND ) + ( \inst2|Add1~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][22]~combout\,
	cin => \inst2|Add1~42\,
	sumout => \inst2|Add1~37_sumout\,
	cout => \inst2|Add1~38\);

-- Location: MLABCELL_X78_Y36_N9
\inst2|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~33_sumout\ = SUM(( \inst2|TSUM[0][23]~combout\ ) + ( GND ) + ( \inst2|Add1~38\ ))
-- \inst2|Add1~34\ = CARRY(( \inst2|TSUM[0][23]~combout\ ) + ( GND ) + ( \inst2|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][23]~combout\,
	cin => \inst2|Add1~38\,
	sumout => \inst2|Add1~33_sumout\,
	cout => \inst2|Add1~34\);

-- Location: MLABCELL_X78_Y36_N12
\inst2|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~29_sumout\ = SUM(( \inst2|TSUM[0][24]~combout\ ) + ( GND ) + ( \inst2|Add1~34\ ))
-- \inst2|Add1~30\ = CARRY(( \inst2|TSUM[0][24]~combout\ ) + ( GND ) + ( \inst2|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][24]~combout\,
	cin => \inst2|Add1~34\,
	sumout => \inst2|Add1~29_sumout\,
	cout => \inst2|Add1~30\);

-- Location: MLABCELL_X78_Y36_N15
\inst2|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~25_sumout\ = SUM(( \inst2|TSUM[0][25]~combout\ ) + ( GND ) + ( \inst2|Add1~30\ ))
-- \inst2|Add1~26\ = CARRY(( \inst2|TSUM[0][25]~combout\ ) + ( GND ) + ( \inst2|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][25]~combout\,
	cin => \inst2|Add1~30\,
	sumout => \inst2|Add1~25_sumout\,
	cout => \inst2|Add1~26\);

-- Location: MLABCELL_X78_Y36_N18
\inst2|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~21_sumout\ = SUM(( \inst2|TSUM[0][26]~combout\ ) + ( GND ) + ( \inst2|Add1~26\ ))
-- \inst2|Add1~22\ = CARRY(( \inst2|TSUM[0][26]~combout\ ) + ( GND ) + ( \inst2|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][26]~combout\,
	cin => \inst2|Add1~26\,
	sumout => \inst2|Add1~21_sumout\,
	cout => \inst2|Add1~22\);

-- Location: MLABCELL_X78_Y36_N21
\inst2|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~17_sumout\ = SUM(( \inst2|TSUM[0][27]~combout\ ) + ( GND ) + ( \inst2|Add1~22\ ))
-- \inst2|Add1~18\ = CARRY(( \inst2|TSUM[0][27]~combout\ ) + ( GND ) + ( \inst2|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][27]~combout\,
	cin => \inst2|Add1~22\,
	sumout => \inst2|Add1~17_sumout\,
	cout => \inst2|Add1~18\);

-- Location: MLABCELL_X78_Y36_N24
\inst2|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~13_sumout\ = SUM(( \inst2|TSUM[0][28]~combout\ ) + ( GND ) + ( \inst2|Add1~18\ ))
-- \inst2|Add1~14\ = CARRY(( \inst2|TSUM[0][28]~combout\ ) + ( GND ) + ( \inst2|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][28]~combout\,
	cin => \inst2|Add1~18\,
	sumout => \inst2|Add1~13_sumout\,
	cout => \inst2|Add1~14\);

-- Location: MLABCELL_X78_Y36_N27
\inst2|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~9_sumout\ = SUM(( \inst2|TSUM[0][29]~combout\ ) + ( GND ) + ( \inst2|Add1~14\ ))
-- \inst2|Add1~10\ = CARRY(( \inst2|TSUM[0][29]~combout\ ) + ( GND ) + ( \inst2|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[0][29]~combout\,
	cin => \inst2|Add1~14\,
	sumout => \inst2|Add1~9_sumout\,
	cout => \inst2|Add1~10\);

-- Location: MLABCELL_X78_Y36_N30
\inst2|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~5_sumout\ = SUM(( \inst2|TSUM[0][30]~combout\ ) + ( GND ) + ( \inst2|Add1~10\ ))
-- \inst2|Add1~6\ = CARRY(( \inst2|TSUM[0][30]~combout\ ) + ( GND ) + ( \inst2|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[0][30]~combout\,
	cin => \inst2|Add1~10\,
	sumout => \inst2|Add1~5_sumout\,
	cout => \inst2|Add1~6\);

-- Location: MLABCELL_X78_Y36_N33
\inst2|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add1~1_sumout\ = SUM(( \inst2|TSUM[0][31]~combout\ ) + ( GND ) + ( \inst2|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[0][31]~combout\,
	cin => \inst2|Add1~6\,
	sumout => \inst2|Add1~1_sumout\);

-- Location: MLABCELL_X78_Y34_N18
\inst2|TSUM[1][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][31]~combout\ = LCELL(( \inst2|Add1~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add1~1_sumout\,
	combout => \inst2|TSUM[1][31]~combout\);

-- Location: LABCELL_X77_Y34_N54
\inst2|TSUM[1][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][30]~combout\ = LCELL(\inst2|Add1~5_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add1~5_sumout\,
	combout => \inst2|TSUM[1][30]~combout\);

-- Location: LABCELL_X77_Y36_N48
\inst2|TSUM[1][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][29]~combout\ = LCELL(( \inst2|Add1~9_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add1~9_sumout\,
	combout => \inst2|TSUM[1][29]~combout\);

-- Location: MLABCELL_X78_Y34_N36
\inst2|TSUM[1][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][28]~combout\ = LCELL(( \inst2|Add1~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~13_sumout\,
	combout => \inst2|TSUM[1][28]~combout\);

-- Location: MLABCELL_X78_Y34_N9
\inst2|TSUM[1][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][27]~combout\ = LCELL(( \inst2|Add1~17_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~17_sumout\,
	combout => \inst2|TSUM[1][27]~combout\);

-- Location: LABCELL_X77_Y36_N33
\inst2|TSUM[1][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][26]~combout\ = LCELL(( \inst2|Add1~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~21_sumout\,
	combout => \inst2|TSUM[1][26]~combout\);

-- Location: LABCELL_X77_Y36_N15
\inst2|TSUM[1][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][25]~combout\ = LCELL(( \inst2|Add1~25_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add1~25_sumout\,
	combout => \inst2|TSUM[1][25]~combout\);

-- Location: LABCELL_X77_Y36_N18
\inst2|TSUM[1][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][24]~combout\ = LCELL(( \inst2|Add1~29_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~29_sumout\,
	combout => \inst2|TSUM[1][24]~combout\);

-- Location: MLABCELL_X78_Y36_N36
\inst2|TSUM[1][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][23]~combout\ = LCELL(( \inst2|Add1~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~33_sumout\,
	combout => \inst2|TSUM[1][23]~combout\);

-- Location: MLABCELL_X78_Y34_N48
\inst2|TSUM[1][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][22]~combout\ = LCELL(( \inst2|Add1~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~37_sumout\,
	combout => \inst2|TSUM[1][22]~combout\);

-- Location: LABCELL_X77_Y36_N39
\inst2|TSUM[1][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][21]~combout\ = LCELL(( \inst2|Add1~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add1~41_sumout\,
	combout => \inst2|TSUM[1][21]~combout\);

-- Location: LABCELL_X77_Y34_N57
\inst2|TSUM[1][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][20]~combout\ = LCELL(\inst2|Add1~45_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add1~45_sumout\,
	combout => \inst2|TSUM[1][20]~combout\);

-- Location: MLABCELL_X78_Y35_N15
\inst2|TSUM[1][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][19]~combout\ = LCELL(( \inst2|Add1~49_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~49_sumout\,
	combout => \inst2|TSUM[1][19]~combout\);

-- Location: LABCELL_X79_Y37_N36
\inst2|TSUM[1][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][18]~combout\ = LCELL(( \inst2|Add1~53_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~53_sumout\,
	combout => \inst2|TSUM[1][18]~combout\);

-- Location: MLABCELL_X78_Y35_N54
\inst2|TSUM[1][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][17]~combout\ = LCELL(( \inst2|Add1~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~57_sumout\,
	combout => \inst2|TSUM[1][17]~combout\);

-- Location: LABCELL_X75_Y35_N30
\inst2|TSUM[1][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][16]~combout\ = LCELL(\inst2|Add1~61_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add1~61_sumout\,
	combout => \inst2|TSUM[1][16]~combout\);

-- Location: LABCELL_X75_Y35_N12
\inst2|TSUM[1][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][15]~combout\ = LCELL(\inst2|Add1~65_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add1~65_sumout\,
	combout => \inst2|TSUM[1][15]~combout\);

-- Location: LABCELL_X75_Y35_N54
\inst2|TSUM[1][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][14]~combout\ = LCELL(( \inst2|Add1~69_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~69_sumout\,
	combout => \inst2|TSUM[1][14]~combout\);

-- Location: LABCELL_X75_Y35_N15
\inst2|TSUM[1][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][13]~combout\ = LCELL(\inst2|Add1~73_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add1~73_sumout\,
	combout => \inst2|TSUM[1][13]~combout\);

-- Location: LABCELL_X75_Y35_N48
\inst2|TSUM[1][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][12]~combout\ = LCELL(( \inst2|Add1~77_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add1~77_sumout\,
	combout => \inst2|TSUM[1][12]~combout\);

-- Location: LABCELL_X75_Y35_N57
\inst2|TSUM[1][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][11]~combout\ = LCELL(( \inst2|Add1~81_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~81_sumout\,
	combout => \inst2|TSUM[1][11]~combout\);

-- Location: LABCELL_X77_Y37_N15
\inst2|TSUM[1][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][10]~combout\ = LCELL(( \inst2|Add1~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~85_sumout\,
	combout => \inst2|TSUM[1][10]~combout\);

-- Location: LABCELL_X75_Y35_N33
\inst2|TSUM[1][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][9]~combout\ = LCELL(( \inst2|Add1~89_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~89_sumout\,
	combout => \inst2|TSUM[1][9]~combout\);

-- Location: LABCELL_X75_Y35_N21
\inst2|TSUM[1][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][8]~combout\ = LCELL(( \inst2|Add1~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add1~93_sumout\,
	combout => \inst2|TSUM[1][8]~combout\);

-- Location: LABCELL_X75_Y35_N36
\inst2|TSUM[1][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][7]~combout\ = LCELL(( \inst2|Add1~97_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~97_sumout\,
	combout => \inst2|TSUM[1][7]~combout\);

-- Location: LABCELL_X75_Y35_N6
\inst2|TSUM[1][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][6]~combout\ = LCELL(( \inst2|Add1~101_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~101_sumout\,
	combout => \inst2|TSUM[1][6]~combout\);

-- Location: LABCELL_X79_Y37_N33
\inst2|TSUM[1][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][5]~combout\ = LCELL(( \inst2|Add1~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~105_sumout\,
	combout => \inst2|TSUM[1][5]~combout\);

-- Location: LABCELL_X83_Y35_N54
\inst2|PSUM[3][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[3][0]~_wirecell_combout\ = !\inst2|PSUM[3][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[3][0]~q\,
	combout => \inst2|PSUM[3][0]~_wirecell_combout\);

-- Location: LABCELL_X83_Y35_N48
\inst2|PSUM[3][5]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[3][5]~5_combout\ = (!\inst6|STEMP\(6)) # (\inst7|DIVD~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101011111111101010101111111110101010111111111010101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|ALT_INV_STEMP\(6),
	datad => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[3][5]~5_combout\);

-- Location: FF_X83_Y35_N2
\inst2|PSUM[3][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|PSUM[3][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[3][5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[3][0]~q\);

-- Location: MLABCELL_X84_Y35_N27
\inst2|Add14~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add14~4_combout\ = ( \inst2|PSUM[3][0]~q\ & ( !\inst2|PSUM[3][1]~q\ ) ) # ( !\inst2|PSUM[3][0]~q\ & ( \inst2|PSUM[3][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[3][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[3][0]~q\,
	combout => \inst2|Add14~4_combout\);

-- Location: FF_X83_Y35_N38
\inst2|PSUM[3][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add14~4_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[3][5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[3][1]~q\);

-- Location: LABCELL_X83_Y35_N30
\inst2|Add14~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add14~3_combout\ = ( \inst2|PSUM[3][1]~q\ & ( !\inst2|PSUM[3][0]~q\ $ (!\inst2|PSUM[3][2]~q\) ) ) # ( !\inst2|PSUM[3][1]~q\ & ( \inst2|PSUM[3][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[3][0]~q\,
	datad => \inst2|ALT_INV_PSUM[3][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[3][1]~q\,
	combout => \inst2|Add14~3_combout\);

-- Location: FF_X83_Y35_N32
\inst2|PSUM[3][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add14~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[3][5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[3][2]~q\);

-- Location: LABCELL_X83_Y35_N21
\inst2|Add14~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add14~2_combout\ = ( \inst2|PSUM[3][1]~q\ & ( !\inst2|PSUM[3][3]~q\ $ (((!\inst2|PSUM[3][0]~q\) # (!\inst2|PSUM[3][2]~q\))) ) ) # ( !\inst2|PSUM[3][1]~q\ & ( \inst2|PSUM[3][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[3][0]~q\,
	datac => \inst2|ALT_INV_PSUM[3][2]~q\,
	datad => \inst2|ALT_INV_PSUM[3][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[3][1]~q\,
	combout => \inst2|Add14~2_combout\);

-- Location: FF_X83_Y35_N23
\inst2|PSUM[3][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add14~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[3][5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[3][3]~q\);

-- Location: LABCELL_X83_Y35_N15
\inst2|Add14~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add14~1_combout\ = ( \inst2|PSUM[3][3]~q\ & ( !\inst2|PSUM[3][4]~q\ $ (((!\inst2|PSUM[3][1]~q\) # ((!\inst2|PSUM[3][0]~q\) # (!\inst2|PSUM[3][2]~q\)))) ) ) # ( !\inst2|PSUM[3][3]~q\ & ( \inst2|PSUM[3][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[3][1]~q\,
	datab => \inst2|ALT_INV_PSUM[3][0]~q\,
	datac => \inst2|ALT_INV_PSUM[3][2]~q\,
	datad => \inst2|ALT_INV_PSUM[3][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[3][3]~q\,
	combout => \inst2|Add14~1_combout\);

-- Location: FF_X83_Y35_N17
\inst2|PSUM[3][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add14~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[3][5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[3][4]~q\);

-- Location: LABCELL_X83_Y35_N3
\inst2|Add14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add14~0_combout\ = ( \inst2|PSUM[3][5]~q\ & ( \inst2|PSUM[3][4]~q\ & ( (!\inst2|PSUM[3][0]~q\) # ((!\inst2|PSUM[3][1]~q\) # ((!\inst2|PSUM[3][2]~q\) # (!\inst2|PSUM[3][3]~q\))) ) ) ) # ( !\inst2|PSUM[3][5]~q\ & ( \inst2|PSUM[3][4]~q\ & ( 
-- (\inst2|PSUM[3][0]~q\ & (\inst2|PSUM[3][1]~q\ & (\inst2|PSUM[3][2]~q\ & \inst2|PSUM[3][3]~q\))) ) ) ) # ( \inst2|PSUM[3][5]~q\ & ( !\inst2|PSUM[3][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[3][0]~q\,
	datab => \inst2|ALT_INV_PSUM[3][1]~q\,
	datac => \inst2|ALT_INV_PSUM[3][2]~q\,
	datad => \inst2|ALT_INV_PSUM[3][3]~q\,
	datae => \inst2|ALT_INV_PSUM[3][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[3][4]~q\,
	combout => \inst2|Add14~0_combout\);

-- Location: FF_X83_Y35_N5
\inst2|PSUM[3][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add14~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[3][5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[3][5]~q\);

-- Location: LABCELL_X75_Y35_N24
\inst2|TSUM[1][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][4]~combout\ = LCELL(\inst2|Add1~109_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add1~109_sumout\,
	combout => \inst2|TSUM[1][4]~combout\);

-- Location: LABCELL_X75_Y35_N27
\inst2|TSUM[1][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][3]~combout\ = LCELL(( \inst2|Add1~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~113_sumout\,
	combout => \inst2|TSUM[1][3]~combout\);

-- Location: LABCELL_X75_Y35_N42
\inst2|TSUM[1][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][2]~combout\ = LCELL(( \inst2|Add1~117_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~117_sumout\,
	combout => \inst2|TSUM[1][2]~combout\);

-- Location: LABCELL_X81_Y37_N51
\inst2|TSUM[1][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][1]~combout\ = LCELL(( \inst2|Add1~121_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~121_sumout\,
	combout => \inst2|TSUM[1][1]~combout\);

-- Location: LABCELL_X79_Y37_N0
\inst2|TSUM[1][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[1][0]~combout\ = LCELL(( \inst2|Add1~125_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add1~125_sumout\,
	combout => \inst2|TSUM[1][0]~combout\);

-- Location: LABCELL_X77_Y35_N0
\inst2|Add2~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~125_sumout\ = SUM(( \inst2|TSUM[1][0]~combout\ ) + ( \inst2|PSUM[3][0]~q\ ) + ( !VCC ))
-- \inst2|Add2~126\ = CARRY(( \inst2|TSUM[1][0]~combout\ ) + ( \inst2|PSUM[3][0]~q\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][0]~combout\,
	dataf => \inst2|ALT_INV_PSUM[3][0]~q\,
	cin => GND,
	sumout => \inst2|Add2~125_sumout\,
	cout => \inst2|Add2~126\);

-- Location: LABCELL_X77_Y35_N3
\inst2|Add2~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~121_sumout\ = SUM(( \inst2|PSUM[3][1]~q\ ) + ( \inst2|TSUM[1][1]~combout\ ) + ( \inst2|Add2~126\ ))
-- \inst2|Add2~122\ = CARRY(( \inst2|PSUM[3][1]~q\ ) + ( \inst2|TSUM[1][1]~combout\ ) + ( \inst2|Add2~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[3][1]~q\,
	datac => \inst2|ALT_INV_TSUM[1][1]~combout\,
	cin => \inst2|Add2~126\,
	sumout => \inst2|Add2~121_sumout\,
	cout => \inst2|Add2~122\);

-- Location: LABCELL_X77_Y35_N6
\inst2|Add2~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~117_sumout\ = SUM(( \inst2|PSUM[3][2]~q\ ) + ( \inst2|TSUM[1][2]~combout\ ) + ( \inst2|Add2~122\ ))
-- \inst2|Add2~118\ = CARRY(( \inst2|PSUM[3][2]~q\ ) + ( \inst2|TSUM[1][2]~combout\ ) + ( \inst2|Add2~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[3][2]~q\,
	datac => \inst2|ALT_INV_TSUM[1][2]~combout\,
	cin => \inst2|Add2~122\,
	sumout => \inst2|Add2~117_sumout\,
	cout => \inst2|Add2~118\);

-- Location: LABCELL_X77_Y35_N9
\inst2|Add2~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~113_sumout\ = SUM(( \inst2|TSUM[1][3]~combout\ ) + ( \inst2|PSUM[3][3]~q\ ) + ( \inst2|Add2~118\ ))
-- \inst2|Add2~114\ = CARRY(( \inst2|TSUM[1][3]~combout\ ) + ( \inst2|PSUM[3][3]~q\ ) + ( \inst2|Add2~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][3]~combout\,
	dataf => \inst2|ALT_INV_PSUM[3][3]~q\,
	cin => \inst2|Add2~118\,
	sumout => \inst2|Add2~113_sumout\,
	cout => \inst2|Add2~114\);

-- Location: LABCELL_X77_Y35_N12
\inst2|Add2~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~109_sumout\ = SUM(( \inst2|TSUM[1][4]~combout\ ) + ( \inst2|PSUM[3][4]~q\ ) + ( \inst2|Add2~114\ ))
-- \inst2|Add2~110\ = CARRY(( \inst2|TSUM[1][4]~combout\ ) + ( \inst2|PSUM[3][4]~q\ ) + ( \inst2|Add2~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][4]~combout\,
	dataf => \inst2|ALT_INV_PSUM[3][4]~q\,
	cin => \inst2|Add2~114\,
	sumout => \inst2|Add2~109_sumout\,
	cout => \inst2|Add2~110\);

-- Location: LABCELL_X77_Y35_N15
\inst2|Add2~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~105_sumout\ = SUM(( \inst2|TSUM[1][5]~combout\ ) + ( \inst2|PSUM[3][5]~q\ ) + ( \inst2|Add2~110\ ))
-- \inst2|Add2~106\ = CARRY(( \inst2|TSUM[1][5]~combout\ ) + ( \inst2|PSUM[3][5]~q\ ) + ( \inst2|Add2~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][5]~combout\,
	dataf => \inst2|ALT_INV_PSUM[3][5]~q\,
	cin => \inst2|Add2~110\,
	sumout => \inst2|Add2~105_sumout\,
	cout => \inst2|Add2~106\);

-- Location: LABCELL_X77_Y35_N18
\inst2|Add2~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~101_sumout\ = SUM(( \inst2|TSUM[1][6]~combout\ ) + ( GND ) + ( \inst2|Add2~106\ ))
-- \inst2|Add2~102\ = CARRY(( \inst2|TSUM[1][6]~combout\ ) + ( GND ) + ( \inst2|Add2~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][6]~combout\,
	cin => \inst2|Add2~106\,
	sumout => \inst2|Add2~101_sumout\,
	cout => \inst2|Add2~102\);

-- Location: LABCELL_X77_Y35_N21
\inst2|Add2~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~97_sumout\ = SUM(( \inst2|TSUM[1][7]~combout\ ) + ( GND ) + ( \inst2|Add2~102\ ))
-- \inst2|Add2~98\ = CARRY(( \inst2|TSUM[1][7]~combout\ ) + ( GND ) + ( \inst2|Add2~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][7]~combout\,
	cin => \inst2|Add2~102\,
	sumout => \inst2|Add2~97_sumout\,
	cout => \inst2|Add2~98\);

-- Location: LABCELL_X77_Y35_N24
\inst2|Add2~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~93_sumout\ = SUM(( \inst2|TSUM[1][8]~combout\ ) + ( GND ) + ( \inst2|Add2~98\ ))
-- \inst2|Add2~94\ = CARRY(( \inst2|TSUM[1][8]~combout\ ) + ( GND ) + ( \inst2|Add2~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][8]~combout\,
	cin => \inst2|Add2~98\,
	sumout => \inst2|Add2~93_sumout\,
	cout => \inst2|Add2~94\);

-- Location: LABCELL_X77_Y35_N27
\inst2|Add2~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~89_sumout\ = SUM(( \inst2|TSUM[1][9]~combout\ ) + ( GND ) + ( \inst2|Add2~94\ ))
-- \inst2|Add2~90\ = CARRY(( \inst2|TSUM[1][9]~combout\ ) + ( GND ) + ( \inst2|Add2~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][9]~combout\,
	cin => \inst2|Add2~94\,
	sumout => \inst2|Add2~89_sumout\,
	cout => \inst2|Add2~90\);

-- Location: LABCELL_X77_Y35_N30
\inst2|Add2~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~85_sumout\ = SUM(( \inst2|TSUM[1][10]~combout\ ) + ( GND ) + ( \inst2|Add2~90\ ))
-- \inst2|Add2~86\ = CARRY(( \inst2|TSUM[1][10]~combout\ ) + ( GND ) + ( \inst2|Add2~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][10]~combout\,
	cin => \inst2|Add2~90\,
	sumout => \inst2|Add2~85_sumout\,
	cout => \inst2|Add2~86\);

-- Location: LABCELL_X77_Y35_N33
\inst2|Add2~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~81_sumout\ = SUM(( \inst2|TSUM[1][11]~combout\ ) + ( GND ) + ( \inst2|Add2~86\ ))
-- \inst2|Add2~82\ = CARRY(( \inst2|TSUM[1][11]~combout\ ) + ( GND ) + ( \inst2|Add2~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][11]~combout\,
	cin => \inst2|Add2~86\,
	sumout => \inst2|Add2~81_sumout\,
	cout => \inst2|Add2~82\);

-- Location: LABCELL_X77_Y35_N36
\inst2|Add2~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~77_sumout\ = SUM(( \inst2|TSUM[1][12]~combout\ ) + ( GND ) + ( \inst2|Add2~82\ ))
-- \inst2|Add2~78\ = CARRY(( \inst2|TSUM[1][12]~combout\ ) + ( GND ) + ( \inst2|Add2~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][12]~combout\,
	cin => \inst2|Add2~82\,
	sumout => \inst2|Add2~77_sumout\,
	cout => \inst2|Add2~78\);

-- Location: LABCELL_X77_Y35_N39
\inst2|Add2~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~73_sumout\ = SUM(( \inst2|TSUM[1][13]~combout\ ) + ( GND ) + ( \inst2|Add2~78\ ))
-- \inst2|Add2~74\ = CARRY(( \inst2|TSUM[1][13]~combout\ ) + ( GND ) + ( \inst2|Add2~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][13]~combout\,
	cin => \inst2|Add2~78\,
	sumout => \inst2|Add2~73_sumout\,
	cout => \inst2|Add2~74\);

-- Location: LABCELL_X77_Y35_N42
\inst2|Add2~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~69_sumout\ = SUM(( \inst2|TSUM[1][14]~combout\ ) + ( GND ) + ( \inst2|Add2~74\ ))
-- \inst2|Add2~70\ = CARRY(( \inst2|TSUM[1][14]~combout\ ) + ( GND ) + ( \inst2|Add2~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][14]~combout\,
	cin => \inst2|Add2~74\,
	sumout => \inst2|Add2~69_sumout\,
	cout => \inst2|Add2~70\);

-- Location: LABCELL_X77_Y35_N45
\inst2|Add2~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~65_sumout\ = SUM(( \inst2|TSUM[1][15]~combout\ ) + ( GND ) + ( \inst2|Add2~70\ ))
-- \inst2|Add2~66\ = CARRY(( \inst2|TSUM[1][15]~combout\ ) + ( GND ) + ( \inst2|Add2~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][15]~combout\,
	cin => \inst2|Add2~70\,
	sumout => \inst2|Add2~65_sumout\,
	cout => \inst2|Add2~66\);

-- Location: LABCELL_X77_Y35_N48
\inst2|Add2~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~61_sumout\ = SUM(( \inst2|TSUM[1][16]~combout\ ) + ( GND ) + ( \inst2|Add2~66\ ))
-- \inst2|Add2~62\ = CARRY(( \inst2|TSUM[1][16]~combout\ ) + ( GND ) + ( \inst2|Add2~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][16]~combout\,
	cin => \inst2|Add2~66\,
	sumout => \inst2|Add2~61_sumout\,
	cout => \inst2|Add2~62\);

-- Location: LABCELL_X77_Y35_N51
\inst2|Add2~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~57_sumout\ = SUM(( \inst2|TSUM[1][17]~combout\ ) + ( GND ) + ( \inst2|Add2~62\ ))
-- \inst2|Add2~58\ = CARRY(( \inst2|TSUM[1][17]~combout\ ) + ( GND ) + ( \inst2|Add2~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][17]~combout\,
	cin => \inst2|Add2~62\,
	sumout => \inst2|Add2~57_sumout\,
	cout => \inst2|Add2~58\);

-- Location: LABCELL_X77_Y35_N54
\inst2|Add2~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~53_sumout\ = SUM(( \inst2|TSUM[1][18]~combout\ ) + ( GND ) + ( \inst2|Add2~58\ ))
-- \inst2|Add2~54\ = CARRY(( \inst2|TSUM[1][18]~combout\ ) + ( GND ) + ( \inst2|Add2~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][18]~combout\,
	cin => \inst2|Add2~58\,
	sumout => \inst2|Add2~53_sumout\,
	cout => \inst2|Add2~54\);

-- Location: LABCELL_X77_Y35_N57
\inst2|Add2~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~49_sumout\ = SUM(( \inst2|TSUM[1][19]~combout\ ) + ( GND ) + ( \inst2|Add2~54\ ))
-- \inst2|Add2~50\ = CARRY(( \inst2|TSUM[1][19]~combout\ ) + ( GND ) + ( \inst2|Add2~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][19]~combout\,
	cin => \inst2|Add2~54\,
	sumout => \inst2|Add2~49_sumout\,
	cout => \inst2|Add2~50\);

-- Location: LABCELL_X77_Y34_N0
\inst2|Add2~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~45_sumout\ = SUM(( \inst2|TSUM[1][20]~combout\ ) + ( GND ) + ( \inst2|Add2~50\ ))
-- \inst2|Add2~46\ = CARRY(( \inst2|TSUM[1][20]~combout\ ) + ( GND ) + ( \inst2|Add2~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][20]~combout\,
	cin => \inst2|Add2~50\,
	sumout => \inst2|Add2~45_sumout\,
	cout => \inst2|Add2~46\);

-- Location: LABCELL_X77_Y34_N3
\inst2|Add2~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~41_sumout\ = SUM(( \inst2|TSUM[1][21]~combout\ ) + ( GND ) + ( \inst2|Add2~46\ ))
-- \inst2|Add2~42\ = CARRY(( \inst2|TSUM[1][21]~combout\ ) + ( GND ) + ( \inst2|Add2~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][21]~combout\,
	cin => \inst2|Add2~46\,
	sumout => \inst2|Add2~41_sumout\,
	cout => \inst2|Add2~42\);

-- Location: LABCELL_X77_Y34_N6
\inst2|Add2~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~37_sumout\ = SUM(( \inst2|TSUM[1][22]~combout\ ) + ( GND ) + ( \inst2|Add2~42\ ))
-- \inst2|Add2~38\ = CARRY(( \inst2|TSUM[1][22]~combout\ ) + ( GND ) + ( \inst2|Add2~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][22]~combout\,
	cin => \inst2|Add2~42\,
	sumout => \inst2|Add2~37_sumout\,
	cout => \inst2|Add2~38\);

-- Location: LABCELL_X77_Y34_N9
\inst2|Add2~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~33_sumout\ = SUM(( \inst2|TSUM[1][23]~combout\ ) + ( GND ) + ( \inst2|Add2~38\ ))
-- \inst2|Add2~34\ = CARRY(( \inst2|TSUM[1][23]~combout\ ) + ( GND ) + ( \inst2|Add2~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][23]~combout\,
	cin => \inst2|Add2~38\,
	sumout => \inst2|Add2~33_sumout\,
	cout => \inst2|Add2~34\);

-- Location: LABCELL_X77_Y34_N12
\inst2|Add2~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~29_sumout\ = SUM(( \inst2|TSUM[1][24]~combout\ ) + ( GND ) + ( \inst2|Add2~34\ ))
-- \inst2|Add2~30\ = CARRY(( \inst2|TSUM[1][24]~combout\ ) + ( GND ) + ( \inst2|Add2~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][24]~combout\,
	cin => \inst2|Add2~34\,
	sumout => \inst2|Add2~29_sumout\,
	cout => \inst2|Add2~30\);

-- Location: LABCELL_X77_Y34_N15
\inst2|Add2~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~25_sumout\ = SUM(( \inst2|TSUM[1][25]~combout\ ) + ( GND ) + ( \inst2|Add2~30\ ))
-- \inst2|Add2~26\ = CARRY(( \inst2|TSUM[1][25]~combout\ ) + ( GND ) + ( \inst2|Add2~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][25]~combout\,
	cin => \inst2|Add2~30\,
	sumout => \inst2|Add2~25_sumout\,
	cout => \inst2|Add2~26\);

-- Location: LABCELL_X77_Y34_N18
\inst2|Add2~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~21_sumout\ = SUM(( \inst2|TSUM[1][26]~combout\ ) + ( GND ) + ( \inst2|Add2~26\ ))
-- \inst2|Add2~22\ = CARRY(( \inst2|TSUM[1][26]~combout\ ) + ( GND ) + ( \inst2|Add2~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][26]~combout\,
	cin => \inst2|Add2~26\,
	sumout => \inst2|Add2~21_sumout\,
	cout => \inst2|Add2~22\);

-- Location: LABCELL_X77_Y34_N21
\inst2|Add2~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~17_sumout\ = SUM(( \inst2|TSUM[1][27]~combout\ ) + ( GND ) + ( \inst2|Add2~22\ ))
-- \inst2|Add2~18\ = CARRY(( \inst2|TSUM[1][27]~combout\ ) + ( GND ) + ( \inst2|Add2~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[1][27]~combout\,
	cin => \inst2|Add2~22\,
	sumout => \inst2|Add2~17_sumout\,
	cout => \inst2|Add2~18\);

-- Location: LABCELL_X77_Y34_N24
\inst2|Add2~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~13_sumout\ = SUM(( \inst2|TSUM[1][28]~combout\ ) + ( GND ) + ( \inst2|Add2~18\ ))
-- \inst2|Add2~14\ = CARRY(( \inst2|TSUM[1][28]~combout\ ) + ( GND ) + ( \inst2|Add2~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[1][28]~combout\,
	cin => \inst2|Add2~18\,
	sumout => \inst2|Add2~13_sumout\,
	cout => \inst2|Add2~14\);

-- Location: LABCELL_X77_Y34_N27
\inst2|Add2~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~9_sumout\ = SUM(( \inst2|TSUM[1][29]~combout\ ) + ( GND ) + ( \inst2|Add2~14\ ))
-- \inst2|Add2~10\ = CARRY(( \inst2|TSUM[1][29]~combout\ ) + ( GND ) + ( \inst2|Add2~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][29]~combout\,
	cin => \inst2|Add2~14\,
	sumout => \inst2|Add2~9_sumout\,
	cout => \inst2|Add2~10\);

-- Location: LABCELL_X77_Y34_N30
\inst2|Add2~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~5_sumout\ = SUM(( \inst2|TSUM[1][30]~combout\ ) + ( GND ) + ( \inst2|Add2~10\ ))
-- \inst2|Add2~6\ = CARRY(( \inst2|TSUM[1][30]~combout\ ) + ( GND ) + ( \inst2|Add2~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][30]~combout\,
	cin => \inst2|Add2~10\,
	sumout => \inst2|Add2~5_sumout\,
	cout => \inst2|Add2~6\);

-- Location: LABCELL_X77_Y34_N33
\inst2|Add2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add2~1_sumout\ = SUM(( \inst2|TSUM[1][31]~combout\ ) + ( GND ) + ( \inst2|Add2~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[1][31]~combout\,
	cin => \inst2|Add2~6\,
	sumout => \inst2|Add2~1_sumout\);

-- Location: LABCELL_X77_Y34_N48
\inst2|TSUM[2][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][31]~combout\ = LCELL(( \inst2|Add2~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~1_sumout\,
	combout => \inst2|TSUM[2][31]~combout\);

-- Location: LABCELL_X77_Y34_N45
\inst2|TSUM[2][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][30]~combout\ = LCELL(( \inst2|Add2~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~5_sumout\,
	combout => \inst2|TSUM[2][30]~combout\);

-- Location: LABCELL_X77_Y34_N36
\inst2|TSUM[2][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][29]~combout\ = LCELL(\inst2|Add2~9_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add2~9_sumout\,
	combout => \inst2|TSUM[2][29]~combout\);

-- Location: LABCELL_X77_Y32_N51
\inst2|TSUM[2][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][28]~combout\ = LCELL(\inst2|Add2~13_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add2~13_sumout\,
	combout => \inst2|TSUM[2][28]~combout\);

-- Location: LABCELL_X77_Y32_N48
\inst2|TSUM[2][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][27]~combout\ = LCELL(\inst2|Add2~17_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add2~17_sumout\,
	combout => \inst2|TSUM[2][27]~combout\);

-- Location: LABCELL_X77_Y32_N45
\inst2|TSUM[2][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][26]~combout\ = LCELL(( \inst2|Add2~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~21_sumout\,
	combout => \inst2|TSUM[2][26]~combout\);

-- Location: LABCELL_X77_Y34_N39
\inst2|TSUM[2][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][25]~combout\ = LCELL(( \inst2|Add2~25_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~25_sumout\,
	combout => \inst2|TSUM[2][25]~combout\);

-- Location: LABCELL_X77_Y32_N39
\inst2|TSUM[2][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][24]~combout\ = LCELL(( \inst2|Add2~29_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~29_sumout\,
	combout => \inst2|TSUM[2][24]~combout\);

-- Location: LABCELL_X77_Y34_N42
\inst2|TSUM[2][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][23]~combout\ = LCELL(( \inst2|Add2~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~33_sumout\,
	combout => \inst2|TSUM[2][23]~combout\);

-- Location: MLABCELL_X78_Y32_N54
\inst2|TSUM[2][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][22]~combout\ = LCELL(( \inst2|Add2~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~37_sumout\,
	combout => \inst2|TSUM[2][22]~combout\);

-- Location: LABCELL_X77_Y32_N54
\inst2|TSUM[2][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][21]~combout\ = LCELL(( \inst2|Add2~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~41_sumout\,
	combout => \inst2|TSUM[2][21]~combout\);

-- Location: LABCELL_X77_Y34_N51
\inst2|TSUM[2][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][20]~combout\ = LCELL(\inst2|Add2~45_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add2~45_sumout\,
	combout => \inst2|TSUM[2][20]~combout\);

-- Location: LABCELL_X75_Y33_N18
\inst2|TSUM[2][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][19]~combout\ = LCELL(( \inst2|Add2~49_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~49_sumout\,
	combout => \inst2|TSUM[2][19]~combout\);

-- Location: LABCELL_X75_Y33_N15
\inst2|TSUM[2][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][18]~combout\ = LCELL(( \inst2|Add2~53_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~53_sumout\,
	combout => \inst2|TSUM[2][18]~combout\);

-- Location: MLABCELL_X78_Y35_N39
\inst2|TSUM[2][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][17]~combout\ = LCELL(( \inst2|Add2~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~57_sumout\,
	combout => \inst2|TSUM[2][17]~combout\);

-- Location: LABCELL_X74_Y33_N15
\inst2|TSUM[2][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][16]~combout\ = LCELL(( \inst2|Add2~61_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~61_sumout\,
	combout => \inst2|TSUM[2][16]~combout\);

-- Location: LABCELL_X74_Y33_N6
\inst2|TSUM[2][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][15]~combout\ = LCELL(( \inst2|Add2~65_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~65_sumout\,
	combout => \inst2|TSUM[2][15]~combout\);

-- Location: LABCELL_X74_Y33_N24
\inst2|TSUM[2][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][14]~combout\ = LCELL(( \inst2|Add2~69_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~69_sumout\,
	combout => \inst2|TSUM[2][14]~combout\);

-- Location: LABCELL_X74_Y33_N57
\inst2|TSUM[2][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][13]~combout\ = LCELL(( \inst2|Add2~73_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~73_sumout\,
	combout => \inst2|TSUM[2][13]~combout\);

-- Location: LABCELL_X75_Y33_N33
\inst2|TSUM[2][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][12]~combout\ = LCELL(( \inst2|Add2~77_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~77_sumout\,
	combout => \inst2|TSUM[2][12]~combout\);

-- Location: LABCELL_X75_Y33_N51
\inst2|TSUM[2][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][11]~combout\ = LCELL(( \inst2|Add2~81_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~81_sumout\,
	combout => \inst2|TSUM[2][11]~combout\);

-- Location: MLABCELL_X78_Y35_N18
\inst2|TSUM[2][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][10]~combout\ = LCELL(( \inst2|Add2~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~85_sumout\,
	combout => \inst2|TSUM[2][10]~combout\);

-- Location: LABCELL_X74_Y33_N39
\inst2|TSUM[2][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][9]~combout\ = LCELL(( \inst2|Add2~89_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~89_sumout\,
	combout => \inst2|TSUM[2][9]~combout\);

-- Location: MLABCELL_X78_Y35_N48
\inst2|TSUM[2][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][8]~combout\ = LCELL(( \inst2|Add2~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~93_sumout\,
	combout => \inst2|TSUM[2][8]~combout\);

-- Location: LABCELL_X75_Y33_N9
\inst2|TSUM[2][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][7]~combout\ = LCELL(( \inst2|Add2~97_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~97_sumout\,
	combout => \inst2|TSUM[2][7]~combout\);

-- Location: MLABCELL_X78_Y35_N33
\inst2|TSUM[2][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][6]~combout\ = LCELL(( \inst2|Add2~101_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~101_sumout\,
	combout => \inst2|TSUM[2][6]~combout\);

-- Location: LABCELL_X85_Y35_N48
\inst2|PSUM[4][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[4][0]~_wirecell_combout\ = !\inst2|PSUM[4][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[4][0]~q\,
	combout => \inst2|PSUM[4][0]~_wirecell_combout\);

-- Location: MLABCELL_X84_Y35_N48
\inst2|PSUM[4][5]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[4][5]~4_combout\ = ( \inst7|DIVD~q\ ) # ( !\inst7|DIVD~q\ & ( !\inst6|STEMP\(8) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst6|ALT_INV_STEMP\(8),
	dataf => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[4][5]~4_combout\);

-- Location: FF_X85_Y35_N50
\inst2|PSUM[4][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|PSUM[4][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[4][5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[4][0]~q\);

-- Location: LABCELL_X85_Y35_N36
\inst2|Add13~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add13~4_combout\ = ( \inst2|PSUM[4][0]~q\ & ( !\inst2|PSUM[4][1]~q\ ) ) # ( !\inst2|PSUM[4][0]~q\ & ( \inst2|PSUM[4][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[4][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[4][0]~q\,
	combout => \inst2|Add13~4_combout\);

-- Location: FF_X85_Y35_N38
\inst2|PSUM[4][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add13~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[4][5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[4][1]~q\);

-- Location: LABCELL_X85_Y35_N42
\inst2|Add13~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add13~3_combout\ = ( \inst2|PSUM[4][1]~q\ & ( !\inst2|PSUM[4][0]~q\ $ (!\inst2|PSUM[4][2]~q\) ) ) # ( !\inst2|PSUM[4][1]~q\ & ( \inst2|PSUM[4][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[4][0]~q\,
	datad => \inst2|ALT_INV_PSUM[4][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[4][1]~q\,
	combout => \inst2|Add13~3_combout\);

-- Location: FF_X85_Y35_N44
\inst2|PSUM[4][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add13~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[4][5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[4][2]~q\);

-- Location: LABCELL_X85_Y35_N51
\inst2|Add13~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add13~2_combout\ = ( \inst2|PSUM[4][0]~q\ & ( !\inst2|PSUM[4][3]~q\ $ (((!\inst2|PSUM[4][1]~q\) # (!\inst2|PSUM[4][2]~q\))) ) ) # ( !\inst2|PSUM[4][0]~q\ & ( \inst2|PSUM[4][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[4][1]~q\,
	datac => \inst2|ALT_INV_PSUM[4][2]~q\,
	datad => \inst2|ALT_INV_PSUM[4][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[4][0]~q\,
	combout => \inst2|Add13~2_combout\);

-- Location: FF_X85_Y35_N53
\inst2|PSUM[4][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add13~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[4][5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[4][3]~q\);

-- Location: LABCELL_X85_Y35_N54
\inst2|Add13~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add13~1_combout\ = ( \inst2|PSUM[4][1]~q\ & ( !\inst2|PSUM[4][4]~q\ $ (((!\inst2|PSUM[4][0]~q\) # ((!\inst2|PSUM[4][2]~q\) # (!\inst2|PSUM[4][3]~q\)))) ) ) # ( !\inst2|PSUM[4][1]~q\ & ( \inst2|PSUM[4][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[4][0]~q\,
	datab => \inst2|ALT_INV_PSUM[4][2]~q\,
	datac => \inst2|ALT_INV_PSUM[4][3]~q\,
	datad => \inst2|ALT_INV_PSUM[4][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[4][1]~q\,
	combout => \inst2|Add13~1_combout\);

-- Location: FF_X85_Y35_N56
\inst2|PSUM[4][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add13~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[4][5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[4][4]~q\);

-- Location: LABCELL_X85_Y35_N12
\inst2|Add13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add13~0_combout\ = ( \inst2|PSUM[4][5]~q\ & ( \inst2|PSUM[4][4]~q\ & ( (!\inst2|PSUM[4][1]~q\) # ((!\inst2|PSUM[4][0]~q\) # ((!\inst2|PSUM[4][3]~q\) # (!\inst2|PSUM[4][2]~q\))) ) ) ) # ( !\inst2|PSUM[4][5]~q\ & ( \inst2|PSUM[4][4]~q\ & ( 
-- (\inst2|PSUM[4][1]~q\ & (\inst2|PSUM[4][0]~q\ & (\inst2|PSUM[4][3]~q\ & \inst2|PSUM[4][2]~q\))) ) ) ) # ( \inst2|PSUM[4][5]~q\ & ( !\inst2|PSUM[4][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[4][1]~q\,
	datab => \inst2|ALT_INV_PSUM[4][0]~q\,
	datac => \inst2|ALT_INV_PSUM[4][3]~q\,
	datad => \inst2|ALT_INV_PSUM[4][2]~q\,
	datae => \inst2|ALT_INV_PSUM[4][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[4][4]~q\,
	combout => \inst2|Add13~0_combout\);

-- Location: FF_X85_Y35_N13
\inst2|PSUM[4][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add13~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[4][5]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[4][5]~q\);

-- Location: LABCELL_X75_Y35_N3
\inst2|TSUM[2][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][5]~combout\ = LCELL(( \inst2|Add2~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add2~105_sumout\,
	combout => \inst2|TSUM[2][5]~combout\);

-- Location: LABCELL_X74_Y33_N42
\inst2|TSUM[2][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][4]~combout\ = LCELL(( \inst2|Add2~109_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~109_sumout\,
	combout => \inst2|TSUM[2][4]~combout\);

-- Location: LABCELL_X74_Y33_N48
\inst2|TSUM[2][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][3]~combout\ = LCELL(( \inst2|Add2~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~113_sumout\,
	combout => \inst2|TSUM[2][3]~combout\);

-- Location: LABCELL_X75_Y33_N36
\inst2|TSUM[2][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][2]~combout\ = LCELL(( \inst2|Add2~117_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~117_sumout\,
	combout => \inst2|TSUM[2][2]~combout\);

-- Location: LABCELL_X75_Y33_N45
\inst2|TSUM[2][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][1]~combout\ = LCELL(( \inst2|Add2~121_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~121_sumout\,
	combout => \inst2|TSUM[2][1]~combout\);

-- Location: MLABCELL_X78_Y34_N15
\inst2|TSUM[2][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[2][0]~combout\ = LCELL(( \inst2|Add2~125_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add2~125_sumout\,
	combout => \inst2|TSUM[2][0]~combout\);

-- Location: LABCELL_X77_Y33_N0
\inst2|Add3~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~125_sumout\ = SUM(( \inst2|PSUM[4][0]~q\ ) + ( \inst2|TSUM[2][0]~combout\ ) + ( !VCC ))
-- \inst2|Add3~126\ = CARRY(( \inst2|PSUM[4][0]~q\ ) + ( \inst2|TSUM[2][0]~combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[4][0]~q\,
	datac => \inst2|ALT_INV_TSUM[2][0]~combout\,
	cin => GND,
	sumout => \inst2|Add3~125_sumout\,
	cout => \inst2|Add3~126\);

-- Location: LABCELL_X77_Y33_N3
\inst2|Add3~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~121_sumout\ = SUM(( \inst2|PSUM[4][1]~q\ ) + ( \inst2|TSUM[2][1]~combout\ ) + ( \inst2|Add3~126\ ))
-- \inst2|Add3~122\ = CARRY(( \inst2|PSUM[4][1]~q\ ) + ( \inst2|TSUM[2][1]~combout\ ) + ( \inst2|Add3~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[4][1]~q\,
	datac => \inst2|ALT_INV_TSUM[2][1]~combout\,
	cin => \inst2|Add3~126\,
	sumout => \inst2|Add3~121_sumout\,
	cout => \inst2|Add3~122\);

-- Location: LABCELL_X77_Y33_N6
\inst2|Add3~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~117_sumout\ = SUM(( \inst2|PSUM[4][2]~q\ ) + ( \inst2|TSUM[2][2]~combout\ ) + ( \inst2|Add3~122\ ))
-- \inst2|Add3~118\ = CARRY(( \inst2|PSUM[4][2]~q\ ) + ( \inst2|TSUM[2][2]~combout\ ) + ( \inst2|Add3~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[2][2]~combout\,
	datad => \inst2|ALT_INV_PSUM[4][2]~q\,
	cin => \inst2|Add3~122\,
	sumout => \inst2|Add3~117_sumout\,
	cout => \inst2|Add3~118\);

-- Location: LABCELL_X77_Y33_N9
\inst2|Add3~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~113_sumout\ = SUM(( \inst2|PSUM[4][3]~q\ ) + ( \inst2|TSUM[2][3]~combout\ ) + ( \inst2|Add3~118\ ))
-- \inst2|Add3~114\ = CARRY(( \inst2|PSUM[4][3]~q\ ) + ( \inst2|TSUM[2][3]~combout\ ) + ( \inst2|Add3~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[4][3]~q\,
	datac => \inst2|ALT_INV_TSUM[2][3]~combout\,
	cin => \inst2|Add3~118\,
	sumout => \inst2|Add3~113_sumout\,
	cout => \inst2|Add3~114\);

-- Location: LABCELL_X77_Y33_N12
\inst2|Add3~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~109_sumout\ = SUM(( \inst2|PSUM[4][4]~q\ ) + ( \inst2|TSUM[2][4]~combout\ ) + ( \inst2|Add3~114\ ))
-- \inst2|Add3~110\ = CARRY(( \inst2|PSUM[4][4]~q\ ) + ( \inst2|TSUM[2][4]~combout\ ) + ( \inst2|Add3~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[4][4]~q\,
	dataf => \inst2|ALT_INV_TSUM[2][4]~combout\,
	cin => \inst2|Add3~114\,
	sumout => \inst2|Add3~109_sumout\,
	cout => \inst2|Add3~110\);

-- Location: LABCELL_X77_Y33_N15
\inst2|Add3~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~105_sumout\ = SUM(( \inst2|PSUM[4][5]~q\ ) + ( \inst2|TSUM[2][5]~combout\ ) + ( \inst2|Add3~110\ ))
-- \inst2|Add3~106\ = CARRY(( \inst2|PSUM[4][5]~q\ ) + ( \inst2|TSUM[2][5]~combout\ ) + ( \inst2|Add3~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[4][5]~q\,
	datac => \inst2|ALT_INV_TSUM[2][5]~combout\,
	cin => \inst2|Add3~110\,
	sumout => \inst2|Add3~105_sumout\,
	cout => \inst2|Add3~106\);

-- Location: LABCELL_X77_Y33_N18
\inst2|Add3~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~101_sumout\ = SUM(( \inst2|TSUM[2][6]~combout\ ) + ( GND ) + ( \inst2|Add3~106\ ))
-- \inst2|Add3~102\ = CARRY(( \inst2|TSUM[2][6]~combout\ ) + ( GND ) + ( \inst2|Add3~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][6]~combout\,
	cin => \inst2|Add3~106\,
	sumout => \inst2|Add3~101_sumout\,
	cout => \inst2|Add3~102\);

-- Location: LABCELL_X77_Y33_N21
\inst2|Add3~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~97_sumout\ = SUM(( \inst2|TSUM[2][7]~combout\ ) + ( GND ) + ( \inst2|Add3~102\ ))
-- \inst2|Add3~98\ = CARRY(( \inst2|TSUM[2][7]~combout\ ) + ( GND ) + ( \inst2|Add3~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][7]~combout\,
	cin => \inst2|Add3~102\,
	sumout => \inst2|Add3~97_sumout\,
	cout => \inst2|Add3~98\);

-- Location: LABCELL_X77_Y33_N24
\inst2|Add3~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~93_sumout\ = SUM(( \inst2|TSUM[2][8]~combout\ ) + ( GND ) + ( \inst2|Add3~98\ ))
-- \inst2|Add3~94\ = CARRY(( \inst2|TSUM[2][8]~combout\ ) + ( GND ) + ( \inst2|Add3~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[2][8]~combout\,
	cin => \inst2|Add3~98\,
	sumout => \inst2|Add3~93_sumout\,
	cout => \inst2|Add3~94\);

-- Location: LABCELL_X77_Y33_N27
\inst2|Add3~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~89_sumout\ = SUM(( \inst2|TSUM[2][9]~combout\ ) + ( GND ) + ( \inst2|Add3~94\ ))
-- \inst2|Add3~90\ = CARRY(( \inst2|TSUM[2][9]~combout\ ) + ( GND ) + ( \inst2|Add3~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][9]~combout\,
	cin => \inst2|Add3~94\,
	sumout => \inst2|Add3~89_sumout\,
	cout => \inst2|Add3~90\);

-- Location: LABCELL_X77_Y33_N30
\inst2|Add3~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~85_sumout\ = SUM(( \inst2|TSUM[2][10]~combout\ ) + ( GND ) + ( \inst2|Add3~90\ ))
-- \inst2|Add3~86\ = CARRY(( \inst2|TSUM[2][10]~combout\ ) + ( GND ) + ( \inst2|Add3~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[2][10]~combout\,
	cin => \inst2|Add3~90\,
	sumout => \inst2|Add3~85_sumout\,
	cout => \inst2|Add3~86\);

-- Location: LABCELL_X77_Y33_N33
\inst2|Add3~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~81_sumout\ = SUM(( \inst2|TSUM[2][11]~combout\ ) + ( GND ) + ( \inst2|Add3~86\ ))
-- \inst2|Add3~82\ = CARRY(( \inst2|TSUM[2][11]~combout\ ) + ( GND ) + ( \inst2|Add3~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[2][11]~combout\,
	cin => \inst2|Add3~86\,
	sumout => \inst2|Add3~81_sumout\,
	cout => \inst2|Add3~82\);

-- Location: LABCELL_X77_Y33_N36
\inst2|Add3~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~77_sumout\ = SUM(( \inst2|TSUM[2][12]~combout\ ) + ( GND ) + ( \inst2|Add3~82\ ))
-- \inst2|Add3~78\ = CARRY(( \inst2|TSUM[2][12]~combout\ ) + ( GND ) + ( \inst2|Add3~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][12]~combout\,
	cin => \inst2|Add3~82\,
	sumout => \inst2|Add3~77_sumout\,
	cout => \inst2|Add3~78\);

-- Location: LABCELL_X77_Y33_N39
\inst2|Add3~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~73_sumout\ = SUM(( \inst2|TSUM[2][13]~combout\ ) + ( GND ) + ( \inst2|Add3~78\ ))
-- \inst2|Add3~74\ = CARRY(( \inst2|TSUM[2][13]~combout\ ) + ( GND ) + ( \inst2|Add3~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][13]~combout\,
	cin => \inst2|Add3~78\,
	sumout => \inst2|Add3~73_sumout\,
	cout => \inst2|Add3~74\);

-- Location: LABCELL_X77_Y33_N42
\inst2|Add3~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~69_sumout\ = SUM(( \inst2|TSUM[2][14]~combout\ ) + ( GND ) + ( \inst2|Add3~74\ ))
-- \inst2|Add3~70\ = CARRY(( \inst2|TSUM[2][14]~combout\ ) + ( GND ) + ( \inst2|Add3~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][14]~combout\,
	cin => \inst2|Add3~74\,
	sumout => \inst2|Add3~69_sumout\,
	cout => \inst2|Add3~70\);

-- Location: LABCELL_X77_Y33_N45
\inst2|Add3~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~65_sumout\ = SUM(( \inst2|TSUM[2][15]~combout\ ) + ( GND ) + ( \inst2|Add3~70\ ))
-- \inst2|Add3~66\ = CARRY(( \inst2|TSUM[2][15]~combout\ ) + ( GND ) + ( \inst2|Add3~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][15]~combout\,
	cin => \inst2|Add3~70\,
	sumout => \inst2|Add3~65_sumout\,
	cout => \inst2|Add3~66\);

-- Location: LABCELL_X77_Y33_N48
\inst2|Add3~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~61_sumout\ = SUM(( \inst2|TSUM[2][16]~combout\ ) + ( GND ) + ( \inst2|Add3~66\ ))
-- \inst2|Add3~62\ = CARRY(( \inst2|TSUM[2][16]~combout\ ) + ( GND ) + ( \inst2|Add3~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][16]~combout\,
	cin => \inst2|Add3~66\,
	sumout => \inst2|Add3~61_sumout\,
	cout => \inst2|Add3~62\);

-- Location: LABCELL_X77_Y33_N51
\inst2|Add3~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~57_sumout\ = SUM(( \inst2|TSUM[2][17]~combout\ ) + ( GND ) + ( \inst2|Add3~62\ ))
-- \inst2|Add3~58\ = CARRY(( \inst2|TSUM[2][17]~combout\ ) + ( GND ) + ( \inst2|Add3~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][17]~combout\,
	cin => \inst2|Add3~62\,
	sumout => \inst2|Add3~57_sumout\,
	cout => \inst2|Add3~58\);

-- Location: LABCELL_X77_Y33_N54
\inst2|Add3~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~53_sumout\ = SUM(( \inst2|TSUM[2][18]~combout\ ) + ( GND ) + ( \inst2|Add3~58\ ))
-- \inst2|Add3~54\ = CARRY(( \inst2|TSUM[2][18]~combout\ ) + ( GND ) + ( \inst2|Add3~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][18]~combout\,
	cin => \inst2|Add3~58\,
	sumout => \inst2|Add3~53_sumout\,
	cout => \inst2|Add3~54\);

-- Location: LABCELL_X77_Y33_N57
\inst2|Add3~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~49_sumout\ = SUM(( \inst2|TSUM[2][19]~combout\ ) + ( GND ) + ( \inst2|Add3~54\ ))
-- \inst2|Add3~50\ = CARRY(( \inst2|TSUM[2][19]~combout\ ) + ( GND ) + ( \inst2|Add3~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][19]~combout\,
	cin => \inst2|Add3~54\,
	sumout => \inst2|Add3~49_sumout\,
	cout => \inst2|Add3~50\);

-- Location: LABCELL_X77_Y32_N0
\inst2|Add3~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~45_sumout\ = SUM(( \inst2|TSUM[2][20]~combout\ ) + ( GND ) + ( \inst2|Add3~50\ ))
-- \inst2|Add3~46\ = CARRY(( \inst2|TSUM[2][20]~combout\ ) + ( GND ) + ( \inst2|Add3~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][20]~combout\,
	cin => \inst2|Add3~50\,
	sumout => \inst2|Add3~45_sumout\,
	cout => \inst2|Add3~46\);

-- Location: LABCELL_X77_Y32_N3
\inst2|Add3~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~41_sumout\ = SUM(( \inst2|TSUM[2][21]~combout\ ) + ( GND ) + ( \inst2|Add3~46\ ))
-- \inst2|Add3~42\ = CARRY(( \inst2|TSUM[2][21]~combout\ ) + ( GND ) + ( \inst2|Add3~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][21]~combout\,
	cin => \inst2|Add3~46\,
	sumout => \inst2|Add3~41_sumout\,
	cout => \inst2|Add3~42\);

-- Location: LABCELL_X77_Y32_N6
\inst2|Add3~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~37_sumout\ = SUM(( \inst2|TSUM[2][22]~combout\ ) + ( GND ) + ( \inst2|Add3~42\ ))
-- \inst2|Add3~38\ = CARRY(( \inst2|TSUM[2][22]~combout\ ) + ( GND ) + ( \inst2|Add3~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][22]~combout\,
	cin => \inst2|Add3~42\,
	sumout => \inst2|Add3~37_sumout\,
	cout => \inst2|Add3~38\);

-- Location: LABCELL_X77_Y32_N9
\inst2|Add3~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~33_sumout\ = SUM(( \inst2|TSUM[2][23]~combout\ ) + ( GND ) + ( \inst2|Add3~38\ ))
-- \inst2|Add3~34\ = CARRY(( \inst2|TSUM[2][23]~combout\ ) + ( GND ) + ( \inst2|Add3~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[2][23]~combout\,
	cin => \inst2|Add3~38\,
	sumout => \inst2|Add3~33_sumout\,
	cout => \inst2|Add3~34\);

-- Location: LABCELL_X77_Y32_N12
\inst2|Add3~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~29_sumout\ = SUM(( \inst2|TSUM[2][24]~combout\ ) + ( GND ) + ( \inst2|Add3~34\ ))
-- \inst2|Add3~30\ = CARRY(( \inst2|TSUM[2][24]~combout\ ) + ( GND ) + ( \inst2|Add3~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][24]~combout\,
	cin => \inst2|Add3~34\,
	sumout => \inst2|Add3~29_sumout\,
	cout => \inst2|Add3~30\);

-- Location: LABCELL_X77_Y32_N15
\inst2|Add3~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~25_sumout\ = SUM(( \inst2|TSUM[2][25]~combout\ ) + ( GND ) + ( \inst2|Add3~30\ ))
-- \inst2|Add3~26\ = CARRY(( \inst2|TSUM[2][25]~combout\ ) + ( GND ) + ( \inst2|Add3~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][25]~combout\,
	cin => \inst2|Add3~30\,
	sumout => \inst2|Add3~25_sumout\,
	cout => \inst2|Add3~26\);

-- Location: LABCELL_X77_Y32_N18
\inst2|Add3~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~21_sumout\ = SUM(( \inst2|TSUM[2][26]~combout\ ) + ( GND ) + ( \inst2|Add3~26\ ))
-- \inst2|Add3~22\ = CARRY(( \inst2|TSUM[2][26]~combout\ ) + ( GND ) + ( \inst2|Add3~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][26]~combout\,
	cin => \inst2|Add3~26\,
	sumout => \inst2|Add3~21_sumout\,
	cout => \inst2|Add3~22\);

-- Location: LABCELL_X77_Y32_N21
\inst2|Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~17_sumout\ = SUM(( \inst2|TSUM[2][27]~combout\ ) + ( GND ) + ( \inst2|Add3~22\ ))
-- \inst2|Add3~18\ = CARRY(( \inst2|TSUM[2][27]~combout\ ) + ( GND ) + ( \inst2|Add3~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][27]~combout\,
	cin => \inst2|Add3~22\,
	sumout => \inst2|Add3~17_sumout\,
	cout => \inst2|Add3~18\);

-- Location: LABCELL_X77_Y32_N24
\inst2|Add3~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~13_sumout\ = SUM(( \inst2|TSUM[2][28]~combout\ ) + ( GND ) + ( \inst2|Add3~18\ ))
-- \inst2|Add3~14\ = CARRY(( \inst2|TSUM[2][28]~combout\ ) + ( GND ) + ( \inst2|Add3~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[2][28]~combout\,
	cin => \inst2|Add3~18\,
	sumout => \inst2|Add3~13_sumout\,
	cout => \inst2|Add3~14\);

-- Location: LABCELL_X77_Y32_N27
\inst2|Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~9_sumout\ = SUM(( \inst2|TSUM[2][29]~combout\ ) + ( GND ) + ( \inst2|Add3~14\ ))
-- \inst2|Add3~10\ = CARRY(( \inst2|TSUM[2][29]~combout\ ) + ( GND ) + ( \inst2|Add3~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][29]~combout\,
	cin => \inst2|Add3~14\,
	sumout => \inst2|Add3~9_sumout\,
	cout => \inst2|Add3~10\);

-- Location: LABCELL_X77_Y32_N30
\inst2|Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~5_sumout\ = SUM(( \inst2|TSUM[2][30]~combout\ ) + ( GND ) + ( \inst2|Add3~10\ ))
-- \inst2|Add3~6\ = CARRY(( \inst2|TSUM[2][30]~combout\ ) + ( GND ) + ( \inst2|Add3~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[2][30]~combout\,
	cin => \inst2|Add3~10\,
	sumout => \inst2|Add3~5_sumout\,
	cout => \inst2|Add3~6\);

-- Location: LABCELL_X77_Y32_N33
\inst2|Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add3~1_sumout\ = SUM(( \inst2|TSUM[2][31]~combout\ ) + ( GND ) + ( \inst2|Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[2][31]~combout\,
	cin => \inst2|Add3~6\,
	sumout => \inst2|Add3~1_sumout\);

-- Location: MLABCELL_X78_Y32_N3
\inst2|TSUM[3][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][31]~combout\ = LCELL(( \inst2|Add3~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add3~1_sumout\,
	combout => \inst2|TSUM[3][31]~combout\);

-- Location: MLABCELL_X78_Y32_N21
\inst2|TSUM[3][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][30]~combout\ = LCELL(( \inst2|Add3~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~5_sumout\,
	combout => \inst2|TSUM[3][30]~combout\);

-- Location: MLABCELL_X78_Y32_N27
\inst2|TSUM[3][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][29]~combout\ = LCELL(( \inst2|Add3~9_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add3~9_sumout\,
	combout => \inst2|TSUM[3][29]~combout\);

-- Location: MLABCELL_X78_Y32_N33
\inst2|TSUM[3][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][28]~combout\ = LCELL(( \inst2|Add3~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~13_sumout\,
	combout => \inst2|TSUM[3][28]~combout\);

-- Location: MLABCELL_X78_Y32_N48
\inst2|TSUM[3][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][27]~combout\ = LCELL(( \inst2|Add3~17_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add3~17_sumout\,
	combout => \inst2|TSUM[3][27]~combout\);

-- Location: MLABCELL_X78_Y32_N6
\inst2|TSUM[3][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][26]~combout\ = LCELL(( \inst2|Add3~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~21_sumout\,
	combout => \inst2|TSUM[3][26]~combout\);

-- Location: MLABCELL_X78_Y32_N18
\inst2|TSUM[3][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][25]~combout\ = LCELL(( \inst2|Add3~25_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~25_sumout\,
	combout => \inst2|TSUM[3][25]~combout\);

-- Location: MLABCELL_X78_Y32_N12
\inst2|TSUM[3][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][24]~combout\ = LCELL(( \inst2|Add3~29_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add3~29_sumout\,
	combout => \inst2|TSUM[3][24]~combout\);

-- Location: MLABCELL_X78_Y32_N30
\inst2|TSUM[3][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][23]~combout\ = LCELL(( \inst2|Add3~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~33_sumout\,
	combout => \inst2|TSUM[3][23]~combout\);

-- Location: MLABCELL_X78_Y32_N42
\inst2|TSUM[3][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][22]~combout\ = LCELL(( \inst2|Add3~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add3~37_sumout\,
	combout => \inst2|TSUM[3][22]~combout\);

-- Location: MLABCELL_X78_Y32_N36
\inst2|TSUM[3][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][21]~combout\ = LCELL(( \inst2|Add3~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~41_sumout\,
	combout => \inst2|TSUM[3][21]~combout\);

-- Location: LABCELL_X79_Y32_N51
\inst2|TSUM[3][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][20]~combout\ = LCELL(( \inst2|Add3~45_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~45_sumout\,
	combout => \inst2|TSUM[3][20]~combout\);

-- Location: MLABCELL_X78_Y33_N24
\inst2|TSUM[3][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][19]~combout\ = LCELL(\inst2|Add3~49_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add3~49_sumout\,
	combout => \inst2|TSUM[3][19]~combout\);

-- Location: MLABCELL_X78_Y33_N27
\inst2|TSUM[3][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][18]~combout\ = LCELL(( \inst2|Add3~53_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~53_sumout\,
	combout => \inst2|TSUM[3][18]~combout\);

-- Location: MLABCELL_X78_Y33_N42
\inst2|TSUM[3][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][17]~combout\ = LCELL(( \inst2|Add3~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~57_sumout\,
	combout => \inst2|TSUM[3][17]~combout\);

-- Location: MLABCELL_X78_Y33_N45
\inst2|TSUM[3][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][16]~combout\ = LCELL(\inst2|Add3~61_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add3~61_sumout\,
	combout => \inst2|TSUM[3][16]~combout\);

-- Location: MLABCELL_X78_Y33_N36
\inst2|TSUM[3][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][15]~combout\ = LCELL(( \inst2|Add3~65_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~65_sumout\,
	combout => \inst2|TSUM[3][15]~combout\);

-- Location: MLABCELL_X78_Y33_N39
\inst2|TSUM[3][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][14]~combout\ = LCELL(\inst2|Add3~69_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add3~69_sumout\,
	combout => \inst2|TSUM[3][14]~combout\);

-- Location: LABCELL_X75_Y33_N27
\inst2|TSUM[3][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][13]~combout\ = LCELL(( \inst2|Add3~73_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add3~73_sumout\,
	combout => \inst2|TSUM[3][13]~combout\);

-- Location: MLABCELL_X78_Y33_N33
\inst2|TSUM[3][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][12]~combout\ = LCELL(( \inst2|Add3~77_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~77_sumout\,
	combout => \inst2|TSUM[3][12]~combout\);

-- Location: MLABCELL_X78_Y33_N12
\inst2|TSUM[3][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][11]~combout\ = LCELL(\inst2|Add3~81_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add3~81_sumout\,
	combout => \inst2|TSUM[3][11]~combout\);

-- Location: MLABCELL_X78_Y33_N15
\inst2|TSUM[3][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][10]~combout\ = LCELL(( \inst2|Add3~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~85_sumout\,
	combout => \inst2|TSUM[3][10]~combout\);

-- Location: MLABCELL_X78_Y33_N18
\inst2|TSUM[3][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][9]~combout\ = LCELL(\inst2|Add3~89_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add3~89_sumout\,
	combout => \inst2|TSUM[3][9]~combout\);

-- Location: MLABCELL_X78_Y33_N21
\inst2|TSUM[3][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][8]~combout\ = LCELL(( \inst2|Add3~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~93_sumout\,
	combout => \inst2|TSUM[3][8]~combout\);

-- Location: MLABCELL_X78_Y33_N48
\inst2|TSUM[3][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][7]~combout\ = LCELL(\inst2|Add3~97_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add3~97_sumout\,
	combout => \inst2|TSUM[3][7]~combout\);

-- Location: MLABCELL_X78_Y33_N51
\inst2|TSUM[3][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][6]~combout\ = LCELL(( \inst2|Add3~101_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~101_sumout\,
	combout => \inst2|TSUM[3][6]~combout\);

-- Location: MLABCELL_X78_Y33_N54
\inst2|TSUM[3][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][5]~combout\ = LCELL(( \inst2|Add3~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~105_sumout\,
	combout => \inst2|TSUM[3][5]~combout\);

-- Location: LABCELL_X81_Y35_N48
\inst2|PSUM[5][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[5][0]~_wirecell_combout\ = !\inst2|PSUM[5][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[5][0]~q\,
	combout => \inst2|PSUM[5][0]~_wirecell_combout\);

-- Location: LABCELL_X81_Y35_N51
\inst2|PSUM[5][1]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[5][1]~3_combout\ = ( \inst7|DIVD~q\ ) # ( !\inst7|DIVD~q\ & ( !\inst6|STEMP\(10) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst6|ALT_INV_STEMP\(10),
	dataf => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[5][1]~3_combout\);

-- Location: FF_X81_Y35_N50
\inst2|PSUM[5][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|PSUM[5][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[5][1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[5][0]~q\);

-- Location: LABCELL_X81_Y35_N33
\inst2|Add12~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add12~4_combout\ = ( \inst2|PSUM[5][0]~q\ & ( !\inst2|PSUM[5][1]~q\ ) ) # ( !\inst2|PSUM[5][0]~q\ & ( \inst2|PSUM[5][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[5][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[5][0]~q\,
	combout => \inst2|Add12~4_combout\);

-- Location: FF_X81_Y35_N35
\inst2|PSUM[5][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add12~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[5][1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[5][1]~q\);

-- Location: LABCELL_X81_Y35_N30
\inst2|Add12~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add12~3_combout\ = ( \inst2|PSUM[5][1]~q\ & ( !\inst2|PSUM[5][0]~q\ $ (!\inst2|PSUM[5][2]~q\) ) ) # ( !\inst2|PSUM[5][1]~q\ & ( \inst2|PSUM[5][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[5][0]~q\,
	datad => \inst2|ALT_INV_PSUM[5][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[5][1]~q\,
	combout => \inst2|Add12~3_combout\);

-- Location: FF_X81_Y35_N32
\inst2|PSUM[5][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add12~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[5][1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[5][2]~q\);

-- Location: LABCELL_X81_Y35_N45
\inst2|Add12~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add12~2_combout\ = ( \inst2|PSUM[5][1]~q\ & ( !\inst2|PSUM[5][3]~q\ $ (((!\inst2|PSUM[5][0]~q\) # (!\inst2|PSUM[5][2]~q\))) ) ) # ( !\inst2|PSUM[5][1]~q\ & ( \inst2|PSUM[5][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[5][0]~q\,
	datac => \inst2|ALT_INV_PSUM[5][2]~q\,
	datad => \inst2|ALT_INV_PSUM[5][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[5][1]~q\,
	combout => \inst2|Add12~2_combout\);

-- Location: FF_X81_Y35_N47
\inst2|PSUM[5][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add12~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[5][1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[5][3]~q\);

-- Location: LABCELL_X81_Y35_N42
\inst2|Add12~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add12~1_combout\ = ( \inst2|PSUM[5][3]~q\ & ( !\inst2|PSUM[5][4]~q\ $ (((!\inst2|PSUM[5][0]~q\) # ((!\inst2|PSUM[5][2]~q\) # (!\inst2|PSUM[5][1]~q\)))) ) ) # ( !\inst2|PSUM[5][3]~q\ & ( \inst2|PSUM[5][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[5][0]~q\,
	datab => \inst2|ALT_INV_PSUM[5][2]~q\,
	datac => \inst2|ALT_INV_PSUM[5][1]~q\,
	datad => \inst2|ALT_INV_PSUM[5][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[5][3]~q\,
	combout => \inst2|Add12~1_combout\);

-- Location: FF_X81_Y35_N44
\inst2|PSUM[5][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add12~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[5][1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[5][4]~q\);

-- Location: LABCELL_X81_Y35_N36
\inst2|Add12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add12~0_combout\ = ( \inst2|PSUM[5][5]~q\ & ( \inst2|PSUM[5][3]~q\ & ( (!\inst2|PSUM[5][1]~q\) # ((!\inst2|PSUM[5][4]~q\) # ((!\inst2|PSUM[5][0]~q\) # (!\inst2|PSUM[5][2]~q\))) ) ) ) # ( !\inst2|PSUM[5][5]~q\ & ( \inst2|PSUM[5][3]~q\ & ( 
-- (\inst2|PSUM[5][1]~q\ & (\inst2|PSUM[5][4]~q\ & (\inst2|PSUM[5][0]~q\ & \inst2|PSUM[5][2]~q\))) ) ) ) # ( \inst2|PSUM[5][5]~q\ & ( !\inst2|PSUM[5][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[5][1]~q\,
	datab => \inst2|ALT_INV_PSUM[5][4]~q\,
	datac => \inst2|ALT_INV_PSUM[5][0]~q\,
	datad => \inst2|ALT_INV_PSUM[5][2]~q\,
	datae => \inst2|ALT_INV_PSUM[5][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[5][3]~q\,
	combout => \inst2|Add12~0_combout\);

-- Location: FF_X81_Y35_N37
\inst2|PSUM[5][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add12~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[5][1]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[5][5]~q\);

-- Location: MLABCELL_X78_Y33_N57
\inst2|TSUM[3][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][4]~combout\ = LCELL(\inst2|Add3~109_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add3~109_sumout\,
	combout => \inst2|TSUM[3][4]~combout\);

-- Location: MLABCELL_X78_Y33_N0
\inst2|TSUM[3][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][3]~combout\ = LCELL(( \inst2|Add3~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~113_sumout\,
	combout => \inst2|TSUM[3][3]~combout\);

-- Location: MLABCELL_X78_Y33_N3
\inst2|TSUM[3][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][2]~combout\ = LCELL(\inst2|Add3~117_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add3~117_sumout\,
	combout => \inst2|TSUM[3][2]~combout\);

-- Location: MLABCELL_X78_Y33_N6
\inst2|TSUM[3][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][1]~combout\ = LCELL(( \inst2|Add3~121_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~121_sumout\,
	combout => \inst2|TSUM[3][1]~combout\);

-- Location: MLABCELL_X78_Y33_N9
\inst2|TSUM[3][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[3][0]~combout\ = LCELL(( \inst2|Add3~125_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add3~125_sumout\,
	combout => \inst2|TSUM[3][0]~combout\);

-- Location: LABCELL_X79_Y33_N0
\inst2|Add4~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~125_sumout\ = SUM(( \inst2|PSUM[5][0]~q\ ) + ( \inst2|TSUM[3][0]~combout\ ) + ( !VCC ))
-- \inst2|Add4~126\ = CARRY(( \inst2|PSUM[5][0]~q\ ) + ( \inst2|TSUM[3][0]~combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[3][0]~combout\,
	datac => \inst2|ALT_INV_PSUM[5][0]~q\,
	cin => GND,
	sumout => \inst2|Add4~125_sumout\,
	cout => \inst2|Add4~126\);

-- Location: LABCELL_X79_Y33_N3
\inst2|Add4~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~121_sumout\ = SUM(( \inst2|TSUM[3][1]~combout\ ) + ( \inst2|PSUM[5][1]~q\ ) + ( \inst2|Add4~126\ ))
-- \inst2|Add4~122\ = CARRY(( \inst2|TSUM[3][1]~combout\ ) + ( \inst2|PSUM[5][1]~q\ ) + ( \inst2|Add4~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][1]~combout\,
	dataf => \inst2|ALT_INV_PSUM[5][1]~q\,
	cin => \inst2|Add4~126\,
	sumout => \inst2|Add4~121_sumout\,
	cout => \inst2|Add4~122\);

-- Location: LABCELL_X79_Y33_N6
\inst2|Add4~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~117_sumout\ = SUM(( \inst2|PSUM[5][2]~q\ ) + ( \inst2|TSUM[3][2]~combout\ ) + ( \inst2|Add4~122\ ))
-- \inst2|Add4~118\ = CARRY(( \inst2|PSUM[5][2]~q\ ) + ( \inst2|TSUM[3][2]~combout\ ) + ( \inst2|Add4~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[5][2]~q\,
	dataf => \inst2|ALT_INV_TSUM[3][2]~combout\,
	cin => \inst2|Add4~122\,
	sumout => \inst2|Add4~117_sumout\,
	cout => \inst2|Add4~118\);

-- Location: LABCELL_X79_Y33_N9
\inst2|Add4~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~113_sumout\ = SUM(( \inst2|PSUM[5][3]~q\ ) + ( \inst2|TSUM[3][3]~combout\ ) + ( \inst2|Add4~118\ ))
-- \inst2|Add4~114\ = CARRY(( \inst2|PSUM[5][3]~q\ ) + ( \inst2|TSUM[3][3]~combout\ ) + ( \inst2|Add4~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[5][3]~q\,
	datac => \inst2|ALT_INV_TSUM[3][3]~combout\,
	cin => \inst2|Add4~118\,
	sumout => \inst2|Add4~113_sumout\,
	cout => \inst2|Add4~114\);

-- Location: LABCELL_X79_Y33_N12
\inst2|Add4~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~109_sumout\ = SUM(( \inst2|PSUM[5][4]~q\ ) + ( \inst2|TSUM[3][4]~combout\ ) + ( \inst2|Add4~114\ ))
-- \inst2|Add4~110\ = CARRY(( \inst2|PSUM[5][4]~q\ ) + ( \inst2|TSUM[3][4]~combout\ ) + ( \inst2|Add4~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[5][4]~q\,
	datac => \inst2|ALT_INV_TSUM[3][4]~combout\,
	cin => \inst2|Add4~114\,
	sumout => \inst2|Add4~109_sumout\,
	cout => \inst2|Add4~110\);

-- Location: LABCELL_X79_Y33_N15
\inst2|Add4~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~105_sumout\ = SUM(( \inst2|TSUM[3][5]~combout\ ) + ( \inst2|PSUM[5][5]~q\ ) + ( \inst2|Add4~110\ ))
-- \inst2|Add4~106\ = CARRY(( \inst2|TSUM[3][5]~combout\ ) + ( \inst2|PSUM[5][5]~q\ ) + ( \inst2|Add4~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][5]~combout\,
	dataf => \inst2|ALT_INV_PSUM[5][5]~q\,
	cin => \inst2|Add4~110\,
	sumout => \inst2|Add4~105_sumout\,
	cout => \inst2|Add4~106\);

-- Location: LABCELL_X79_Y33_N18
\inst2|Add4~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~101_sumout\ = SUM(( \inst2|TSUM[3][6]~combout\ ) + ( GND ) + ( \inst2|Add4~106\ ))
-- \inst2|Add4~102\ = CARRY(( \inst2|TSUM[3][6]~combout\ ) + ( GND ) + ( \inst2|Add4~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][6]~combout\,
	cin => \inst2|Add4~106\,
	sumout => \inst2|Add4~101_sumout\,
	cout => \inst2|Add4~102\);

-- Location: LABCELL_X79_Y33_N21
\inst2|Add4~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~97_sumout\ = SUM(( \inst2|TSUM[3][7]~combout\ ) + ( GND ) + ( \inst2|Add4~102\ ))
-- \inst2|Add4~98\ = CARRY(( \inst2|TSUM[3][7]~combout\ ) + ( GND ) + ( \inst2|Add4~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][7]~combout\,
	cin => \inst2|Add4~102\,
	sumout => \inst2|Add4~97_sumout\,
	cout => \inst2|Add4~98\);

-- Location: LABCELL_X79_Y33_N24
\inst2|Add4~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~93_sumout\ = SUM(( \inst2|TSUM[3][8]~combout\ ) + ( GND ) + ( \inst2|Add4~98\ ))
-- \inst2|Add4~94\ = CARRY(( \inst2|TSUM[3][8]~combout\ ) + ( GND ) + ( \inst2|Add4~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][8]~combout\,
	cin => \inst2|Add4~98\,
	sumout => \inst2|Add4~93_sumout\,
	cout => \inst2|Add4~94\);

-- Location: LABCELL_X79_Y33_N27
\inst2|Add4~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~89_sumout\ = SUM(( \inst2|TSUM[3][9]~combout\ ) + ( GND ) + ( \inst2|Add4~94\ ))
-- \inst2|Add4~90\ = CARRY(( \inst2|TSUM[3][9]~combout\ ) + ( GND ) + ( \inst2|Add4~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][9]~combout\,
	cin => \inst2|Add4~94\,
	sumout => \inst2|Add4~89_sumout\,
	cout => \inst2|Add4~90\);

-- Location: LABCELL_X79_Y33_N30
\inst2|Add4~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~85_sumout\ = SUM(( \inst2|TSUM[3][10]~combout\ ) + ( GND ) + ( \inst2|Add4~90\ ))
-- \inst2|Add4~86\ = CARRY(( \inst2|TSUM[3][10]~combout\ ) + ( GND ) + ( \inst2|Add4~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[3][10]~combout\,
	cin => \inst2|Add4~90\,
	sumout => \inst2|Add4~85_sumout\,
	cout => \inst2|Add4~86\);

-- Location: LABCELL_X79_Y33_N33
\inst2|Add4~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~81_sumout\ = SUM(( \inst2|TSUM[3][11]~combout\ ) + ( GND ) + ( \inst2|Add4~86\ ))
-- \inst2|Add4~82\ = CARRY(( \inst2|TSUM[3][11]~combout\ ) + ( GND ) + ( \inst2|Add4~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][11]~combout\,
	cin => \inst2|Add4~86\,
	sumout => \inst2|Add4~81_sumout\,
	cout => \inst2|Add4~82\);

-- Location: LABCELL_X79_Y33_N36
\inst2|Add4~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~77_sumout\ = SUM(( \inst2|TSUM[3][12]~combout\ ) + ( GND ) + ( \inst2|Add4~82\ ))
-- \inst2|Add4~78\ = CARRY(( \inst2|TSUM[3][12]~combout\ ) + ( GND ) + ( \inst2|Add4~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[3][12]~combout\,
	cin => \inst2|Add4~82\,
	sumout => \inst2|Add4~77_sumout\,
	cout => \inst2|Add4~78\);

-- Location: LABCELL_X79_Y33_N39
\inst2|Add4~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~73_sumout\ = SUM(( \inst2|TSUM[3][13]~combout\ ) + ( GND ) + ( \inst2|Add4~78\ ))
-- \inst2|Add4~74\ = CARRY(( \inst2|TSUM[3][13]~combout\ ) + ( GND ) + ( \inst2|Add4~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][13]~combout\,
	cin => \inst2|Add4~78\,
	sumout => \inst2|Add4~73_sumout\,
	cout => \inst2|Add4~74\);

-- Location: LABCELL_X79_Y33_N42
\inst2|Add4~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~69_sumout\ = SUM(( \inst2|TSUM[3][14]~combout\ ) + ( GND ) + ( \inst2|Add4~74\ ))
-- \inst2|Add4~70\ = CARRY(( \inst2|TSUM[3][14]~combout\ ) + ( GND ) + ( \inst2|Add4~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][14]~combout\,
	cin => \inst2|Add4~74\,
	sumout => \inst2|Add4~69_sumout\,
	cout => \inst2|Add4~70\);

-- Location: LABCELL_X79_Y33_N45
\inst2|Add4~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~65_sumout\ = SUM(( \inst2|TSUM[3][15]~combout\ ) + ( GND ) + ( \inst2|Add4~70\ ))
-- \inst2|Add4~66\ = CARRY(( \inst2|TSUM[3][15]~combout\ ) + ( GND ) + ( \inst2|Add4~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][15]~combout\,
	cin => \inst2|Add4~70\,
	sumout => \inst2|Add4~65_sumout\,
	cout => \inst2|Add4~66\);

-- Location: LABCELL_X79_Y33_N48
\inst2|Add4~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~61_sumout\ = SUM(( \inst2|TSUM[3][16]~combout\ ) + ( GND ) + ( \inst2|Add4~66\ ))
-- \inst2|Add4~62\ = CARRY(( \inst2|TSUM[3][16]~combout\ ) + ( GND ) + ( \inst2|Add4~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][16]~combout\,
	cin => \inst2|Add4~66\,
	sumout => \inst2|Add4~61_sumout\,
	cout => \inst2|Add4~62\);

-- Location: LABCELL_X79_Y33_N51
\inst2|Add4~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~57_sumout\ = SUM(( \inst2|TSUM[3][17]~combout\ ) + ( GND ) + ( \inst2|Add4~62\ ))
-- \inst2|Add4~58\ = CARRY(( \inst2|TSUM[3][17]~combout\ ) + ( GND ) + ( \inst2|Add4~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][17]~combout\,
	cin => \inst2|Add4~62\,
	sumout => \inst2|Add4~57_sumout\,
	cout => \inst2|Add4~58\);

-- Location: LABCELL_X79_Y33_N54
\inst2|Add4~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~53_sumout\ = SUM(( \inst2|TSUM[3][18]~combout\ ) + ( GND ) + ( \inst2|Add4~58\ ))
-- \inst2|Add4~54\ = CARRY(( \inst2|TSUM[3][18]~combout\ ) + ( GND ) + ( \inst2|Add4~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[3][18]~combout\,
	cin => \inst2|Add4~58\,
	sumout => \inst2|Add4~53_sumout\,
	cout => \inst2|Add4~54\);

-- Location: LABCELL_X79_Y33_N57
\inst2|Add4~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~49_sumout\ = SUM(( \inst2|TSUM[3][19]~combout\ ) + ( GND ) + ( \inst2|Add4~54\ ))
-- \inst2|Add4~50\ = CARRY(( \inst2|TSUM[3][19]~combout\ ) + ( GND ) + ( \inst2|Add4~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][19]~combout\,
	cin => \inst2|Add4~54\,
	sumout => \inst2|Add4~49_sumout\,
	cout => \inst2|Add4~50\);

-- Location: LABCELL_X79_Y32_N0
\inst2|Add4~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~45_sumout\ = SUM(( \inst2|TSUM[3][20]~combout\ ) + ( GND ) + ( \inst2|Add4~50\ ))
-- \inst2|Add4~46\ = CARRY(( \inst2|TSUM[3][20]~combout\ ) + ( GND ) + ( \inst2|Add4~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][20]~combout\,
	cin => \inst2|Add4~50\,
	sumout => \inst2|Add4~45_sumout\,
	cout => \inst2|Add4~46\);

-- Location: LABCELL_X79_Y32_N3
\inst2|Add4~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~41_sumout\ = SUM(( \inst2|TSUM[3][21]~combout\ ) + ( GND ) + ( \inst2|Add4~46\ ))
-- \inst2|Add4~42\ = CARRY(( \inst2|TSUM[3][21]~combout\ ) + ( GND ) + ( \inst2|Add4~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][21]~combout\,
	cin => \inst2|Add4~46\,
	sumout => \inst2|Add4~41_sumout\,
	cout => \inst2|Add4~42\);

-- Location: LABCELL_X79_Y32_N6
\inst2|Add4~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~37_sumout\ = SUM(( \inst2|TSUM[3][22]~combout\ ) + ( GND ) + ( \inst2|Add4~42\ ))
-- \inst2|Add4~38\ = CARRY(( \inst2|TSUM[3][22]~combout\ ) + ( GND ) + ( \inst2|Add4~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[3][22]~combout\,
	cin => \inst2|Add4~42\,
	sumout => \inst2|Add4~37_sumout\,
	cout => \inst2|Add4~38\);

-- Location: LABCELL_X79_Y32_N9
\inst2|Add4~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~33_sumout\ = SUM(( \inst2|TSUM[3][23]~combout\ ) + ( GND ) + ( \inst2|Add4~38\ ))
-- \inst2|Add4~34\ = CARRY(( \inst2|TSUM[3][23]~combout\ ) + ( GND ) + ( \inst2|Add4~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][23]~combout\,
	cin => \inst2|Add4~38\,
	sumout => \inst2|Add4~33_sumout\,
	cout => \inst2|Add4~34\);

-- Location: LABCELL_X79_Y32_N12
\inst2|Add4~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~29_sumout\ = SUM(( \inst2|TSUM[3][24]~combout\ ) + ( GND ) + ( \inst2|Add4~34\ ))
-- \inst2|Add4~30\ = CARRY(( \inst2|TSUM[3][24]~combout\ ) + ( GND ) + ( \inst2|Add4~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][24]~combout\,
	cin => \inst2|Add4~34\,
	sumout => \inst2|Add4~29_sumout\,
	cout => \inst2|Add4~30\);

-- Location: LABCELL_X79_Y32_N15
\inst2|Add4~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~25_sumout\ = SUM(( \inst2|TSUM[3][25]~combout\ ) + ( GND ) + ( \inst2|Add4~30\ ))
-- \inst2|Add4~26\ = CARRY(( \inst2|TSUM[3][25]~combout\ ) + ( GND ) + ( \inst2|Add4~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][25]~combout\,
	cin => \inst2|Add4~30\,
	sumout => \inst2|Add4~25_sumout\,
	cout => \inst2|Add4~26\);

-- Location: LABCELL_X79_Y32_N18
\inst2|Add4~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~21_sumout\ = SUM(( \inst2|TSUM[3][26]~combout\ ) + ( GND ) + ( \inst2|Add4~26\ ))
-- \inst2|Add4~22\ = CARRY(( \inst2|TSUM[3][26]~combout\ ) + ( GND ) + ( \inst2|Add4~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][26]~combout\,
	cin => \inst2|Add4~26\,
	sumout => \inst2|Add4~21_sumout\,
	cout => \inst2|Add4~22\);

-- Location: LABCELL_X79_Y32_N21
\inst2|Add4~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~17_sumout\ = SUM(( \inst2|TSUM[3][27]~combout\ ) + ( GND ) + ( \inst2|Add4~22\ ))
-- \inst2|Add4~18\ = CARRY(( \inst2|TSUM[3][27]~combout\ ) + ( GND ) + ( \inst2|Add4~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][27]~combout\,
	cin => \inst2|Add4~22\,
	sumout => \inst2|Add4~17_sumout\,
	cout => \inst2|Add4~18\);

-- Location: LABCELL_X79_Y32_N24
\inst2|Add4~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~13_sumout\ = SUM(( \inst2|TSUM[3][28]~combout\ ) + ( GND ) + ( \inst2|Add4~18\ ))
-- \inst2|Add4~14\ = CARRY(( \inst2|TSUM[3][28]~combout\ ) + ( GND ) + ( \inst2|Add4~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[3][28]~combout\,
	cin => \inst2|Add4~18\,
	sumout => \inst2|Add4~13_sumout\,
	cout => \inst2|Add4~14\);

-- Location: LABCELL_X79_Y32_N27
\inst2|Add4~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~9_sumout\ = SUM(( \inst2|TSUM[3][29]~combout\ ) + ( GND ) + ( \inst2|Add4~14\ ))
-- \inst2|Add4~10\ = CARRY(( \inst2|TSUM[3][29]~combout\ ) + ( GND ) + ( \inst2|Add4~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[3][29]~combout\,
	cin => \inst2|Add4~14\,
	sumout => \inst2|Add4~9_sumout\,
	cout => \inst2|Add4~10\);

-- Location: LABCELL_X79_Y32_N30
\inst2|Add4~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~5_sumout\ = SUM(( \inst2|TSUM[3][30]~combout\ ) + ( GND ) + ( \inst2|Add4~10\ ))
-- \inst2|Add4~6\ = CARRY(( \inst2|TSUM[3][30]~combout\ ) + ( GND ) + ( \inst2|Add4~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][30]~combout\,
	cin => \inst2|Add4~10\,
	sumout => \inst2|Add4~5_sumout\,
	cout => \inst2|Add4~6\);

-- Location: LABCELL_X79_Y32_N33
\inst2|Add4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add4~1_sumout\ = SUM(( \inst2|TSUM[3][31]~combout\ ) + ( GND ) + ( \inst2|Add4~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[3][31]~combout\,
	cin => \inst2|Add4~6\,
	sumout => \inst2|Add4~1_sumout\);

-- Location: MLABCELL_X82_Y32_N12
\inst2|TSUM[4][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][31]~combout\ = LCELL(( \inst2|Add4~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~1_sumout\,
	combout => \inst2|TSUM[4][31]~combout\);

-- Location: MLABCELL_X82_Y32_N18
\inst2|TSUM[4][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][30]~combout\ = LCELL(( \inst2|Add4~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~5_sumout\,
	combout => \inst2|TSUM[4][30]~combout\);

-- Location: LABCELL_X79_Y32_N39
\inst2|TSUM[4][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][29]~combout\ = LCELL(\inst2|Add4~9_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add4~9_sumout\,
	combout => \inst2|TSUM[4][29]~combout\);

-- Location: LABCELL_X79_Y32_N36
\inst2|TSUM[4][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][28]~combout\ = LCELL(( \inst2|Add4~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~13_sumout\,
	combout => \inst2|TSUM[4][28]~combout\);

-- Location: MLABCELL_X82_Y32_N27
\inst2|TSUM[4][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][27]~combout\ = LCELL(( \inst2|Add4~17_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~17_sumout\,
	combout => \inst2|TSUM[4][27]~combout\);

-- Location: LABCELL_X83_Y32_N51
\inst2|TSUM[4][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][26]~combout\ = LCELL(( \inst2|Add4~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~21_sumout\,
	combout => \inst2|TSUM[4][26]~combout\);

-- Location: MLABCELL_X82_Y32_N57
\inst2|TSUM[4][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][25]~combout\ = LCELL(( \inst2|Add4~25_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~25_sumout\,
	combout => \inst2|TSUM[4][25]~combout\);

-- Location: LABCELL_X79_Y32_N57
\inst2|TSUM[4][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][24]~combout\ = LCELL(( \inst2|Add4~29_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~29_sumout\,
	combout => \inst2|TSUM[4][24]~combout\);

-- Location: LABCELL_X79_Y32_N54
\inst2|TSUM[4][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][23]~combout\ = LCELL(( \inst2|Add4~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~33_sumout\,
	combout => \inst2|TSUM[4][23]~combout\);

-- Location: LABCELL_X79_Y32_N48
\inst2|TSUM[4][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][22]~combout\ = LCELL(( \inst2|Add4~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~37_sumout\,
	combout => \inst2|TSUM[4][22]~combout\);

-- Location: MLABCELL_X82_Y32_N36
\inst2|TSUM[4][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][21]~combout\ = LCELL(( \inst2|Add4~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~41_sumout\,
	combout => \inst2|TSUM[4][21]~combout\);

-- Location: MLABCELL_X82_Y32_N6
\inst2|TSUM[4][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][20]~combout\ = LCELL(( \inst2|Add4~45_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~45_sumout\,
	combout => \inst2|TSUM[4][20]~combout\);

-- Location: LABCELL_X75_Y33_N0
\inst2|TSUM[4][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][19]~combout\ = LCELL(( \inst2|Add4~49_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~49_sumout\,
	combout => \inst2|TSUM[4][19]~combout\);

-- Location: MLABCELL_X82_Y33_N15
\inst2|TSUM[4][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][18]~combout\ = LCELL(( \inst2|Add4~53_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~53_sumout\,
	combout => \inst2|TSUM[4][18]~combout\);

-- Location: LABCELL_X80_Y31_N15
\inst2|TSUM[4][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][17]~combout\ = LCELL(( \inst2|Add4~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~57_sumout\,
	combout => \inst2|TSUM[4][17]~combout\);

-- Location: LABCELL_X80_Y33_N12
\inst2|TSUM[4][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][16]~combout\ = LCELL(( \inst2|Add4~61_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~61_sumout\,
	combout => \inst2|TSUM[4][16]~combout\);

-- Location: MLABCELL_X82_Y33_N18
\inst2|TSUM[4][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][15]~combout\ = LCELL(( \inst2|Add4~65_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~65_sumout\,
	combout => \inst2|TSUM[4][15]~combout\);

-- Location: MLABCELL_X82_Y33_N27
\inst2|TSUM[4][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][14]~combout\ = LCELL(( \inst2|Add4~69_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~69_sumout\,
	combout => \inst2|TSUM[4][14]~combout\);

-- Location: LABCELL_X80_Y33_N33
\inst2|TSUM[4][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][13]~combout\ = LCELL(( \inst2|Add4~73_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~73_sumout\,
	combout => \inst2|TSUM[4][13]~combout\);

-- Location: MLABCELL_X82_Y33_N54
\inst2|TSUM[4][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][12]~combout\ = LCELL(( \inst2|Add4~77_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~77_sumout\,
	combout => \inst2|TSUM[4][12]~combout\);

-- Location: MLABCELL_X82_Y33_N48
\inst2|TSUM[4][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][11]~combout\ = LCELL(( \inst2|Add4~81_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~81_sumout\,
	combout => \inst2|TSUM[4][11]~combout\);

-- Location: LABCELL_X80_Y31_N6
\inst2|TSUM[4][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][10]~combout\ = LCELL(( \inst2|Add4~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~85_sumout\,
	combout => \inst2|TSUM[4][10]~combout\);

-- Location: LABCELL_X75_Y33_N57
\inst2|TSUM[4][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][9]~combout\ = LCELL(( \inst2|Add4~89_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~89_sumout\,
	combout => \inst2|TSUM[4][9]~combout\);

-- Location: MLABCELL_X82_Y33_N6
\inst2|TSUM[4][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][8]~combout\ = LCELL(( \inst2|Add4~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~93_sumout\,
	combout => \inst2|TSUM[4][8]~combout\);

-- Location: MLABCELL_X82_Y33_N3
\inst2|TSUM[4][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][7]~combout\ = LCELL(( \inst2|Add4~97_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~97_sumout\,
	combout => \inst2|TSUM[4][7]~combout\);

-- Location: LABCELL_X80_Y33_N27
\inst2|TSUM[4][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][6]~combout\ = LCELL(( \inst2|Add4~101_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~101_sumout\,
	combout => \inst2|TSUM[4][6]~combout\);

-- Location: MLABCELL_X82_Y33_N45
\inst2|TSUM[4][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][5]~combout\ = LCELL(( \inst2|Add4~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~105_sumout\,
	combout => \inst2|TSUM[4][5]~combout\);

-- Location: LABCELL_X85_Y35_N6
\inst2|PSUM[6][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[6][0]~_wirecell_combout\ = ( !\inst2|PSUM[6][0]~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_PSUM[6][0]~q\,
	combout => \inst2|PSUM[6][0]~_wirecell_combout\);

-- Location: MLABCELL_X84_Y35_N12
\inst2|PSUM[6][5]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[6][5]~2_combout\ = ( \inst7|DIVD~q\ ) # ( !\inst7|DIVD~q\ & ( !\inst6|STEMP\(12) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst6|ALT_INV_STEMP\(12),
	dataf => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[6][5]~2_combout\);

-- Location: FF_X84_Y35_N11
\inst2|PSUM[6][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|PSUM[6][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[6][5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[6][0]~q\);

-- Location: LABCELL_X85_Y35_N33
\inst2|Add11~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add11~4_combout\ = ( !\inst2|PSUM[6][1]~q\ & ( \inst2|PSUM[6][0]~q\ ) ) # ( \inst2|PSUM[6][1]~q\ & ( !\inst2|PSUM[6][0]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_PSUM[6][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[6][0]~q\,
	combout => \inst2|Add11~4_combout\);

-- Location: FF_X85_Y35_N35
\inst2|PSUM[6][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add11~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[6][5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[6][1]~q\);

-- Location: LABCELL_X85_Y35_N18
\inst2|Add11~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add11~3_combout\ = ( \inst2|PSUM[6][0]~q\ & ( !\inst2|PSUM[6][1]~q\ $ (!\inst2|PSUM[6][2]~q\) ) ) # ( !\inst2|PSUM[6][0]~q\ & ( \inst2|PSUM[6][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[6][1]~q\,
	datad => \inst2|ALT_INV_PSUM[6][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[6][0]~q\,
	combout => \inst2|Add11~3_combout\);

-- Location: FF_X84_Y35_N50
\inst2|PSUM[6][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add11~3_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[6][5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[6][2]~q\);

-- Location: MLABCELL_X84_Y35_N30
\inst2|Add11~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add11~2_combout\ = ( \inst2|PSUM[6][0]~q\ & ( !\inst2|PSUM[6][3]~q\ $ (((!\inst2|PSUM[6][1]~q\) # (!\inst2|PSUM[6][2]~q\))) ) ) # ( !\inst2|PSUM[6][0]~q\ & ( \inst2|PSUM[6][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010110010101100101011001010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[6][3]~q\,
	datab => \inst2|ALT_INV_PSUM[6][1]~q\,
	datac => \inst2|ALT_INV_PSUM[6][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[6][0]~q\,
	combout => \inst2|Add11~2_combout\);

-- Location: FF_X84_Y35_N53
\inst2|PSUM[6][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	asdata => \inst2|Add11~2_combout\,
	sclr => \inst7|DIVD~q\,
	sload => VCC,
	ena => \inst2|PSUM[6][5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[6][3]~q\);

-- Location: MLABCELL_X84_Y35_N6
\inst2|Add11~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add11~1_combout\ = ( \inst2|PSUM[6][2]~q\ & ( !\inst2|PSUM[6][4]~q\ $ (((!\inst2|PSUM[6][1]~q\) # ((!\inst2|PSUM[6][0]~q\) # (!\inst2|PSUM[6][3]~q\)))) ) ) # ( !\inst2|PSUM[6][2]~q\ & ( \inst2|PSUM[6][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[6][1]~q\,
	datab => \inst2|ALT_INV_PSUM[6][0]~q\,
	datac => \inst2|ALT_INV_PSUM[6][3]~q\,
	datad => \inst2|ALT_INV_PSUM[6][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[6][2]~q\,
	combout => \inst2|Add11~1_combout\);

-- Location: FF_X84_Y35_N8
\inst2|PSUM[6][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add11~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[6][5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[6][4]~q\);

-- Location: LABCELL_X85_Y35_N24
\inst2|Add11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add11~0_combout\ = ( \inst2|PSUM[6][5]~q\ & ( \inst2|PSUM[6][2]~q\ & ( (!\inst2|PSUM[6][0]~q\) # ((!\inst2|PSUM[6][3]~q\) # ((!\inst2|PSUM[6][1]~q\) # (!\inst2|PSUM[6][4]~q\))) ) ) ) # ( !\inst2|PSUM[6][5]~q\ & ( \inst2|PSUM[6][2]~q\ & ( 
-- (\inst2|PSUM[6][0]~q\ & (\inst2|PSUM[6][3]~q\ & (\inst2|PSUM[6][1]~q\ & \inst2|PSUM[6][4]~q\))) ) ) ) # ( \inst2|PSUM[6][5]~q\ & ( !\inst2|PSUM[6][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[6][0]~q\,
	datab => \inst2|ALT_INV_PSUM[6][3]~q\,
	datac => \inst2|ALT_INV_PSUM[6][1]~q\,
	datad => \inst2|ALT_INV_PSUM[6][4]~q\,
	datae => \inst2|ALT_INV_PSUM[6][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[6][2]~q\,
	combout => \inst2|Add11~0_combout\);

-- Location: FF_X85_Y35_N25
\inst2|PSUM[6][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add11~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[6][5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[6][5]~q\);

-- Location: MLABCELL_X82_Y33_N39
\inst2|TSUM[4][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][4]~combout\ = LCELL(( \inst2|Add4~109_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~109_sumout\,
	combout => \inst2|TSUM[4][4]~combout\);

-- Location: LABCELL_X79_Y35_N48
\inst2|TSUM[4][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][3]~combout\ = LCELL(( \inst2|Add4~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add4~113_sumout\,
	combout => \inst2|TSUM[4][3]~combout\);

-- Location: LABCELL_X80_Y31_N36
\inst2|TSUM[4][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][2]~combout\ = LCELL(( \inst2|Add4~117_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~117_sumout\,
	combout => \inst2|TSUM[4][2]~combout\);

-- Location: MLABCELL_X82_Y33_N42
\inst2|TSUM[4][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][1]~combout\ = LCELL(( \inst2|Add4~121_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~121_sumout\,
	combout => \inst2|TSUM[4][1]~combout\);

-- Location: MLABCELL_X82_Y33_N30
\inst2|TSUM[4][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[4][0]~combout\ = LCELL(( \inst2|Add4~125_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add4~125_sumout\,
	combout => \inst2|TSUM[4][0]~combout\);

-- Location: LABCELL_X83_Y33_N0
\inst2|Add5~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~125_sumout\ = SUM(( \inst2|PSUM[6][0]~q\ ) + ( \inst2|TSUM[4][0]~combout\ ) + ( !VCC ))
-- \inst2|Add5~126\ = CARRY(( \inst2|PSUM[6][0]~q\ ) + ( \inst2|TSUM[4][0]~combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[6][0]~q\,
	datac => \inst2|ALT_INV_TSUM[4][0]~combout\,
	cin => GND,
	sumout => \inst2|Add5~125_sumout\,
	cout => \inst2|Add5~126\);

-- Location: LABCELL_X83_Y33_N3
\inst2|Add5~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~121_sumout\ = SUM(( \inst2|PSUM[6][1]~q\ ) + ( \inst2|TSUM[4][1]~combout\ ) + ( \inst2|Add5~126\ ))
-- \inst2|Add5~122\ = CARRY(( \inst2|PSUM[6][1]~q\ ) + ( \inst2|TSUM[4][1]~combout\ ) + ( \inst2|Add5~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[6][1]~q\,
	datac => \inst2|ALT_INV_TSUM[4][1]~combout\,
	cin => \inst2|Add5~126\,
	sumout => \inst2|Add5~121_sumout\,
	cout => \inst2|Add5~122\);

-- Location: LABCELL_X83_Y33_N6
\inst2|Add5~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~117_sumout\ = SUM(( \inst2|PSUM[6][2]~q\ ) + ( \inst2|TSUM[4][2]~combout\ ) + ( \inst2|Add5~122\ ))
-- \inst2|Add5~118\ = CARRY(( \inst2|PSUM[6][2]~q\ ) + ( \inst2|TSUM[4][2]~combout\ ) + ( \inst2|Add5~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[6][2]~q\,
	datac => \inst2|ALT_INV_TSUM[4][2]~combout\,
	cin => \inst2|Add5~122\,
	sumout => \inst2|Add5~117_sumout\,
	cout => \inst2|Add5~118\);

-- Location: LABCELL_X83_Y33_N9
\inst2|Add5~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~113_sumout\ = SUM(( \inst2|PSUM[6][3]~q\ ) + ( \inst2|TSUM[4][3]~combout\ ) + ( \inst2|Add5~118\ ))
-- \inst2|Add5~114\ = CARRY(( \inst2|PSUM[6][3]~q\ ) + ( \inst2|TSUM[4][3]~combout\ ) + ( \inst2|Add5~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[6][3]~q\,
	datac => \inst2|ALT_INV_TSUM[4][3]~combout\,
	cin => \inst2|Add5~118\,
	sumout => \inst2|Add5~113_sumout\,
	cout => \inst2|Add5~114\);

-- Location: LABCELL_X83_Y33_N12
\inst2|Add5~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~109_sumout\ = SUM(( \inst2|PSUM[6][4]~q\ ) + ( \inst2|TSUM[4][4]~combout\ ) + ( \inst2|Add5~114\ ))
-- \inst2|Add5~110\ = CARRY(( \inst2|PSUM[6][4]~q\ ) + ( \inst2|TSUM[4][4]~combout\ ) + ( \inst2|Add5~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[6][4]~q\,
	datac => \inst2|ALT_INV_TSUM[4][4]~combout\,
	cin => \inst2|Add5~114\,
	sumout => \inst2|Add5~109_sumout\,
	cout => \inst2|Add5~110\);

-- Location: LABCELL_X83_Y33_N15
\inst2|Add5~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~105_sumout\ = SUM(( \inst2|PSUM[6][5]~q\ ) + ( \inst2|TSUM[4][5]~combout\ ) + ( \inst2|Add5~110\ ))
-- \inst2|Add5~106\ = CARRY(( \inst2|PSUM[6][5]~q\ ) + ( \inst2|TSUM[4][5]~combout\ ) + ( \inst2|Add5~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][5]~combout\,
	datac => \inst2|ALT_INV_PSUM[6][5]~q\,
	cin => \inst2|Add5~110\,
	sumout => \inst2|Add5~105_sumout\,
	cout => \inst2|Add5~106\);

-- Location: LABCELL_X83_Y33_N18
\inst2|Add5~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~101_sumout\ = SUM(( \inst2|TSUM[4][6]~combout\ ) + ( GND ) + ( \inst2|Add5~106\ ))
-- \inst2|Add5~102\ = CARRY(( \inst2|TSUM[4][6]~combout\ ) + ( GND ) + ( \inst2|Add5~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][6]~combout\,
	cin => \inst2|Add5~106\,
	sumout => \inst2|Add5~101_sumout\,
	cout => \inst2|Add5~102\);

-- Location: LABCELL_X83_Y33_N21
\inst2|Add5~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~97_sumout\ = SUM(( \inst2|TSUM[4][7]~combout\ ) + ( GND ) + ( \inst2|Add5~102\ ))
-- \inst2|Add5~98\ = CARRY(( \inst2|TSUM[4][7]~combout\ ) + ( GND ) + ( \inst2|Add5~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][7]~combout\,
	cin => \inst2|Add5~102\,
	sumout => \inst2|Add5~97_sumout\,
	cout => \inst2|Add5~98\);

-- Location: LABCELL_X83_Y33_N24
\inst2|Add5~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~93_sumout\ = SUM(( \inst2|TSUM[4][8]~combout\ ) + ( GND ) + ( \inst2|Add5~98\ ))
-- \inst2|Add5~94\ = CARRY(( \inst2|TSUM[4][8]~combout\ ) + ( GND ) + ( \inst2|Add5~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][8]~combout\,
	cin => \inst2|Add5~98\,
	sumout => \inst2|Add5~93_sumout\,
	cout => \inst2|Add5~94\);

-- Location: LABCELL_X83_Y33_N27
\inst2|Add5~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~89_sumout\ = SUM(( \inst2|TSUM[4][9]~combout\ ) + ( GND ) + ( \inst2|Add5~94\ ))
-- \inst2|Add5~90\ = CARRY(( \inst2|TSUM[4][9]~combout\ ) + ( GND ) + ( \inst2|Add5~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][9]~combout\,
	cin => \inst2|Add5~94\,
	sumout => \inst2|Add5~89_sumout\,
	cout => \inst2|Add5~90\);

-- Location: LABCELL_X83_Y33_N30
\inst2|Add5~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~85_sumout\ = SUM(( \inst2|TSUM[4][10]~combout\ ) + ( GND ) + ( \inst2|Add5~90\ ))
-- \inst2|Add5~86\ = CARRY(( \inst2|TSUM[4][10]~combout\ ) + ( GND ) + ( \inst2|Add5~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][10]~combout\,
	cin => \inst2|Add5~90\,
	sumout => \inst2|Add5~85_sumout\,
	cout => \inst2|Add5~86\);

-- Location: LABCELL_X83_Y33_N33
\inst2|Add5~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~81_sumout\ = SUM(( \inst2|TSUM[4][11]~combout\ ) + ( GND ) + ( \inst2|Add5~86\ ))
-- \inst2|Add5~82\ = CARRY(( \inst2|TSUM[4][11]~combout\ ) + ( GND ) + ( \inst2|Add5~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][11]~combout\,
	cin => \inst2|Add5~86\,
	sumout => \inst2|Add5~81_sumout\,
	cout => \inst2|Add5~82\);

-- Location: LABCELL_X83_Y33_N36
\inst2|Add5~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~77_sumout\ = SUM(( \inst2|TSUM[4][12]~combout\ ) + ( GND ) + ( \inst2|Add5~82\ ))
-- \inst2|Add5~78\ = CARRY(( \inst2|TSUM[4][12]~combout\ ) + ( GND ) + ( \inst2|Add5~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][12]~combout\,
	cin => \inst2|Add5~82\,
	sumout => \inst2|Add5~77_sumout\,
	cout => \inst2|Add5~78\);

-- Location: LABCELL_X83_Y33_N39
\inst2|Add5~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~73_sumout\ = SUM(( \inst2|TSUM[4][13]~combout\ ) + ( GND ) + ( \inst2|Add5~78\ ))
-- \inst2|Add5~74\ = CARRY(( \inst2|TSUM[4][13]~combout\ ) + ( GND ) + ( \inst2|Add5~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][13]~combout\,
	cin => \inst2|Add5~78\,
	sumout => \inst2|Add5~73_sumout\,
	cout => \inst2|Add5~74\);

-- Location: LABCELL_X83_Y33_N42
\inst2|Add5~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~69_sumout\ = SUM(( \inst2|TSUM[4][14]~combout\ ) + ( GND ) + ( \inst2|Add5~74\ ))
-- \inst2|Add5~70\ = CARRY(( \inst2|TSUM[4][14]~combout\ ) + ( GND ) + ( \inst2|Add5~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][14]~combout\,
	cin => \inst2|Add5~74\,
	sumout => \inst2|Add5~69_sumout\,
	cout => \inst2|Add5~70\);

-- Location: LABCELL_X83_Y33_N45
\inst2|Add5~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~65_sumout\ = SUM(( \inst2|TSUM[4][15]~combout\ ) + ( GND ) + ( \inst2|Add5~70\ ))
-- \inst2|Add5~66\ = CARRY(( \inst2|TSUM[4][15]~combout\ ) + ( GND ) + ( \inst2|Add5~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][15]~combout\,
	cin => \inst2|Add5~70\,
	sumout => \inst2|Add5~65_sumout\,
	cout => \inst2|Add5~66\);

-- Location: LABCELL_X83_Y33_N48
\inst2|Add5~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~61_sumout\ = SUM(( \inst2|TSUM[4][16]~combout\ ) + ( GND ) + ( \inst2|Add5~66\ ))
-- \inst2|Add5~62\ = CARRY(( \inst2|TSUM[4][16]~combout\ ) + ( GND ) + ( \inst2|Add5~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][16]~combout\,
	cin => \inst2|Add5~66\,
	sumout => \inst2|Add5~61_sumout\,
	cout => \inst2|Add5~62\);

-- Location: LABCELL_X83_Y33_N51
\inst2|Add5~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~57_sumout\ = SUM(( \inst2|TSUM[4][17]~combout\ ) + ( GND ) + ( \inst2|Add5~62\ ))
-- \inst2|Add5~58\ = CARRY(( \inst2|TSUM[4][17]~combout\ ) + ( GND ) + ( \inst2|Add5~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][17]~combout\,
	cin => \inst2|Add5~62\,
	sumout => \inst2|Add5~57_sumout\,
	cout => \inst2|Add5~58\);

-- Location: LABCELL_X83_Y33_N54
\inst2|Add5~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~53_sumout\ = SUM(( \inst2|TSUM[4][18]~combout\ ) + ( GND ) + ( \inst2|Add5~58\ ))
-- \inst2|Add5~54\ = CARRY(( \inst2|TSUM[4][18]~combout\ ) + ( GND ) + ( \inst2|Add5~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][18]~combout\,
	cin => \inst2|Add5~58\,
	sumout => \inst2|Add5~53_sumout\,
	cout => \inst2|Add5~54\);

-- Location: LABCELL_X83_Y33_N57
\inst2|Add5~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~49_sumout\ = SUM(( \inst2|TSUM[4][19]~combout\ ) + ( GND ) + ( \inst2|Add5~54\ ))
-- \inst2|Add5~50\ = CARRY(( \inst2|TSUM[4][19]~combout\ ) + ( GND ) + ( \inst2|Add5~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][19]~combout\,
	cin => \inst2|Add5~54\,
	sumout => \inst2|Add5~49_sumout\,
	cout => \inst2|Add5~50\);

-- Location: LABCELL_X83_Y32_N0
\inst2|Add5~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~45_sumout\ = SUM(( \inst2|TSUM[4][20]~combout\ ) + ( GND ) + ( \inst2|Add5~50\ ))
-- \inst2|Add5~46\ = CARRY(( \inst2|TSUM[4][20]~combout\ ) + ( GND ) + ( \inst2|Add5~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][20]~combout\,
	cin => \inst2|Add5~50\,
	sumout => \inst2|Add5~45_sumout\,
	cout => \inst2|Add5~46\);

-- Location: LABCELL_X83_Y32_N3
\inst2|Add5~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~41_sumout\ = SUM(( \inst2|TSUM[4][21]~combout\ ) + ( GND ) + ( \inst2|Add5~46\ ))
-- \inst2|Add5~42\ = CARRY(( \inst2|TSUM[4][21]~combout\ ) + ( GND ) + ( \inst2|Add5~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][21]~combout\,
	cin => \inst2|Add5~46\,
	sumout => \inst2|Add5~41_sumout\,
	cout => \inst2|Add5~42\);

-- Location: LABCELL_X83_Y32_N6
\inst2|Add5~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~37_sumout\ = SUM(( \inst2|TSUM[4][22]~combout\ ) + ( GND ) + ( \inst2|Add5~42\ ))
-- \inst2|Add5~38\ = CARRY(( \inst2|TSUM[4][22]~combout\ ) + ( GND ) + ( \inst2|Add5~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][22]~combout\,
	cin => \inst2|Add5~42\,
	sumout => \inst2|Add5~37_sumout\,
	cout => \inst2|Add5~38\);

-- Location: LABCELL_X83_Y32_N9
\inst2|Add5~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~33_sumout\ = SUM(( \inst2|TSUM[4][23]~combout\ ) + ( GND ) + ( \inst2|Add5~38\ ))
-- \inst2|Add5~34\ = CARRY(( \inst2|TSUM[4][23]~combout\ ) + ( GND ) + ( \inst2|Add5~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][23]~combout\,
	cin => \inst2|Add5~38\,
	sumout => \inst2|Add5~33_sumout\,
	cout => \inst2|Add5~34\);

-- Location: LABCELL_X83_Y32_N12
\inst2|Add5~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~29_sumout\ = SUM(( \inst2|TSUM[4][24]~combout\ ) + ( GND ) + ( \inst2|Add5~34\ ))
-- \inst2|Add5~30\ = CARRY(( \inst2|TSUM[4][24]~combout\ ) + ( GND ) + ( \inst2|Add5~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][24]~combout\,
	cin => \inst2|Add5~34\,
	sumout => \inst2|Add5~29_sumout\,
	cout => \inst2|Add5~30\);

-- Location: LABCELL_X83_Y32_N15
\inst2|Add5~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~25_sumout\ = SUM(( \inst2|TSUM[4][25]~combout\ ) + ( GND ) + ( \inst2|Add5~30\ ))
-- \inst2|Add5~26\ = CARRY(( \inst2|TSUM[4][25]~combout\ ) + ( GND ) + ( \inst2|Add5~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][25]~combout\,
	cin => \inst2|Add5~30\,
	sumout => \inst2|Add5~25_sumout\,
	cout => \inst2|Add5~26\);

-- Location: LABCELL_X83_Y32_N18
\inst2|Add5~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~21_sumout\ = SUM(( \inst2|TSUM[4][26]~combout\ ) + ( GND ) + ( \inst2|Add5~26\ ))
-- \inst2|Add5~22\ = CARRY(( \inst2|TSUM[4][26]~combout\ ) + ( GND ) + ( \inst2|Add5~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][26]~combout\,
	cin => \inst2|Add5~26\,
	sumout => \inst2|Add5~21_sumout\,
	cout => \inst2|Add5~22\);

-- Location: LABCELL_X83_Y32_N21
\inst2|Add5~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~17_sumout\ = SUM(( \inst2|TSUM[4][27]~combout\ ) + ( GND ) + ( \inst2|Add5~22\ ))
-- \inst2|Add5~18\ = CARRY(( \inst2|TSUM[4][27]~combout\ ) + ( GND ) + ( \inst2|Add5~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][27]~combout\,
	cin => \inst2|Add5~22\,
	sumout => \inst2|Add5~17_sumout\,
	cout => \inst2|Add5~18\);

-- Location: LABCELL_X83_Y32_N24
\inst2|Add5~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~13_sumout\ = SUM(( \inst2|TSUM[4][28]~combout\ ) + ( GND ) + ( \inst2|Add5~18\ ))
-- \inst2|Add5~14\ = CARRY(( \inst2|TSUM[4][28]~combout\ ) + ( GND ) + ( \inst2|Add5~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][28]~combout\,
	cin => \inst2|Add5~18\,
	sumout => \inst2|Add5~13_sumout\,
	cout => \inst2|Add5~14\);

-- Location: LABCELL_X83_Y32_N27
\inst2|Add5~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~9_sumout\ = SUM(( \inst2|TSUM[4][29]~combout\ ) + ( GND ) + ( \inst2|Add5~14\ ))
-- \inst2|Add5~10\ = CARRY(( \inst2|TSUM[4][29]~combout\ ) + ( GND ) + ( \inst2|Add5~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[4][29]~combout\,
	cin => \inst2|Add5~14\,
	sumout => \inst2|Add5~9_sumout\,
	cout => \inst2|Add5~10\);

-- Location: LABCELL_X83_Y32_N30
\inst2|Add5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~5_sumout\ = SUM(( \inst2|TSUM[4][30]~combout\ ) + ( GND ) + ( \inst2|Add5~10\ ))
-- \inst2|Add5~6\ = CARRY(( \inst2|TSUM[4][30]~combout\ ) + ( GND ) + ( \inst2|Add5~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[4][30]~combout\,
	cin => \inst2|Add5~10\,
	sumout => \inst2|Add5~5_sumout\,
	cout => \inst2|Add5~6\);

-- Location: LABCELL_X83_Y32_N33
\inst2|Add5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add5~1_sumout\ = SUM(( \inst2|TSUM[4][31]~combout\ ) + ( GND ) + ( \inst2|Add5~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[4][31]~combout\,
	cin => \inst2|Add5~6\,
	sumout => \inst2|Add5~1_sumout\);

-- Location: MLABCELL_X84_Y32_N33
\inst2|TSUM[5][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][31]~combout\ = LCELL(( \inst2|Add5~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~1_sumout\,
	combout => \inst2|TSUM[5][31]~combout\);

-- Location: MLABCELL_X84_Y32_N12
\inst2|TSUM[5][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][30]~combout\ = LCELL(( \inst2|Add5~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~5_sumout\,
	combout => \inst2|TSUM[5][30]~combout\);

-- Location: MLABCELL_X84_Y32_N21
\inst2|TSUM[5][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][29]~combout\ = LCELL(( \inst2|Add5~9_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~9_sumout\,
	combout => \inst2|TSUM[5][29]~combout\);

-- Location: MLABCELL_X84_Y32_N27
\inst2|TSUM[5][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][28]~combout\ = LCELL(( \inst2|Add5~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~13_sumout\,
	combout => \inst2|TSUM[5][28]~combout\);

-- Location: LABCELL_X83_Y32_N36
\inst2|TSUM[5][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][27]~combout\ = LCELL(( \inst2|Add5~17_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~17_sumout\,
	combout => \inst2|TSUM[5][27]~combout\);

-- Location: MLABCELL_X84_Y32_N15
\inst2|TSUM[5][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][26]~combout\ = LCELL(( \inst2|Add5~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~21_sumout\,
	combout => \inst2|TSUM[5][26]~combout\);

-- Location: MLABCELL_X84_Y32_N45
\inst2|TSUM[5][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][25]~combout\ = LCELL(( \inst2|Add5~25_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~25_sumout\,
	combout => \inst2|TSUM[5][25]~combout\);

-- Location: MLABCELL_X84_Y32_N48
\inst2|TSUM[5][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][24]~combout\ = LCELL(( \inst2|Add5~29_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~29_sumout\,
	combout => \inst2|TSUM[5][24]~combout\);

-- Location: MLABCELL_X84_Y32_N57
\inst2|TSUM[5][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][23]~combout\ = LCELL(( \inst2|Add5~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~33_sumout\,
	combout => \inst2|TSUM[5][23]~combout\);

-- Location: LABCELL_X83_Y32_N57
\inst2|TSUM[5][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][22]~combout\ = LCELL(( \inst2|Add5~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~37_sumout\,
	combout => \inst2|TSUM[5][22]~combout\);

-- Location: MLABCELL_X84_Y32_N0
\inst2|TSUM[5][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][21]~combout\ = LCELL(( \inst2|Add5~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~41_sumout\,
	combout => \inst2|TSUM[5][21]~combout\);

-- Location: MLABCELL_X84_Y32_N6
\inst2|TSUM[5][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][20]~combout\ = LCELL(( \inst2|Add5~45_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~45_sumout\,
	combout => \inst2|TSUM[5][20]~combout\);

-- Location: MLABCELL_X84_Y34_N51
\inst2|TSUM[5][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][19]~combout\ = LCELL(( \inst2|Add5~49_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~49_sumout\,
	combout => \inst2|TSUM[5][19]~combout\);

-- Location: MLABCELL_X84_Y34_N9
\inst2|TSUM[5][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][18]~combout\ = LCELL(( \inst2|Add5~53_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~53_sumout\,
	combout => \inst2|TSUM[5][18]~combout\);

-- Location: MLABCELL_X84_Y33_N57
\inst2|TSUM[5][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][17]~combout\ = LCELL(( \inst2|Add5~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~57_sumout\,
	combout => \inst2|TSUM[5][17]~combout\);

-- Location: MLABCELL_X84_Y33_N0
\inst2|TSUM[5][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][16]~combout\ = LCELL(\inst2|Add5~61_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add5~61_sumout\,
	combout => \inst2|TSUM[5][16]~combout\);

-- Location: MLABCELL_X84_Y33_N45
\inst2|TSUM[5][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][15]~combout\ = LCELL(( \inst2|Add5~65_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~65_sumout\,
	combout => \inst2|TSUM[5][15]~combout\);

-- Location: MLABCELL_X84_Y33_N12
\inst2|TSUM[5][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][14]~combout\ = LCELL(( \inst2|Add5~69_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~69_sumout\,
	combout => \inst2|TSUM[5][14]~combout\);

-- Location: MLABCELL_X84_Y33_N51
\inst2|TSUM[5][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][13]~combout\ = LCELL(( \inst2|Add5~73_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~73_sumout\,
	combout => \inst2|TSUM[5][13]~combout\);

-- Location: MLABCELL_X84_Y33_N54
\inst2|TSUM[5][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][12]~combout\ = LCELL(( \inst2|Add5~77_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~77_sumout\,
	combout => \inst2|TSUM[5][12]~combout\);

-- Location: MLABCELL_X84_Y33_N3
\inst2|TSUM[5][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][11]~combout\ = LCELL(\inst2|Add5~81_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add5~81_sumout\,
	combout => \inst2|TSUM[5][11]~combout\);

-- Location: MLABCELL_X84_Y33_N6
\inst2|TSUM[5][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][10]~combout\ = LCELL(( \inst2|Add5~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~85_sumout\,
	combout => \inst2|TSUM[5][10]~combout\);

-- Location: MLABCELL_X84_Y33_N27
\inst2|TSUM[5][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][9]~combout\ = LCELL(\inst2|Add5~89_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add5~89_sumout\,
	combout => \inst2|TSUM[5][9]~combout\);

-- Location: MLABCELL_X84_Y33_N24
\inst2|TSUM[5][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][8]~combout\ = LCELL(( \inst2|Add5~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~93_sumout\,
	combout => \inst2|TSUM[5][8]~combout\);

-- Location: MLABCELL_X84_Y33_N42
\inst2|TSUM[5][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][7]~combout\ = LCELL(\inst2|Add5~97_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add5~97_sumout\,
	combout => \inst2|TSUM[5][7]~combout\);

-- Location: LABCELL_X83_Y34_N12
\inst2|TSUM[5][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][6]~combout\ = LCELL(( \inst2|Add5~101_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~101_sumout\,
	combout => \inst2|TSUM[5][6]~combout\);

-- Location: MLABCELL_X87_Y35_N45
\inst2|PSUM[7][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[7][0]~_wirecell_combout\ = !\inst2|PSUM[7][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[7][0]~q\,
	combout => \inst2|PSUM[7][0]~_wirecell_combout\);

-- Location: MLABCELL_X87_Y35_N18
\inst2|PSUM[7][2]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[7][2]~1_combout\ = ( \inst7|DIVD~q\ ) # ( !\inst7|DIVD~q\ & ( !\inst6|STEMP\(14) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011001100110011001100110011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst6|ALT_INV_STEMP\(14),
	dataf => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[7][2]~1_combout\);

-- Location: FF_X87_Y35_N47
\inst2|PSUM[7][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|PSUM[7][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[7][2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[7][0]~q\);

-- Location: MLABCELL_X87_Y35_N21
\inst2|Add10~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add10~4_combout\ = ( \inst2|PSUM[7][0]~q\ & ( !\inst2|PSUM[7][1]~q\ ) ) # ( !\inst2|PSUM[7][0]~q\ & ( \inst2|PSUM[7][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[7][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[7][0]~q\,
	combout => \inst2|Add10~4_combout\);

-- Location: FF_X87_Y35_N23
\inst2|PSUM[7][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add10~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[7][2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[7][1]~q\);

-- Location: MLABCELL_X87_Y35_N36
\inst2|Add10~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add10~3_combout\ = ( \inst2|PSUM[7][0]~q\ & ( !\inst2|PSUM[7][1]~q\ $ (!\inst2|PSUM[7][2]~q\) ) ) # ( !\inst2|PSUM[7][0]~q\ & ( \inst2|PSUM[7][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[7][1]~q\,
	datad => \inst2|ALT_INV_PSUM[7][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[7][0]~q\,
	combout => \inst2|Add10~3_combout\);

-- Location: FF_X87_Y35_N38
\inst2|PSUM[7][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add10~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[7][2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[7][2]~q\);

-- Location: MLABCELL_X87_Y35_N39
\inst2|Add10~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add10~2_combout\ = ( \inst2|PSUM[7][2]~q\ & ( !\inst2|PSUM[7][3]~q\ $ (((!\inst2|PSUM[7][1]~q\) # (!\inst2|PSUM[7][0]~q\))) ) ) # ( !\inst2|PSUM[7][2]~q\ & ( \inst2|PSUM[7][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[7][1]~q\,
	datac => \inst2|ALT_INV_PSUM[7][0]~q\,
	datad => \inst2|ALT_INV_PSUM[7][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[7][2]~q\,
	combout => \inst2|Add10~2_combout\);

-- Location: FF_X87_Y35_N41
\inst2|PSUM[7][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add10~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[7][2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[7][3]~q\);

-- Location: MLABCELL_X87_Y35_N42
\inst2|Add10~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add10~1_combout\ = ( \inst2|PSUM[7][3]~q\ & ( !\inst2|PSUM[7][4]~q\ $ (((!\inst2|PSUM[7][1]~q\) # ((!\inst2|PSUM[7][0]~q\) # (!\inst2|PSUM[7][2]~q\)))) ) ) # ( !\inst2|PSUM[7][3]~q\ & ( \inst2|PSUM[7][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[7][1]~q\,
	datab => \inst2|ALT_INV_PSUM[7][0]~q\,
	datac => \inst2|ALT_INV_PSUM[7][2]~q\,
	datad => \inst2|ALT_INV_PSUM[7][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[7][3]~q\,
	combout => \inst2|Add10~1_combout\);

-- Location: FF_X87_Y35_N44
\inst2|PSUM[7][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add10~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[7][2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[7][4]~q\);

-- Location: MLABCELL_X87_Y35_N48
\inst2|Add10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add10~0_combout\ = ( \inst2|PSUM[7][5]~q\ & ( \inst2|PSUM[7][3]~q\ & ( (!\inst2|PSUM[7][1]~q\) # ((!\inst2|PSUM[7][0]~q\) # ((!\inst2|PSUM[7][2]~q\) # (!\inst2|PSUM[7][4]~q\))) ) ) ) # ( !\inst2|PSUM[7][5]~q\ & ( \inst2|PSUM[7][3]~q\ & ( 
-- (\inst2|PSUM[7][1]~q\ & (\inst2|PSUM[7][0]~q\ & (\inst2|PSUM[7][2]~q\ & \inst2|PSUM[7][4]~q\))) ) ) ) # ( \inst2|PSUM[7][5]~q\ & ( !\inst2|PSUM[7][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[7][1]~q\,
	datab => \inst2|ALT_INV_PSUM[7][0]~q\,
	datac => \inst2|ALT_INV_PSUM[7][2]~q\,
	datad => \inst2|ALT_INV_PSUM[7][4]~q\,
	datae => \inst2|ALT_INV_PSUM[7][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[7][3]~q\,
	combout => \inst2|Add10~0_combout\);

-- Location: FF_X87_Y35_N49
\inst2|PSUM[7][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add10~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[7][2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[7][5]~q\);

-- Location: LABCELL_X83_Y34_N33
\inst2|TSUM[5][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][5]~combout\ = LCELL(( \inst2|Add5~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~105_sumout\,
	combout => \inst2|TSUM[5][5]~combout\);

-- Location: MLABCELL_X84_Y33_N18
\inst2|TSUM[5][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][4]~combout\ = LCELL(( \inst2|Add5~109_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~109_sumout\,
	combout => \inst2|TSUM[5][4]~combout\);

-- Location: LABCELL_X83_Y34_N36
\inst2|TSUM[5][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][3]~combout\ = LCELL(( \inst2|Add5~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add5~113_sumout\,
	combout => \inst2|TSUM[5][3]~combout\);

-- Location: MLABCELL_X84_Y33_N39
\inst2|TSUM[5][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][2]~combout\ = LCELL(\inst2|Add5~117_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add5~117_sumout\,
	combout => \inst2|TSUM[5][2]~combout\);

-- Location: MLABCELL_X84_Y33_N33
\inst2|TSUM[5][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][1]~combout\ = LCELL(( \inst2|Add5~121_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~121_sumout\,
	combout => \inst2|TSUM[5][1]~combout\);

-- Location: MLABCELL_X84_Y33_N36
\inst2|TSUM[5][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[5][0]~combout\ = LCELL(( \inst2|Add5~125_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add5~125_sumout\,
	combout => \inst2|TSUM[5][0]~combout\);

-- Location: LABCELL_X85_Y33_N0
\inst2|Add6~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~125_sumout\ = SUM(( \inst2|TSUM[5][0]~combout\ ) + ( \inst2|PSUM[7][0]~q\ ) + ( !VCC ))
-- \inst2|Add6~126\ = CARRY(( \inst2|TSUM[5][0]~combout\ ) + ( \inst2|PSUM[7][0]~q\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][0]~combout\,
	dataf => \inst2|ALT_INV_PSUM[7][0]~q\,
	cin => GND,
	sumout => \inst2|Add6~125_sumout\,
	cout => \inst2|Add6~126\);

-- Location: LABCELL_X85_Y33_N3
\inst2|Add6~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~121_sumout\ = SUM(( \inst2|PSUM[7][1]~q\ ) + ( \inst2|TSUM[5][1]~combout\ ) + ( \inst2|Add6~126\ ))
-- \inst2|Add6~122\ = CARRY(( \inst2|PSUM[7][1]~q\ ) + ( \inst2|TSUM[5][1]~combout\ ) + ( \inst2|Add6~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[7][1]~q\,
	datac => \inst2|ALT_INV_TSUM[5][1]~combout\,
	cin => \inst2|Add6~126\,
	sumout => \inst2|Add6~121_sumout\,
	cout => \inst2|Add6~122\);

-- Location: LABCELL_X85_Y33_N6
\inst2|Add6~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~117_sumout\ = SUM(( \inst2|PSUM[7][2]~q\ ) + ( \inst2|TSUM[5][2]~combout\ ) + ( \inst2|Add6~122\ ))
-- \inst2|Add6~118\ = CARRY(( \inst2|PSUM[7][2]~q\ ) + ( \inst2|TSUM[5][2]~combout\ ) + ( \inst2|Add6~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[7][2]~q\,
	datac => \inst2|ALT_INV_TSUM[5][2]~combout\,
	cin => \inst2|Add6~122\,
	sumout => \inst2|Add6~117_sumout\,
	cout => \inst2|Add6~118\);

-- Location: LABCELL_X85_Y33_N9
\inst2|Add6~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~113_sumout\ = SUM(( \inst2|PSUM[7][3]~q\ ) + ( \inst2|TSUM[5][3]~combout\ ) + ( \inst2|Add6~118\ ))
-- \inst2|Add6~114\ = CARRY(( \inst2|PSUM[7][3]~q\ ) + ( \inst2|TSUM[5][3]~combout\ ) + ( \inst2|Add6~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[5][3]~combout\,
	datac => \inst2|ALT_INV_PSUM[7][3]~q\,
	cin => \inst2|Add6~118\,
	sumout => \inst2|Add6~113_sumout\,
	cout => \inst2|Add6~114\);

-- Location: LABCELL_X85_Y33_N12
\inst2|Add6~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~109_sumout\ = SUM(( \inst2|PSUM[7][4]~q\ ) + ( \inst2|TSUM[5][4]~combout\ ) + ( \inst2|Add6~114\ ))
-- \inst2|Add6~110\ = CARRY(( \inst2|PSUM[7][4]~q\ ) + ( \inst2|TSUM[5][4]~combout\ ) + ( \inst2|Add6~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][4]~combout\,
	datac => \inst2|ALT_INV_PSUM[7][4]~q\,
	cin => \inst2|Add6~114\,
	sumout => \inst2|Add6~109_sumout\,
	cout => \inst2|Add6~110\);

-- Location: LABCELL_X85_Y33_N15
\inst2|Add6~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~105_sumout\ = SUM(( \inst2|PSUM[7][5]~q\ ) + ( \inst2|TSUM[5][5]~combout\ ) + ( \inst2|Add6~110\ ))
-- \inst2|Add6~106\ = CARRY(( \inst2|PSUM[7][5]~q\ ) + ( \inst2|TSUM[5][5]~combout\ ) + ( \inst2|Add6~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[7][5]~q\,
	dataf => \inst2|ALT_INV_TSUM[5][5]~combout\,
	cin => \inst2|Add6~110\,
	sumout => \inst2|Add6~105_sumout\,
	cout => \inst2|Add6~106\);

-- Location: LABCELL_X85_Y33_N18
\inst2|Add6~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~101_sumout\ = SUM(( \inst2|TSUM[5][6]~combout\ ) + ( GND ) + ( \inst2|Add6~106\ ))
-- \inst2|Add6~102\ = CARRY(( \inst2|TSUM[5][6]~combout\ ) + ( GND ) + ( \inst2|Add6~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][6]~combout\,
	cin => \inst2|Add6~106\,
	sumout => \inst2|Add6~101_sumout\,
	cout => \inst2|Add6~102\);

-- Location: LABCELL_X85_Y33_N21
\inst2|Add6~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~97_sumout\ = SUM(( \inst2|TSUM[5][7]~combout\ ) + ( GND ) + ( \inst2|Add6~102\ ))
-- \inst2|Add6~98\ = CARRY(( \inst2|TSUM[5][7]~combout\ ) + ( GND ) + ( \inst2|Add6~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][7]~combout\,
	cin => \inst2|Add6~102\,
	sumout => \inst2|Add6~97_sumout\,
	cout => \inst2|Add6~98\);

-- Location: LABCELL_X85_Y33_N24
\inst2|Add6~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~93_sumout\ = SUM(( \inst2|TSUM[5][8]~combout\ ) + ( GND ) + ( \inst2|Add6~98\ ))
-- \inst2|Add6~94\ = CARRY(( \inst2|TSUM[5][8]~combout\ ) + ( GND ) + ( \inst2|Add6~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][8]~combout\,
	cin => \inst2|Add6~98\,
	sumout => \inst2|Add6~93_sumout\,
	cout => \inst2|Add6~94\);

-- Location: LABCELL_X85_Y33_N27
\inst2|Add6~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~89_sumout\ = SUM(( \inst2|TSUM[5][9]~combout\ ) + ( GND ) + ( \inst2|Add6~94\ ))
-- \inst2|Add6~90\ = CARRY(( \inst2|TSUM[5][9]~combout\ ) + ( GND ) + ( \inst2|Add6~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[5][9]~combout\,
	cin => \inst2|Add6~94\,
	sumout => \inst2|Add6~89_sumout\,
	cout => \inst2|Add6~90\);

-- Location: LABCELL_X85_Y33_N30
\inst2|Add6~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~85_sumout\ = SUM(( \inst2|TSUM[5][10]~combout\ ) + ( GND ) + ( \inst2|Add6~90\ ))
-- \inst2|Add6~86\ = CARRY(( \inst2|TSUM[5][10]~combout\ ) + ( GND ) + ( \inst2|Add6~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][10]~combout\,
	cin => \inst2|Add6~90\,
	sumout => \inst2|Add6~85_sumout\,
	cout => \inst2|Add6~86\);

-- Location: LABCELL_X85_Y33_N33
\inst2|Add6~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~81_sumout\ = SUM(( \inst2|TSUM[5][11]~combout\ ) + ( GND ) + ( \inst2|Add6~86\ ))
-- \inst2|Add6~82\ = CARRY(( \inst2|TSUM[5][11]~combout\ ) + ( GND ) + ( \inst2|Add6~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][11]~combout\,
	cin => \inst2|Add6~86\,
	sumout => \inst2|Add6~81_sumout\,
	cout => \inst2|Add6~82\);

-- Location: LABCELL_X85_Y33_N36
\inst2|Add6~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~77_sumout\ = SUM(( \inst2|TSUM[5][12]~combout\ ) + ( GND ) + ( \inst2|Add6~82\ ))
-- \inst2|Add6~78\ = CARRY(( \inst2|TSUM[5][12]~combout\ ) + ( GND ) + ( \inst2|Add6~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][12]~combout\,
	cin => \inst2|Add6~82\,
	sumout => \inst2|Add6~77_sumout\,
	cout => \inst2|Add6~78\);

-- Location: LABCELL_X85_Y33_N39
\inst2|Add6~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~73_sumout\ = SUM(( \inst2|TSUM[5][13]~combout\ ) + ( GND ) + ( \inst2|Add6~78\ ))
-- \inst2|Add6~74\ = CARRY(( \inst2|TSUM[5][13]~combout\ ) + ( GND ) + ( \inst2|Add6~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[5][13]~combout\,
	cin => \inst2|Add6~78\,
	sumout => \inst2|Add6~73_sumout\,
	cout => \inst2|Add6~74\);

-- Location: LABCELL_X85_Y33_N42
\inst2|Add6~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~69_sumout\ = SUM(( \inst2|TSUM[5][14]~combout\ ) + ( GND ) + ( \inst2|Add6~74\ ))
-- \inst2|Add6~70\ = CARRY(( \inst2|TSUM[5][14]~combout\ ) + ( GND ) + ( \inst2|Add6~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][14]~combout\,
	cin => \inst2|Add6~74\,
	sumout => \inst2|Add6~69_sumout\,
	cout => \inst2|Add6~70\);

-- Location: LABCELL_X85_Y33_N45
\inst2|Add6~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~65_sumout\ = SUM(( \inst2|TSUM[5][15]~combout\ ) + ( GND ) + ( \inst2|Add6~70\ ))
-- \inst2|Add6~66\ = CARRY(( \inst2|TSUM[5][15]~combout\ ) + ( GND ) + ( \inst2|Add6~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[5][15]~combout\,
	cin => \inst2|Add6~70\,
	sumout => \inst2|Add6~65_sumout\,
	cout => \inst2|Add6~66\);

-- Location: LABCELL_X85_Y33_N48
\inst2|Add6~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~61_sumout\ = SUM(( \inst2|TSUM[5][16]~combout\ ) + ( GND ) + ( \inst2|Add6~66\ ))
-- \inst2|Add6~62\ = CARRY(( \inst2|TSUM[5][16]~combout\ ) + ( GND ) + ( \inst2|Add6~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][16]~combout\,
	cin => \inst2|Add6~66\,
	sumout => \inst2|Add6~61_sumout\,
	cout => \inst2|Add6~62\);

-- Location: LABCELL_X85_Y33_N51
\inst2|Add6~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~57_sumout\ = SUM(( \inst2|TSUM[5][17]~combout\ ) + ( GND ) + ( \inst2|Add6~62\ ))
-- \inst2|Add6~58\ = CARRY(( \inst2|TSUM[5][17]~combout\ ) + ( GND ) + ( \inst2|Add6~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][17]~combout\,
	cin => \inst2|Add6~62\,
	sumout => \inst2|Add6~57_sumout\,
	cout => \inst2|Add6~58\);

-- Location: LABCELL_X85_Y33_N54
\inst2|Add6~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~53_sumout\ = SUM(( \inst2|TSUM[5][18]~combout\ ) + ( GND ) + ( \inst2|Add6~58\ ))
-- \inst2|Add6~54\ = CARRY(( \inst2|TSUM[5][18]~combout\ ) + ( GND ) + ( \inst2|Add6~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][18]~combout\,
	cin => \inst2|Add6~58\,
	sumout => \inst2|Add6~53_sumout\,
	cout => \inst2|Add6~54\);

-- Location: LABCELL_X85_Y33_N57
\inst2|Add6~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~49_sumout\ = SUM(( \inst2|TSUM[5][19]~combout\ ) + ( GND ) + ( \inst2|Add6~54\ ))
-- \inst2|Add6~50\ = CARRY(( \inst2|TSUM[5][19]~combout\ ) + ( GND ) + ( \inst2|Add6~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][19]~combout\,
	cin => \inst2|Add6~54\,
	sumout => \inst2|Add6~49_sumout\,
	cout => \inst2|Add6~50\);

-- Location: LABCELL_X85_Y32_N0
\inst2|Add6~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~45_sumout\ = SUM(( \inst2|TSUM[5][20]~combout\ ) + ( GND ) + ( \inst2|Add6~50\ ))
-- \inst2|Add6~46\ = CARRY(( \inst2|TSUM[5][20]~combout\ ) + ( GND ) + ( \inst2|Add6~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][20]~combout\,
	cin => \inst2|Add6~50\,
	sumout => \inst2|Add6~45_sumout\,
	cout => \inst2|Add6~46\);

-- Location: LABCELL_X85_Y32_N3
\inst2|Add6~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~41_sumout\ = SUM(( \inst2|TSUM[5][21]~combout\ ) + ( GND ) + ( \inst2|Add6~46\ ))
-- \inst2|Add6~42\ = CARRY(( \inst2|TSUM[5][21]~combout\ ) + ( GND ) + ( \inst2|Add6~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][21]~combout\,
	cin => \inst2|Add6~46\,
	sumout => \inst2|Add6~41_sumout\,
	cout => \inst2|Add6~42\);

-- Location: LABCELL_X85_Y32_N6
\inst2|Add6~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~37_sumout\ = SUM(( \inst2|TSUM[5][22]~combout\ ) + ( GND ) + ( \inst2|Add6~42\ ))
-- \inst2|Add6~38\ = CARRY(( \inst2|TSUM[5][22]~combout\ ) + ( GND ) + ( \inst2|Add6~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][22]~combout\,
	cin => \inst2|Add6~42\,
	sumout => \inst2|Add6~37_sumout\,
	cout => \inst2|Add6~38\);

-- Location: LABCELL_X85_Y32_N9
\inst2|Add6~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~33_sumout\ = SUM(( \inst2|TSUM[5][23]~combout\ ) + ( GND ) + ( \inst2|Add6~38\ ))
-- \inst2|Add6~34\ = CARRY(( \inst2|TSUM[5][23]~combout\ ) + ( GND ) + ( \inst2|Add6~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][23]~combout\,
	cin => \inst2|Add6~38\,
	sumout => \inst2|Add6~33_sumout\,
	cout => \inst2|Add6~34\);

-- Location: LABCELL_X85_Y32_N12
\inst2|Add6~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~29_sumout\ = SUM(( \inst2|TSUM[5][24]~combout\ ) + ( GND ) + ( \inst2|Add6~34\ ))
-- \inst2|Add6~30\ = CARRY(( \inst2|TSUM[5][24]~combout\ ) + ( GND ) + ( \inst2|Add6~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][24]~combout\,
	cin => \inst2|Add6~34\,
	sumout => \inst2|Add6~29_sumout\,
	cout => \inst2|Add6~30\);

-- Location: LABCELL_X85_Y32_N15
\inst2|Add6~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~25_sumout\ = SUM(( \inst2|TSUM[5][25]~combout\ ) + ( GND ) + ( \inst2|Add6~30\ ))
-- \inst2|Add6~26\ = CARRY(( \inst2|TSUM[5][25]~combout\ ) + ( GND ) + ( \inst2|Add6~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[5][25]~combout\,
	cin => \inst2|Add6~30\,
	sumout => \inst2|Add6~25_sumout\,
	cout => \inst2|Add6~26\);

-- Location: LABCELL_X85_Y32_N18
\inst2|Add6~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~21_sumout\ = SUM(( \inst2|TSUM[5][26]~combout\ ) + ( GND ) + ( \inst2|Add6~26\ ))
-- \inst2|Add6~22\ = CARRY(( \inst2|TSUM[5][26]~combout\ ) + ( GND ) + ( \inst2|Add6~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[5][26]~combout\,
	cin => \inst2|Add6~26\,
	sumout => \inst2|Add6~21_sumout\,
	cout => \inst2|Add6~22\);

-- Location: LABCELL_X85_Y32_N21
\inst2|Add6~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~17_sumout\ = SUM(( \inst2|TSUM[5][27]~combout\ ) + ( GND ) + ( \inst2|Add6~22\ ))
-- \inst2|Add6~18\ = CARRY(( \inst2|TSUM[5][27]~combout\ ) + ( GND ) + ( \inst2|Add6~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][27]~combout\,
	cin => \inst2|Add6~22\,
	sumout => \inst2|Add6~17_sumout\,
	cout => \inst2|Add6~18\);

-- Location: LABCELL_X85_Y32_N24
\inst2|Add6~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~13_sumout\ = SUM(( \inst2|TSUM[5][28]~combout\ ) + ( GND ) + ( \inst2|Add6~18\ ))
-- \inst2|Add6~14\ = CARRY(( \inst2|TSUM[5][28]~combout\ ) + ( GND ) + ( \inst2|Add6~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][28]~combout\,
	cin => \inst2|Add6~18\,
	sumout => \inst2|Add6~13_sumout\,
	cout => \inst2|Add6~14\);

-- Location: LABCELL_X85_Y32_N27
\inst2|Add6~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~9_sumout\ = SUM(( \inst2|TSUM[5][29]~combout\ ) + ( GND ) + ( \inst2|Add6~14\ ))
-- \inst2|Add6~10\ = CARRY(( \inst2|TSUM[5][29]~combout\ ) + ( GND ) + ( \inst2|Add6~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[5][29]~combout\,
	cin => \inst2|Add6~14\,
	sumout => \inst2|Add6~9_sumout\,
	cout => \inst2|Add6~10\);

-- Location: LABCELL_X85_Y32_N30
\inst2|Add6~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~5_sumout\ = SUM(( \inst2|TSUM[5][30]~combout\ ) + ( GND ) + ( \inst2|Add6~10\ ))
-- \inst2|Add6~6\ = CARRY(( \inst2|TSUM[5][30]~combout\ ) + ( GND ) + ( \inst2|Add6~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][30]~combout\,
	cin => \inst2|Add6~10\,
	sumout => \inst2|Add6~5_sumout\,
	cout => \inst2|Add6~6\);

-- Location: LABCELL_X85_Y32_N33
\inst2|Add6~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add6~1_sumout\ = SUM(( \inst2|TSUM[5][31]~combout\ ) + ( GND ) + ( \inst2|Add6~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[5][31]~combout\,
	cin => \inst2|Add6~6\,
	sumout => \inst2|Add6~1_sumout\);

-- Location: LABCELL_X85_Y32_N51
\inst2|TSUM[6][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][31]~combout\ = LCELL(( \inst2|Add6~1_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~1_sumout\,
	combout => \inst2|TSUM[6][31]~combout\);

-- Location: LABCELL_X85_Y32_N57
\inst2|TSUM[6][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][30]~combout\ = LCELL(( \inst2|Add6~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~5_sumout\,
	combout => \inst2|TSUM[6][30]~combout\);

-- Location: LABCELL_X88_Y36_N15
\inst2|TSUM[6][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][29]~combout\ = LCELL(( \inst2|Add6~9_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~9_sumout\,
	combout => \inst2|TSUM[6][29]~combout\);

-- Location: LABCELL_X85_Y32_N36
\inst2|TSUM[6][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][28]~combout\ = LCELL(( \inst2|Add6~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~13_sumout\,
	combout => \inst2|TSUM[6][28]~combout\);

-- Location: LABCELL_X83_Y36_N48
\inst2|TSUM[6][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][27]~combout\ = LCELL(( \inst2|Add6~17_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~17_sumout\,
	combout => \inst2|TSUM[6][27]~combout\);

-- Location: MLABCELL_X84_Y36_N57
\inst2|TSUM[6][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][26]~combout\ = LCELL(( \inst2|Add6~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~21_sumout\,
	combout => \inst2|TSUM[6][26]~combout\);

-- Location: LABCELL_X85_Y34_N6
\inst2|TSUM[6][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][25]~combout\ = LCELL(( \inst2|Add6~25_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~25_sumout\,
	combout => \inst2|TSUM[6][25]~combout\);

-- Location: MLABCELL_X84_Y32_N39
\inst2|TSUM[6][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][24]~combout\ = LCELL(( \inst2|Add6~29_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~29_sumout\,
	combout => \inst2|TSUM[6][24]~combout\);

-- Location: LABCELL_X85_Y32_N54
\inst2|TSUM[6][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][23]~combout\ = LCELL(( \inst2|Add6~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~33_sumout\,
	combout => \inst2|TSUM[6][23]~combout\);

-- Location: LABCELL_X85_Y32_N48
\inst2|TSUM[6][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][22]~combout\ = LCELL(( \inst2|Add6~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~37_sumout\,
	combout => \inst2|TSUM[6][22]~combout\);

-- Location: LABCELL_X85_Y32_N42
\inst2|TSUM[6][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][21]~combout\ = LCELL(( \inst2|Add6~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~41_sumout\,
	combout => \inst2|TSUM[6][21]~combout\);

-- Location: MLABCELL_X84_Y36_N48
\inst2|TSUM[6][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][20]~combout\ = LCELL(( \inst2|Add6~45_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~45_sumout\,
	combout => \inst2|TSUM[6][20]~combout\);

-- Location: LABCELL_X81_Y35_N57
\inst2|TSUM[6][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][19]~combout\ = LCELL(( \inst2|Add6~49_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~49_sumout\,
	combout => \inst2|TSUM[6][19]~combout\);

-- Location: LABCELL_X85_Y34_N12
\inst2|TSUM[6][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][18]~combout\ = LCELL(( \inst2|Add6~53_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~53_sumout\,
	combout => \inst2|TSUM[6][18]~combout\);

-- Location: LABCELL_X80_Y35_N12
\inst2|TSUM[6][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][17]~combout\ = LCELL(( \inst2|Add6~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~57_sumout\,
	combout => \inst2|TSUM[6][17]~combout\);

-- Location: LABCELL_X81_Y35_N27
\inst2|TSUM[6][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][16]~combout\ = LCELL(( \inst2|Add6~61_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~61_sumout\,
	combout => \inst2|TSUM[6][16]~combout\);

-- Location: LABCELL_X83_Y35_N36
\inst2|TSUM[6][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][15]~combout\ = LCELL(\inst2|Add6~65_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add6~65_sumout\,
	combout => \inst2|TSUM[6][15]~combout\);

-- Location: LABCELL_X85_Y37_N36
\inst2|TSUM[6][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][14]~combout\ = LCELL(( \inst2|Add6~69_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~69_sumout\,
	combout => \inst2|TSUM[6][14]~combout\);

-- Location: LABCELL_X85_Y37_N6
\inst2|TSUM[6][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][13]~combout\ = LCELL(( \inst2|Add6~73_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~73_sumout\,
	combout => \inst2|TSUM[6][13]~combout\);

-- Location: LABCELL_X85_Y37_N15
\inst2|TSUM[6][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][12]~combout\ = LCELL(( \inst2|Add6~77_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~77_sumout\,
	combout => \inst2|TSUM[6][12]~combout\);

-- Location: MLABCELL_X87_Y35_N54
\inst2|TSUM[6][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][11]~combout\ = LCELL(( \inst2|Add6~81_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~81_sumout\,
	combout => \inst2|TSUM[6][11]~combout\);

-- Location: LABCELL_X81_Y33_N15
\inst2|TSUM[6][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][10]~combout\ = LCELL(( \inst2|Add6~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~85_sumout\,
	combout => \inst2|TSUM[6][10]~combout\);

-- Location: LABCELL_X85_Y37_N54
\inst2|TSUM[6][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][9]~combout\ = LCELL(( \inst2|Add6~89_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~89_sumout\,
	combout => \inst2|TSUM[6][9]~combout\);

-- Location: LABCELL_X85_Y37_N0
\inst2|TSUM[6][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][8]~combout\ = LCELL(( \inst2|Add6~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~93_sumout\,
	combout => \inst2|TSUM[6][8]~combout\);

-- Location: LABCELL_X85_Y37_N33
\inst2|TSUM[6][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][7]~combout\ = LCELL(( \inst2|Add6~97_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~97_sumout\,
	combout => \inst2|TSUM[6][7]~combout\);

-- Location: LABCELL_X85_Y34_N33
\inst2|TSUM[6][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][6]~combout\ = LCELL(( \inst2|Add6~101_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~101_sumout\,
	combout => \inst2|TSUM[6][6]~combout\);

-- Location: MLABCELL_X82_Y35_N36
\inst2|PSUM[8][0]~_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[8][0]~_wirecell_combout\ = !\inst2|PSUM[8][0]~q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[8][0]~q\,
	combout => \inst2|PSUM[8][0]~_wirecell_combout\);

-- Location: MLABCELL_X82_Y35_N51
\inst2|PSUM[8][5]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|PSUM[8][5]~0_combout\ = ( \inst7|DIVD~q\ ) # ( !\inst7|DIVD~q\ & ( !\inst6|STEMP\(16) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|ALT_INV_STEMP\(16),
	dataf => \inst7|ALT_INV_DIVD~q\,
	combout => \inst2|PSUM[8][5]~0_combout\);

-- Location: FF_X82_Y35_N38
\inst2|PSUM[8][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|PSUM[8][0]~_wirecell_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[8][5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[8][0]~q\);

-- Location: MLABCELL_X82_Y35_N39
\inst2|Add9~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add9~4_combout\ = ( \inst2|PSUM[8][0]~q\ & ( !\inst2|PSUM[8][1]~q\ ) ) # ( !\inst2|PSUM[8][0]~q\ & ( \inst2|PSUM[8][1]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_PSUM[8][1]~q\,
	dataf => \inst2|ALT_INV_PSUM[8][0]~q\,
	combout => \inst2|Add9~4_combout\);

-- Location: FF_X82_Y35_N41
\inst2|PSUM[8][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add9~4_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[8][5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[8][1]~q\);

-- Location: MLABCELL_X82_Y35_N48
\inst2|Add9~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add9~3_combout\ = ( \inst2|PSUM[8][1]~q\ & ( !\inst2|PSUM[8][0]~q\ $ (!\inst2|PSUM[8][2]~q\) ) ) # ( !\inst2|PSUM[8][1]~q\ & ( \inst2|PSUM[8][2]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_PSUM[8][0]~q\,
	datad => \inst2|ALT_INV_PSUM[8][2]~q\,
	dataf => \inst2|ALT_INV_PSUM[8][1]~q\,
	combout => \inst2|Add9~3_combout\);

-- Location: FF_X82_Y35_N50
\inst2|PSUM[8][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add9~3_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[8][5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[8][2]~q\);

-- Location: MLABCELL_X82_Y35_N9
\inst2|Add9~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add9~2_combout\ = ( \inst2|PSUM[8][1]~q\ & ( !\inst2|PSUM[8][3]~q\ $ (((!\inst2|PSUM[8][0]~q\) # (!\inst2|PSUM[8][2]~q\))) ) ) # ( !\inst2|PSUM[8][1]~q\ & ( \inst2|PSUM[8][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101111110100000010111111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[8][0]~q\,
	datac => \inst2|ALT_INV_PSUM[8][2]~q\,
	datad => \inst2|ALT_INV_PSUM[8][3]~q\,
	dataf => \inst2|ALT_INV_PSUM[8][1]~q\,
	combout => \inst2|Add9~2_combout\);

-- Location: FF_X82_Y35_N11
\inst2|PSUM[8][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add9~2_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[8][5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[8][3]~q\);

-- Location: MLABCELL_X82_Y35_N6
\inst2|Add9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add9~1_combout\ = ( \inst2|PSUM[8][3]~q\ & ( !\inst2|PSUM[8][4]~q\ $ (((!\inst2|PSUM[8][0]~q\) # ((!\inst2|PSUM[8][1]~q\) # (!\inst2|PSUM[8][2]~q\)))) ) ) # ( !\inst2|PSUM[8][3]~q\ & ( \inst2|PSUM[8][4]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000001111111100000000111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[8][0]~q\,
	datab => \inst2|ALT_INV_PSUM[8][1]~q\,
	datac => \inst2|ALT_INV_PSUM[8][2]~q\,
	datad => \inst2|ALT_INV_PSUM[8][4]~q\,
	dataf => \inst2|ALT_INV_PSUM[8][3]~q\,
	combout => \inst2|Add9~1_combout\);

-- Location: FF_X82_Y35_N8
\inst2|PSUM[8][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add9~1_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[8][5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[8][4]~q\);

-- Location: MLABCELL_X82_Y35_N42
\inst2|Add9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add9~0_combout\ = ( \inst2|PSUM[8][5]~q\ & ( \inst2|PSUM[8][3]~q\ & ( (!\inst2|PSUM[8][0]~q\) # ((!\inst2|PSUM[8][1]~q\) # ((!\inst2|PSUM[8][2]~q\) # (!\inst2|PSUM[8][4]~q\))) ) ) ) # ( !\inst2|PSUM[8][5]~q\ & ( \inst2|PSUM[8][3]~q\ & ( 
-- (\inst2|PSUM[8][0]~q\ & (\inst2|PSUM[8][1]~q\ & (\inst2|PSUM[8][2]~q\ & \inst2|PSUM[8][4]~q\))) ) ) ) # ( \inst2|PSUM[8][5]~q\ & ( !\inst2|PSUM[8][3]~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000011111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[8][0]~q\,
	datab => \inst2|ALT_INV_PSUM[8][1]~q\,
	datac => \inst2|ALT_INV_PSUM[8][2]~q\,
	datad => \inst2|ALT_INV_PSUM[8][4]~q\,
	datae => \inst2|ALT_INV_PSUM[8][5]~q\,
	dataf => \inst2|ALT_INV_PSUM[8][3]~q\,
	combout => \inst2|Add9~0_combout\);

-- Location: FF_X82_Y35_N43
\inst2|PSUM[8][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~inputCLKENA0_outclk\,
	d => \inst2|Add9~0_combout\,
	sclr => \inst7|DIVD~q\,
	ena => \inst2|PSUM[8][5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2|PSUM[8][5]~q\);

-- Location: LABCELL_X85_Y37_N51
\inst2|TSUM[6][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][5]~combout\ = LCELL(( \inst2|Add6~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~105_sumout\,
	combout => \inst2|TSUM[6][5]~combout\);

-- Location: LABCELL_X85_Y37_N12
\inst2|TSUM[6][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][4]~combout\ = LCELL(\inst2|Add6~109_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add6~109_sumout\,
	combout => \inst2|TSUM[6][4]~combout\);

-- Location: LABCELL_X85_Y37_N18
\inst2|TSUM[6][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][3]~combout\ = LCELL(( \inst2|Add6~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~113_sumout\,
	combout => \inst2|TSUM[6][3]~combout\);

-- Location: MLABCELL_X84_Y33_N48
\inst2|TSUM[6][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][2]~combout\ = LCELL(\inst2|Add6~117_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add6~117_sumout\,
	combout => \inst2|TSUM[6][2]~combout\);

-- Location: LABCELL_X85_Y37_N27
\inst2|TSUM[6][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][1]~combout\ = LCELL(( \inst2|Add6~121_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add6~121_sumout\,
	combout => \inst2|TSUM[6][1]~combout\);

-- Location: LABCELL_X85_Y37_N42
\inst2|TSUM[6][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[6][0]~combout\ = LCELL(( \inst2|Add6~125_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add6~125_sumout\,
	combout => \inst2|TSUM[6][0]~combout\);

-- Location: MLABCELL_X84_Y37_N0
\inst2|Add7~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~125_sumout\ = SUM(( \inst2|PSUM[8][0]~q\ ) + ( \inst2|TSUM[6][0]~combout\ ) + ( !VCC ))
-- \inst2|Add7~126\ = CARRY(( \inst2|PSUM[8][0]~q\ ) + ( \inst2|TSUM[6][0]~combout\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][0]~combout\,
	datac => \inst2|ALT_INV_PSUM[8][0]~q\,
	cin => GND,
	sumout => \inst2|Add7~125_sumout\,
	cout => \inst2|Add7~126\);

-- Location: MLABCELL_X84_Y37_N3
\inst2|Add7~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~121_sumout\ = SUM(( \inst2|PSUM[8][1]~q\ ) + ( \inst2|TSUM[6][1]~combout\ ) + ( \inst2|Add7~126\ ))
-- \inst2|Add7~122\ = CARRY(( \inst2|PSUM[8][1]~q\ ) + ( \inst2|TSUM[6][1]~combout\ ) + ( \inst2|Add7~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][1]~combout\,
	datad => \inst2|ALT_INV_PSUM[8][1]~q\,
	cin => \inst2|Add7~126\,
	sumout => \inst2|Add7~121_sumout\,
	cout => \inst2|Add7~122\);

-- Location: MLABCELL_X84_Y37_N6
\inst2|Add7~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~117_sumout\ = SUM(( \inst2|PSUM[8][2]~q\ ) + ( \inst2|TSUM[6][2]~combout\ ) + ( \inst2|Add7~122\ ))
-- \inst2|Add7~118\ = CARRY(( \inst2|PSUM[8][2]~q\ ) + ( \inst2|TSUM[6][2]~combout\ ) + ( \inst2|Add7~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_PSUM[8][2]~q\,
	datac => \inst2|ALT_INV_TSUM[6][2]~combout\,
	cin => \inst2|Add7~122\,
	sumout => \inst2|Add7~117_sumout\,
	cout => \inst2|Add7~118\);

-- Location: MLABCELL_X84_Y37_N9
\inst2|Add7~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~113_sumout\ = SUM(( \inst2|PSUM[8][3]~q\ ) + ( \inst2|TSUM[6][3]~combout\ ) + ( \inst2|Add7~118\ ))
-- \inst2|Add7~114\ = CARRY(( \inst2|PSUM[8][3]~q\ ) + ( \inst2|TSUM[6][3]~combout\ ) + ( \inst2|Add7~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[8][3]~q\,
	dataf => \inst2|ALT_INV_TSUM[6][3]~combout\,
	cin => \inst2|Add7~118\,
	sumout => \inst2|Add7~113_sumout\,
	cout => \inst2|Add7~114\);

-- Location: MLABCELL_X84_Y37_N12
\inst2|Add7~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~109_sumout\ = SUM(( \inst2|PSUM[8][4]~q\ ) + ( \inst2|TSUM[6][4]~combout\ ) + ( \inst2|Add7~114\ ))
-- \inst2|Add7~110\ = CARRY(( \inst2|PSUM[8][4]~q\ ) + ( \inst2|TSUM[6][4]~combout\ ) + ( \inst2|Add7~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][4]~combout\,
	datac => \inst2|ALT_INV_PSUM[8][4]~q\,
	cin => \inst2|Add7~114\,
	sumout => \inst2|Add7~109_sumout\,
	cout => \inst2|Add7~110\);

-- Location: MLABCELL_X84_Y37_N15
\inst2|Add7~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~105_sumout\ = SUM(( \inst2|PSUM[8][5]~q\ ) + ( \inst2|TSUM[6][5]~combout\ ) + ( \inst2|Add7~110\ ))
-- \inst2|Add7~106\ = CARRY(( \inst2|PSUM[8][5]~q\ ) + ( \inst2|TSUM[6][5]~combout\ ) + ( \inst2|Add7~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_PSUM[8][5]~q\,
	dataf => \inst2|ALT_INV_TSUM[6][5]~combout\,
	cin => \inst2|Add7~110\,
	sumout => \inst2|Add7~105_sumout\,
	cout => \inst2|Add7~106\);

-- Location: MLABCELL_X84_Y37_N18
\inst2|Add7~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~101_sumout\ = SUM(( \inst2|TSUM[6][6]~combout\ ) + ( GND ) + ( \inst2|Add7~106\ ))
-- \inst2|Add7~102\ = CARRY(( \inst2|TSUM[6][6]~combout\ ) + ( GND ) + ( \inst2|Add7~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][6]~combout\,
	cin => \inst2|Add7~106\,
	sumout => \inst2|Add7~101_sumout\,
	cout => \inst2|Add7~102\);

-- Location: MLABCELL_X84_Y37_N21
\inst2|Add7~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~97_sumout\ = SUM(( \inst2|TSUM[6][7]~combout\ ) + ( GND ) + ( \inst2|Add7~102\ ))
-- \inst2|Add7~98\ = CARRY(( \inst2|TSUM[6][7]~combout\ ) + ( GND ) + ( \inst2|Add7~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][7]~combout\,
	cin => \inst2|Add7~102\,
	sumout => \inst2|Add7~97_sumout\,
	cout => \inst2|Add7~98\);

-- Location: MLABCELL_X84_Y37_N24
\inst2|Add7~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~93_sumout\ = SUM(( \inst2|TSUM[6][8]~combout\ ) + ( GND ) + ( \inst2|Add7~98\ ))
-- \inst2|Add7~94\ = CARRY(( \inst2|TSUM[6][8]~combout\ ) + ( GND ) + ( \inst2|Add7~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][8]~combout\,
	cin => \inst2|Add7~98\,
	sumout => \inst2|Add7~93_sumout\,
	cout => \inst2|Add7~94\);

-- Location: MLABCELL_X84_Y37_N27
\inst2|Add7~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~89_sumout\ = SUM(( \inst2|TSUM[6][9]~combout\ ) + ( GND ) + ( \inst2|Add7~94\ ))
-- \inst2|Add7~90\ = CARRY(( \inst2|TSUM[6][9]~combout\ ) + ( GND ) + ( \inst2|Add7~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][9]~combout\,
	cin => \inst2|Add7~94\,
	sumout => \inst2|Add7~89_sumout\,
	cout => \inst2|Add7~90\);

-- Location: MLABCELL_X84_Y37_N30
\inst2|Add7~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~85_sumout\ = SUM(( \inst2|TSUM[6][10]~combout\ ) + ( GND ) + ( \inst2|Add7~90\ ))
-- \inst2|Add7~86\ = CARRY(( \inst2|TSUM[6][10]~combout\ ) + ( GND ) + ( \inst2|Add7~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][10]~combout\,
	cin => \inst2|Add7~90\,
	sumout => \inst2|Add7~85_sumout\,
	cout => \inst2|Add7~86\);

-- Location: MLABCELL_X84_Y37_N33
\inst2|Add7~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~81_sumout\ = SUM(( \inst2|TSUM[6][11]~combout\ ) + ( GND ) + ( \inst2|Add7~86\ ))
-- \inst2|Add7~82\ = CARRY(( \inst2|TSUM[6][11]~combout\ ) + ( GND ) + ( \inst2|Add7~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][11]~combout\,
	cin => \inst2|Add7~86\,
	sumout => \inst2|Add7~81_sumout\,
	cout => \inst2|Add7~82\);

-- Location: MLABCELL_X84_Y37_N36
\inst2|Add7~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~77_sumout\ = SUM(( \inst2|TSUM[6][12]~combout\ ) + ( GND ) + ( \inst2|Add7~82\ ))
-- \inst2|Add7~78\ = CARRY(( \inst2|TSUM[6][12]~combout\ ) + ( GND ) + ( \inst2|Add7~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][12]~combout\,
	cin => \inst2|Add7~82\,
	sumout => \inst2|Add7~77_sumout\,
	cout => \inst2|Add7~78\);

-- Location: MLABCELL_X84_Y37_N39
\inst2|Add7~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~73_sumout\ = SUM(( \inst2|TSUM[6][13]~combout\ ) + ( GND ) + ( \inst2|Add7~78\ ))
-- \inst2|Add7~74\ = CARRY(( \inst2|TSUM[6][13]~combout\ ) + ( GND ) + ( \inst2|Add7~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][13]~combout\,
	cin => \inst2|Add7~78\,
	sumout => \inst2|Add7~73_sumout\,
	cout => \inst2|Add7~74\);

-- Location: MLABCELL_X84_Y37_N42
\inst2|Add7~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~69_sumout\ = SUM(( \inst2|TSUM[6][14]~combout\ ) + ( GND ) + ( \inst2|Add7~74\ ))
-- \inst2|Add7~70\ = CARRY(( \inst2|TSUM[6][14]~combout\ ) + ( GND ) + ( \inst2|Add7~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][14]~combout\,
	cin => \inst2|Add7~74\,
	sumout => \inst2|Add7~69_sumout\,
	cout => \inst2|Add7~70\);

-- Location: MLABCELL_X84_Y37_N45
\inst2|Add7~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~65_sumout\ = SUM(( \inst2|TSUM[6][15]~combout\ ) + ( GND ) + ( \inst2|Add7~70\ ))
-- \inst2|Add7~66\ = CARRY(( \inst2|TSUM[6][15]~combout\ ) + ( GND ) + ( \inst2|Add7~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][15]~combout\,
	cin => \inst2|Add7~70\,
	sumout => \inst2|Add7~65_sumout\,
	cout => \inst2|Add7~66\);

-- Location: MLABCELL_X84_Y37_N48
\inst2|Add7~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~61_sumout\ = SUM(( \inst2|TSUM[6][16]~combout\ ) + ( GND ) + ( \inst2|Add7~66\ ))
-- \inst2|Add7~62\ = CARRY(( \inst2|TSUM[6][16]~combout\ ) + ( GND ) + ( \inst2|Add7~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][16]~combout\,
	cin => \inst2|Add7~66\,
	sumout => \inst2|Add7~61_sumout\,
	cout => \inst2|Add7~62\);

-- Location: MLABCELL_X84_Y37_N51
\inst2|Add7~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~57_sumout\ = SUM(( \inst2|TSUM[6][17]~combout\ ) + ( GND ) + ( \inst2|Add7~62\ ))
-- \inst2|Add7~58\ = CARRY(( \inst2|TSUM[6][17]~combout\ ) + ( GND ) + ( \inst2|Add7~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][17]~combout\,
	cin => \inst2|Add7~62\,
	sumout => \inst2|Add7~57_sumout\,
	cout => \inst2|Add7~58\);

-- Location: MLABCELL_X84_Y37_N54
\inst2|Add7~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~53_sumout\ = SUM(( \inst2|TSUM[6][18]~combout\ ) + ( GND ) + ( \inst2|Add7~58\ ))
-- \inst2|Add7~54\ = CARRY(( \inst2|TSUM[6][18]~combout\ ) + ( GND ) + ( \inst2|Add7~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][18]~combout\,
	cin => \inst2|Add7~58\,
	sumout => \inst2|Add7~53_sumout\,
	cout => \inst2|Add7~54\);

-- Location: MLABCELL_X84_Y37_N57
\inst2|Add7~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~49_sumout\ = SUM(( \inst2|TSUM[6][19]~combout\ ) + ( GND ) + ( \inst2|Add7~54\ ))
-- \inst2|Add7~50\ = CARRY(( \inst2|TSUM[6][19]~combout\ ) + ( GND ) + ( \inst2|Add7~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][19]~combout\,
	cin => \inst2|Add7~54\,
	sumout => \inst2|Add7~49_sumout\,
	cout => \inst2|Add7~50\);

-- Location: MLABCELL_X84_Y36_N0
\inst2|Add7~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~45_sumout\ = SUM(( \inst2|TSUM[6][20]~combout\ ) + ( GND ) + ( \inst2|Add7~50\ ))
-- \inst2|Add7~46\ = CARRY(( \inst2|TSUM[6][20]~combout\ ) + ( GND ) + ( \inst2|Add7~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][20]~combout\,
	cin => \inst2|Add7~50\,
	sumout => \inst2|Add7~45_sumout\,
	cout => \inst2|Add7~46\);

-- Location: MLABCELL_X84_Y36_N3
\inst2|Add7~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~41_sumout\ = SUM(( \inst2|TSUM[6][21]~combout\ ) + ( GND ) + ( \inst2|Add7~46\ ))
-- \inst2|Add7~42\ = CARRY(( \inst2|TSUM[6][21]~combout\ ) + ( GND ) + ( \inst2|Add7~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][21]~combout\,
	cin => \inst2|Add7~46\,
	sumout => \inst2|Add7~41_sumout\,
	cout => \inst2|Add7~42\);

-- Location: MLABCELL_X84_Y36_N6
\inst2|Add7~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~37_sumout\ = SUM(( \inst2|TSUM[6][22]~combout\ ) + ( GND ) + ( \inst2|Add7~42\ ))
-- \inst2|Add7~38\ = CARRY(( \inst2|TSUM[6][22]~combout\ ) + ( GND ) + ( \inst2|Add7~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][22]~combout\,
	cin => \inst2|Add7~42\,
	sumout => \inst2|Add7~37_sumout\,
	cout => \inst2|Add7~38\);

-- Location: MLABCELL_X84_Y36_N9
\inst2|Add7~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~33_sumout\ = SUM(( \inst2|TSUM[6][23]~combout\ ) + ( GND ) + ( \inst2|Add7~38\ ))
-- \inst2|Add7~34\ = CARRY(( \inst2|TSUM[6][23]~combout\ ) + ( GND ) + ( \inst2|Add7~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][23]~combout\,
	cin => \inst2|Add7~38\,
	sumout => \inst2|Add7~33_sumout\,
	cout => \inst2|Add7~34\);

-- Location: MLABCELL_X84_Y36_N12
\inst2|Add7~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~29_sumout\ = SUM(( \inst2|TSUM[6][24]~combout\ ) + ( GND ) + ( \inst2|Add7~34\ ))
-- \inst2|Add7~30\ = CARRY(( \inst2|TSUM[6][24]~combout\ ) + ( GND ) + ( \inst2|Add7~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][24]~combout\,
	cin => \inst2|Add7~34\,
	sumout => \inst2|Add7~29_sumout\,
	cout => \inst2|Add7~30\);

-- Location: MLABCELL_X84_Y36_N15
\inst2|Add7~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~25_sumout\ = SUM(( \inst2|TSUM[6][25]~combout\ ) + ( GND ) + ( \inst2|Add7~30\ ))
-- \inst2|Add7~26\ = CARRY(( \inst2|TSUM[6][25]~combout\ ) + ( GND ) + ( \inst2|Add7~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][25]~combout\,
	cin => \inst2|Add7~30\,
	sumout => \inst2|Add7~25_sumout\,
	cout => \inst2|Add7~26\);

-- Location: MLABCELL_X84_Y36_N18
\inst2|Add7~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~21_sumout\ = SUM(( \inst2|TSUM[6][26]~combout\ ) + ( GND ) + ( \inst2|Add7~26\ ))
-- \inst2|Add7~22\ = CARRY(( \inst2|TSUM[6][26]~combout\ ) + ( GND ) + ( \inst2|Add7~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][26]~combout\,
	cin => \inst2|Add7~26\,
	sumout => \inst2|Add7~21_sumout\,
	cout => \inst2|Add7~22\);

-- Location: MLABCELL_X84_Y36_N21
\inst2|Add7~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~17_sumout\ = SUM(( \inst2|TSUM[6][27]~combout\ ) + ( GND ) + ( \inst2|Add7~22\ ))
-- \inst2|Add7~18\ = CARRY(( \inst2|TSUM[6][27]~combout\ ) + ( GND ) + ( \inst2|Add7~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][27]~combout\,
	cin => \inst2|Add7~22\,
	sumout => \inst2|Add7~17_sumout\,
	cout => \inst2|Add7~18\);

-- Location: MLABCELL_X84_Y36_N24
\inst2|Add7~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~13_sumout\ = SUM(( \inst2|TSUM[6][28]~combout\ ) + ( GND ) + ( \inst2|Add7~18\ ))
-- \inst2|Add7~14\ = CARRY(( \inst2|TSUM[6][28]~combout\ ) + ( GND ) + ( \inst2|Add7~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[6][28]~combout\,
	cin => \inst2|Add7~18\,
	sumout => \inst2|Add7~13_sumout\,
	cout => \inst2|Add7~14\);

-- Location: MLABCELL_X84_Y36_N27
\inst2|Add7~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~9_sumout\ = SUM(( \inst2|TSUM[6][29]~combout\ ) + ( GND ) + ( \inst2|Add7~14\ ))
-- \inst2|Add7~10\ = CARRY(( \inst2|TSUM[6][29]~combout\ ) + ( GND ) + ( \inst2|Add7~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[6][29]~combout\,
	cin => \inst2|Add7~14\,
	sumout => \inst2|Add7~9_sumout\,
	cout => \inst2|Add7~10\);

-- Location: MLABCELL_X84_Y36_N30
\inst2|Add7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~5_sumout\ = SUM(( \inst2|TSUM[6][30]~combout\ ) + ( GND ) + ( \inst2|Add7~10\ ))
-- \inst2|Add7~6\ = CARRY(( \inst2|TSUM[6][30]~combout\ ) + ( GND ) + ( \inst2|Add7~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][30]~combout\,
	cin => \inst2|Add7~10\,
	sumout => \inst2|Add7~5_sumout\,
	cout => \inst2|Add7~6\);

-- Location: MLABCELL_X84_Y36_N33
\inst2|Add7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add7~1_sumout\ = SUM(( \inst2|TSUM[6][31]~combout\ ) + ( GND ) + ( \inst2|Add7~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[6][31]~combout\,
	cin => \inst2|Add7~6\,
	sumout => \inst2|Add7~1_sumout\);

-- Location: LABCELL_X85_Y38_N54
\inst2|TSUM[7][31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][31]~combout\ = LCELL(\inst2|Add7~1_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add7~1_sumout\,
	combout => \inst2|TSUM[7][31]~combout\);

-- Location: LABCELL_X85_Y38_N15
\inst2|SUM[31]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(31) = LCELL(( \inst2|TSUM[7][31]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_TSUM[7][31]~combout\,
	combout => \inst2|SUM\(31));

-- Location: LABCELL_X83_Y38_N51
\inst2|TSUM[7][30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][30]~combout\ = LCELL(( \inst2|Add7~5_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~5_sumout\,
	combout => \inst2|TSUM[7][30]~combout\);

-- Location: LABCELL_X83_Y38_N48
\inst2|SUM[30]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(30) = LCELL(( \inst2|TSUM[7][30]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][30]~combout\,
	combout => \inst2|SUM\(30));

-- Location: LABCELL_X83_Y37_N57
\inst2|TSUM[7][29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][29]~combout\ = LCELL(\inst2|Add7~9_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add7~9_sumout\,
	combout => \inst2|TSUM[7][29]~combout\);

-- Location: LABCELL_X83_Y37_N54
\inst2|SUM[29]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(29) = LCELL(\inst2|TSUM[7][29]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[7][29]~combout\,
	combout => \inst2|SUM\(29));

-- Location: MLABCELL_X84_Y36_N39
\inst2|TSUM[7][28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][28]~combout\ = LCELL(( \inst2|Add7~13_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~13_sumout\,
	combout => \inst2|TSUM[7][28]~combout\);

-- Location: LABCELL_X85_Y38_N0
\inst2|SUM[28]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(28) = LCELL(( \inst2|TSUM[7][28]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][28]~combout\,
	combout => \inst2|SUM\(28));

-- Location: MLABCELL_X87_Y38_N45
\inst2|TSUM[7][27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][27]~combout\ = LCELL(\inst2|Add7~17_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add7~17_sumout\,
	combout => \inst2|TSUM[7][27]~combout\);

-- Location: MLABCELL_X87_Y38_N42
\inst2|SUM[27]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(27) = LCELL(\inst2|TSUM[7][27]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[7][27]~combout\,
	combout => \inst2|SUM\(27));

-- Location: MLABCELL_X84_Y34_N33
\inst2|TSUM[7][26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][26]~combout\ = LCELL(( \inst2|Add7~21_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~21_sumout\,
	combout => \inst2|TSUM[7][26]~combout\);

-- Location: MLABCELL_X84_Y34_N39
\inst2|SUM[26]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(26) = LCELL(( \inst2|TSUM[7][26]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][26]~combout\,
	combout => \inst2|SUM\(26));

-- Location: LABCELL_X83_Y37_N51
\inst2|TSUM[7][25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][25]~combout\ = LCELL(\inst2|Add7~25_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add7~25_sumout\,
	combout => \inst2|TSUM[7][25]~combout\);

-- Location: LABCELL_X83_Y37_N48
\inst2|SUM[25]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(25) = LCELL(( \inst2|TSUM[7][25]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][25]~combout\,
	combout => \inst2|SUM\(25));

-- Location: LABCELL_X85_Y34_N36
\inst2|TSUM[7][24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][24]~combout\ = LCELL(\inst2|Add7~29_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add7~29_sumout\,
	combout => \inst2|TSUM[7][24]~combout\);

-- Location: LABCELL_X85_Y34_N39
\inst2|SUM[24]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(24) = LCELL(\inst2|TSUM[7][24]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[7][24]~combout\,
	combout => \inst2|SUM\(24));

-- Location: LABCELL_X85_Y38_N39
\inst2|TSUM[7][23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][23]~combout\ = LCELL(( \inst2|Add7~33_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add7~33_sumout\,
	combout => \inst2|TSUM[7][23]~combout\);

-- Location: LABCELL_X85_Y38_N21
\inst2|SUM[23]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(23) = LCELL(( \inst2|TSUM[7][23]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][23]~combout\,
	combout => \inst2|SUM\(23));

-- Location: MLABCELL_X87_Y38_N9
\inst2|TSUM[7][22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][22]~combout\ = LCELL(( \inst2|Add7~37_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~37_sumout\,
	combout => \inst2|TSUM[7][22]~combout\);

-- Location: MLABCELL_X87_Y38_N24
\inst2|SUM[22]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(22) = LCELL(( \inst2|TSUM[7][22]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][22]~combout\,
	combout => \inst2|SUM\(22));

-- Location: LABCELL_X85_Y38_N33
\inst2|TSUM[7][21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][21]~combout\ = LCELL(( \inst2|Add7~41_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~41_sumout\,
	combout => \inst2|TSUM[7][21]~combout\);

-- Location: LABCELL_X85_Y38_N24
\inst2|SUM[21]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(21) = LCELL(( \inst2|TSUM[7][21]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][21]~combout\,
	combout => \inst2|SUM\(21));

-- Location: MLABCELL_X87_Y38_N39
\inst2|TSUM[7][20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][20]~combout\ = LCELL(\inst2|Add7~45_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add7~45_sumout\,
	combout => \inst2|TSUM[7][20]~combout\);

-- Location: MLABCELL_X87_Y38_N36
\inst2|SUM[20]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(20) = LCELL(( \inst2|TSUM[7][20]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][20]~combout\,
	combout => \inst2|SUM\(20));

-- Location: LABCELL_X83_Y37_N21
\inst2|TSUM[7][19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][19]~combout\ = LCELL(\inst2|Add7~49_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add7~49_sumout\,
	combout => \inst2|TSUM[7][19]~combout\);

-- Location: LABCELL_X83_Y37_N18
\inst2|SUM[19]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(19) = LCELL(\inst2|TSUM[7][19]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[7][19]~combout\,
	combout => \inst2|SUM\(19));

-- Location: LABCELL_X88_Y35_N45
\inst2|TSUM[7][18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][18]~combout\ = LCELL(\inst2|Add7~53_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add7~53_sumout\,
	combout => \inst2|TSUM[7][18]~combout\);

-- Location: LABCELL_X88_Y35_N42
\inst2|SUM[18]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(18) = LCELL(\inst2|TSUM[7][18]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[7][18]~combout\,
	combout => \inst2|SUM\(18));

-- Location: LABCELL_X83_Y39_N54
\inst2|TSUM[7][17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][17]~combout\ = LCELL(( \inst2|Add7~57_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~57_sumout\,
	combout => \inst2|TSUM[7][17]~combout\);

-- Location: LABCELL_X83_Y39_N21
\inst2|SUM[17]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(17) = LCELL(\inst2|TSUM[7][17]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[7][17]~combout\,
	combout => \inst2|SUM\(17));

-- Location: LABCELL_X83_Y39_N12
\inst2|TSUM[7][16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][16]~combout\ = LCELL(( \inst2|Add7~61_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~61_sumout\,
	combout => \inst2|TSUM[7][16]~combout\);

-- Location: LABCELL_X83_Y39_N0
\inst2|SUM[16]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(16) = LCELL(( \inst2|TSUM[7][16]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][16]~combout\,
	combout => \inst2|SUM\(16));

-- Location: LABCELL_X83_Y39_N24
\inst2|TSUM[7][15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][15]~combout\ = LCELL(\inst2|Add7~65_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_Add7~65_sumout\,
	combout => \inst2|TSUM[7][15]~combout\);

-- Location: LABCELL_X83_Y39_N45
\inst2|SUM[15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(15) = LCELL(( \inst2|TSUM[7][15]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][15]~combout\,
	combout => \inst2|SUM\(15));

-- Location: LABCELL_X83_Y37_N0
\inst2|TSUM[7][14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][14]~combout\ = LCELL(\inst2|Add7~69_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add7~69_sumout\,
	combout => \inst2|TSUM[7][14]~combout\);

-- Location: LABCELL_X83_Y39_N15
\inst2|SUM[14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(14) = LCELL(( \inst2|TSUM[7][14]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][14]~combout\,
	combout => \inst2|SUM\(14));

-- Location: LABCELL_X88_Y35_N3
\inst2|TSUM[7][13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][13]~combout\ = LCELL(( \inst2|Add7~73_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~73_sumout\,
	combout => \inst2|TSUM[7][13]~combout\);

-- Location: LABCELL_X88_Y35_N0
\inst2|SUM[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(13) = LCELL(\inst2|TSUM[7][13]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_TSUM[7][13]~combout\,
	combout => \inst2|SUM\(13));

-- Location: LABCELL_X83_Y37_N36
\inst2|TSUM[7][12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][12]~combout\ = LCELL(\inst2|Add7~77_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add7~77_sumout\,
	combout => \inst2|TSUM[7][12]~combout\);

-- Location: LABCELL_X83_Y37_N39
\inst2|SUM[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(12) = LCELL(\inst2|TSUM[7][12]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_TSUM[7][12]~combout\,
	combout => \inst2|SUM\(12));

-- Location: LABCELL_X85_Y38_N6
\inst2|TSUM[7][11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][11]~combout\ = LCELL(( \inst2|Add7~81_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~81_sumout\,
	combout => \inst2|TSUM[7][11]~combout\);

-- Location: LABCELL_X85_Y38_N51
\inst2|SUM[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(11) = LCELL(( \inst2|TSUM[7][11]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][11]~combout\,
	combout => \inst2|SUM\(11));

-- Location: LABCELL_X85_Y39_N51
\inst2|TSUM[7][10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][10]~combout\ = LCELL(( \inst2|Add7~85_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \inst2|ALT_INV_Add7~85_sumout\,
	combout => \inst2|TSUM[7][10]~combout\);

-- Location: LABCELL_X85_Y39_N42
\inst2|SUM[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(10) = LCELL(( \inst2|TSUM[7][10]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][10]~combout\,
	combout => \inst2|SUM\(10));

-- Location: LABCELL_X83_Y37_N42
\inst2|TSUM[7][9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][9]~combout\ = LCELL(( \inst2|Add7~89_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~89_sumout\,
	combout => \inst2|TSUM[7][9]~combout\);

-- Location: LABCELL_X83_Y37_N45
\inst2|SUM[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(9) = LCELL(( \inst2|TSUM[7][9]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][9]~combout\,
	combout => \inst2|SUM\(9));

-- Location: LABCELL_X83_Y37_N12
\inst2|TSUM[7][8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][8]~combout\ = LCELL(( \inst2|Add7~93_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~93_sumout\,
	combout => \inst2|TSUM[7][8]~combout\);

-- Location: LABCELL_X83_Y37_N15
\inst2|SUM[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(8) = LCELL(( \inst2|TSUM[7][8]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][8]~combout\,
	combout => \inst2|SUM\(8));

-- Location: LABCELL_X83_Y37_N6
\inst2|TSUM[7][7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][7]~combout\ = LCELL(\inst2|Add7~97_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add7~97_sumout\,
	combout => \inst2|TSUM[7][7]~combout\);

-- Location: LABCELL_X83_Y37_N9
\inst2|SUM[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(7) = LCELL(( \inst2|TSUM[7][7]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][7]~combout\,
	combout => \inst2|SUM\(7));

-- Location: LABCELL_X83_Y39_N6
\inst2|TSUM[7][6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][6]~combout\ = LCELL(\inst2|Add7~101_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_Add7~101_sumout\,
	combout => \inst2|TSUM[7][6]~combout\);

-- Location: LABCELL_X83_Y39_N9
\inst2|SUM[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(6) = LCELL(( \inst2|TSUM[7][6]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][6]~combout\,
	combout => \inst2|SUM\(6));

-- Location: LABCELL_X83_Y39_N48
\inst2|TSUM[7][5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][5]~combout\ = LCELL(( \inst2|Add7~105_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~105_sumout\,
	combout => \inst2|TSUM[7][5]~combout\);

-- Location: LABCELL_X83_Y39_N51
\inst2|SUM[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(5) = LCELL(( \inst2|TSUM[7][5]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][5]~combout\,
	combout => \inst2|SUM\(5));

-- Location: LABCELL_X85_Y38_N45
\inst2|TSUM[7][4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][4]~combout\ = LCELL(( \inst2|Add7~109_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~109_sumout\,
	combout => \inst2|TSUM[7][4]~combout\);

-- Location: LABCELL_X85_Y38_N42
\inst2|SUM[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(4) = LCELL(\inst2|TSUM[7][4]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_TSUM[7][4]~combout\,
	combout => \inst2|SUM\(4));

-- Location: LABCELL_X88_Y35_N54
\inst2|TSUM[7][3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][3]~combout\ = LCELL(( \inst2|Add7~113_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~113_sumout\,
	combout => \inst2|TSUM[7][3]~combout\);

-- Location: LABCELL_X88_Y35_N39
\inst2|SUM[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(3) = LCELL(\inst2|TSUM[7][3]~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst2|ALT_INV_TSUM[7][3]~combout\,
	combout => \inst2|SUM\(3));

-- Location: MLABCELL_X87_Y38_N33
\inst2|TSUM[7][2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][2]~combout\ = LCELL(( \inst2|Add7~117_sumout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_Add7~117_sumout\,
	combout => \inst2|TSUM[7][2]~combout\);

-- Location: MLABCELL_X87_Y38_N30
\inst2|SUM[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(2) = LCELL(( \inst2|TSUM[7][2]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][2]~combout\,
	combout => \inst2|SUM\(2));

-- Location: MLABCELL_X87_Y38_N48
\inst2|TSUM[7][1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][1]~combout\ = LCELL(\inst2|Add7~121_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add7~121_sumout\,
	combout => \inst2|TSUM[7][1]~combout\);

-- Location: MLABCELL_X87_Y38_N51
\inst2|SUM[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(1) = LCELL(( \inst2|TSUM[7][1]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][1]~combout\,
	combout => \inst2|SUM\(1));

-- Location: LABCELL_X88_Y35_N12
\inst2|TSUM[7][0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|TSUM[7][0]~combout\ = LCELL(\inst2|Add7~125_sumout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_Add7~125_sumout\,
	combout => \inst2|TSUM[7][0]~combout\);

-- Location: LABCELL_X88_Y35_N15
\inst2|SUM[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|SUM\(0) = LCELL(( \inst2|TSUM[7][0]~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \inst2|ALT_INV_TSUM[7][0]~combout\,
	combout => \inst2|SUM\(0));

-- Location: MLABCELL_X84_Y39_N0
\inst2|Add8~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~125_sumout\ = SUM(( !\inst2|SUM\(0) ) + ( VCC ) + ( !VCC ))
-- \inst2|Add8~126\ = CARRY(( !\inst2|SUM\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(0),
	cin => GND,
	sumout => \inst2|Add8~125_sumout\,
	cout => \inst2|Add8~126\);

-- Location: MLABCELL_X84_Y39_N3
\inst2|Add8~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~121_sumout\ = SUM(( !\inst2|SUM\(1) ) + ( GND ) + ( \inst2|Add8~126\ ))
-- \inst2|Add8~122\ = CARRY(( !\inst2|SUM\(1) ) + ( GND ) + ( \inst2|Add8~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(1),
	cin => \inst2|Add8~126\,
	sumout => \inst2|Add8~121_sumout\,
	cout => \inst2|Add8~122\);

-- Location: MLABCELL_X84_Y39_N6
\inst2|Add8~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~117_sumout\ = SUM(( !\inst2|SUM\(2) ) + ( GND ) + ( \inst2|Add8~122\ ))
-- \inst2|Add8~118\ = CARRY(( !\inst2|SUM\(2) ) + ( GND ) + ( \inst2|Add8~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(2),
	cin => \inst2|Add8~122\,
	sumout => \inst2|Add8~117_sumout\,
	cout => \inst2|Add8~118\);

-- Location: MLABCELL_X84_Y39_N9
\inst2|Add8~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~113_sumout\ = SUM(( !\inst2|SUM\(3) ) + ( GND ) + ( \inst2|Add8~118\ ))
-- \inst2|Add8~114\ = CARRY(( !\inst2|SUM\(3) ) + ( GND ) + ( \inst2|Add8~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(3),
	cin => \inst2|Add8~118\,
	sumout => \inst2|Add8~113_sumout\,
	cout => \inst2|Add8~114\);

-- Location: MLABCELL_X84_Y39_N12
\inst2|Add8~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~109_sumout\ = SUM(( !\inst2|SUM\(4) ) + ( GND ) + ( \inst2|Add8~114\ ))
-- \inst2|Add8~110\ = CARRY(( !\inst2|SUM\(4) ) + ( GND ) + ( \inst2|Add8~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(4),
	cin => \inst2|Add8~114\,
	sumout => \inst2|Add8~109_sumout\,
	cout => \inst2|Add8~110\);

-- Location: MLABCELL_X84_Y39_N15
\inst2|Add8~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~105_sumout\ = SUM(( !\inst2|SUM\(5) ) + ( GND ) + ( \inst2|Add8~110\ ))
-- \inst2|Add8~106\ = CARRY(( !\inst2|SUM\(5) ) + ( GND ) + ( \inst2|Add8~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(5),
	cin => \inst2|Add8~110\,
	sumout => \inst2|Add8~105_sumout\,
	cout => \inst2|Add8~106\);

-- Location: MLABCELL_X84_Y39_N18
\inst2|Add8~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~101_sumout\ = SUM(( !\inst2|SUM\(6) ) + ( GND ) + ( \inst2|Add8~106\ ))
-- \inst2|Add8~102\ = CARRY(( !\inst2|SUM\(6) ) + ( GND ) + ( \inst2|Add8~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(6),
	cin => \inst2|Add8~106\,
	sumout => \inst2|Add8~101_sumout\,
	cout => \inst2|Add8~102\);

-- Location: MLABCELL_X84_Y39_N21
\inst2|Add8~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~97_sumout\ = SUM(( !\inst2|SUM\(7) ) + ( GND ) + ( \inst2|Add8~102\ ))
-- \inst2|Add8~98\ = CARRY(( !\inst2|SUM\(7) ) + ( GND ) + ( \inst2|Add8~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(7),
	cin => \inst2|Add8~102\,
	sumout => \inst2|Add8~97_sumout\,
	cout => \inst2|Add8~98\);

-- Location: MLABCELL_X84_Y39_N24
\inst2|Add8~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~93_sumout\ = SUM(( !\inst2|SUM\(8) ) + ( GND ) + ( \inst2|Add8~98\ ))
-- \inst2|Add8~94\ = CARRY(( !\inst2|SUM\(8) ) + ( GND ) + ( \inst2|Add8~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(8),
	cin => \inst2|Add8~98\,
	sumout => \inst2|Add8~93_sumout\,
	cout => \inst2|Add8~94\);

-- Location: MLABCELL_X84_Y39_N27
\inst2|Add8~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~89_sumout\ = SUM(( !\inst2|SUM\(9) ) + ( GND ) + ( \inst2|Add8~94\ ))
-- \inst2|Add8~90\ = CARRY(( !\inst2|SUM\(9) ) + ( GND ) + ( \inst2|Add8~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(9),
	cin => \inst2|Add8~94\,
	sumout => \inst2|Add8~89_sumout\,
	cout => \inst2|Add8~90\);

-- Location: MLABCELL_X84_Y39_N30
\inst2|Add8~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~85_sumout\ = SUM(( !\inst2|SUM\(10) ) + ( GND ) + ( \inst2|Add8~90\ ))
-- \inst2|Add8~86\ = CARRY(( !\inst2|SUM\(10) ) + ( GND ) + ( \inst2|Add8~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(10),
	cin => \inst2|Add8~90\,
	sumout => \inst2|Add8~85_sumout\,
	cout => \inst2|Add8~86\);

-- Location: MLABCELL_X84_Y39_N33
\inst2|Add8~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~81_sumout\ = SUM(( !\inst2|SUM\(11) ) + ( GND ) + ( \inst2|Add8~86\ ))
-- \inst2|Add8~82\ = CARRY(( !\inst2|SUM\(11) ) + ( GND ) + ( \inst2|Add8~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(11),
	cin => \inst2|Add8~86\,
	sumout => \inst2|Add8~81_sumout\,
	cout => \inst2|Add8~82\);

-- Location: MLABCELL_X84_Y39_N36
\inst2|Add8~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~77_sumout\ = SUM(( !\inst2|SUM\(12) ) + ( GND ) + ( \inst2|Add8~82\ ))
-- \inst2|Add8~78\ = CARRY(( !\inst2|SUM\(12) ) + ( GND ) + ( \inst2|Add8~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(12),
	cin => \inst2|Add8~82\,
	sumout => \inst2|Add8~77_sumout\,
	cout => \inst2|Add8~78\);

-- Location: MLABCELL_X84_Y39_N39
\inst2|Add8~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~73_sumout\ = SUM(( !\inst2|SUM\(13) ) + ( GND ) + ( \inst2|Add8~78\ ))
-- \inst2|Add8~74\ = CARRY(( !\inst2|SUM\(13) ) + ( GND ) + ( \inst2|Add8~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(13),
	cin => \inst2|Add8~78\,
	sumout => \inst2|Add8~73_sumout\,
	cout => \inst2|Add8~74\);

-- Location: MLABCELL_X84_Y39_N42
\inst2|Add8~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~69_sumout\ = SUM(( !\inst2|SUM\(14) ) + ( GND ) + ( \inst2|Add8~74\ ))
-- \inst2|Add8~70\ = CARRY(( !\inst2|SUM\(14) ) + ( GND ) + ( \inst2|Add8~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(14),
	cin => \inst2|Add8~74\,
	sumout => \inst2|Add8~69_sumout\,
	cout => \inst2|Add8~70\);

-- Location: MLABCELL_X84_Y39_N45
\inst2|Add8~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~65_sumout\ = SUM(( !\inst2|SUM\(15) ) + ( GND ) + ( \inst2|Add8~70\ ))
-- \inst2|Add8~66\ = CARRY(( !\inst2|SUM\(15) ) + ( GND ) + ( \inst2|Add8~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(15),
	cin => \inst2|Add8~70\,
	sumout => \inst2|Add8~65_sumout\,
	cout => \inst2|Add8~66\);

-- Location: MLABCELL_X84_Y39_N48
\inst2|Add8~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~61_sumout\ = SUM(( !\inst2|SUM\(16) ) + ( GND ) + ( \inst2|Add8~66\ ))
-- \inst2|Add8~62\ = CARRY(( !\inst2|SUM\(16) ) + ( GND ) + ( \inst2|Add8~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(16),
	cin => \inst2|Add8~66\,
	sumout => \inst2|Add8~61_sumout\,
	cout => \inst2|Add8~62\);

-- Location: MLABCELL_X84_Y39_N51
\inst2|Add8~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~57_sumout\ = SUM(( !\inst2|SUM\(17) ) + ( GND ) + ( \inst2|Add8~62\ ))
-- \inst2|Add8~58\ = CARRY(( !\inst2|SUM\(17) ) + ( GND ) + ( \inst2|Add8~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(17),
	cin => \inst2|Add8~62\,
	sumout => \inst2|Add8~57_sumout\,
	cout => \inst2|Add8~58\);

-- Location: MLABCELL_X84_Y39_N54
\inst2|Add8~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~53_sumout\ = SUM(( !\inst2|SUM\(18) ) + ( GND ) + ( \inst2|Add8~58\ ))
-- \inst2|Add8~54\ = CARRY(( !\inst2|SUM\(18) ) + ( GND ) + ( \inst2|Add8~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(18),
	cin => \inst2|Add8~58\,
	sumout => \inst2|Add8~53_sumout\,
	cout => \inst2|Add8~54\);

-- Location: MLABCELL_X84_Y39_N57
\inst2|Add8~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~49_sumout\ = SUM(( !\inst2|SUM\(19) ) + ( GND ) + ( \inst2|Add8~54\ ))
-- \inst2|Add8~50\ = CARRY(( !\inst2|SUM\(19) ) + ( GND ) + ( \inst2|Add8~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(19),
	cin => \inst2|Add8~54\,
	sumout => \inst2|Add8~49_sumout\,
	cout => \inst2|Add8~50\);

-- Location: MLABCELL_X84_Y38_N0
\inst2|Add8~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~45_sumout\ = SUM(( !\inst2|SUM\(20) ) + ( GND ) + ( \inst2|Add8~50\ ))
-- \inst2|Add8~46\ = CARRY(( !\inst2|SUM\(20) ) + ( GND ) + ( \inst2|Add8~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(20),
	cin => \inst2|Add8~50\,
	sumout => \inst2|Add8~45_sumout\,
	cout => \inst2|Add8~46\);

-- Location: MLABCELL_X84_Y38_N3
\inst2|Add8~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~41_sumout\ = SUM(( !\inst2|SUM\(21) ) + ( GND ) + ( \inst2|Add8~46\ ))
-- \inst2|Add8~42\ = CARRY(( !\inst2|SUM\(21) ) + ( GND ) + ( \inst2|Add8~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(21),
	cin => \inst2|Add8~46\,
	sumout => \inst2|Add8~41_sumout\,
	cout => \inst2|Add8~42\);

-- Location: MLABCELL_X84_Y38_N6
\inst2|Add8~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~37_sumout\ = SUM(( !\inst2|SUM\(22) ) + ( GND ) + ( \inst2|Add8~42\ ))
-- \inst2|Add8~38\ = CARRY(( !\inst2|SUM\(22) ) + ( GND ) + ( \inst2|Add8~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(22),
	cin => \inst2|Add8~42\,
	sumout => \inst2|Add8~37_sumout\,
	cout => \inst2|Add8~38\);

-- Location: MLABCELL_X84_Y38_N9
\inst2|Add8~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~33_sumout\ = SUM(( !\inst2|SUM\(23) ) + ( GND ) + ( \inst2|Add8~38\ ))
-- \inst2|Add8~34\ = CARRY(( !\inst2|SUM\(23) ) + ( GND ) + ( \inst2|Add8~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(23),
	cin => \inst2|Add8~38\,
	sumout => \inst2|Add8~33_sumout\,
	cout => \inst2|Add8~34\);

-- Location: MLABCELL_X84_Y38_N12
\inst2|Add8~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~29_sumout\ = SUM(( !\inst2|SUM\(24) ) + ( GND ) + ( \inst2|Add8~34\ ))
-- \inst2|Add8~30\ = CARRY(( !\inst2|SUM\(24) ) + ( GND ) + ( \inst2|Add8~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(24),
	cin => \inst2|Add8~34\,
	sumout => \inst2|Add8~29_sumout\,
	cout => \inst2|Add8~30\);

-- Location: MLABCELL_X84_Y38_N15
\inst2|Add8~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~25_sumout\ = SUM(( !\inst2|SUM\(25) ) + ( GND ) + ( \inst2|Add8~30\ ))
-- \inst2|Add8~26\ = CARRY(( !\inst2|SUM\(25) ) + ( GND ) + ( \inst2|Add8~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(25),
	cin => \inst2|Add8~30\,
	sumout => \inst2|Add8~25_sumout\,
	cout => \inst2|Add8~26\);

-- Location: MLABCELL_X84_Y38_N18
\inst2|Add8~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~21_sumout\ = SUM(( !\inst2|SUM\(26) ) + ( GND ) + ( \inst2|Add8~26\ ))
-- \inst2|Add8~22\ = CARRY(( !\inst2|SUM\(26) ) + ( GND ) + ( \inst2|Add8~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(26),
	cin => \inst2|Add8~26\,
	sumout => \inst2|Add8~21_sumout\,
	cout => \inst2|Add8~22\);

-- Location: MLABCELL_X84_Y38_N21
\inst2|Add8~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~17_sumout\ = SUM(( !\inst2|SUM\(27) ) + ( GND ) + ( \inst2|Add8~22\ ))
-- \inst2|Add8~18\ = CARRY(( !\inst2|SUM\(27) ) + ( GND ) + ( \inst2|Add8~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(27),
	cin => \inst2|Add8~22\,
	sumout => \inst2|Add8~17_sumout\,
	cout => \inst2|Add8~18\);

-- Location: MLABCELL_X84_Y38_N24
\inst2|Add8~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~13_sumout\ = SUM(( !\inst2|SUM\(28) ) + ( GND ) + ( \inst2|Add8~18\ ))
-- \inst2|Add8~14\ = CARRY(( !\inst2|SUM\(28) ) + ( GND ) + ( \inst2|Add8~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(28),
	cin => \inst2|Add8~18\,
	sumout => \inst2|Add8~13_sumout\,
	cout => \inst2|Add8~14\);

-- Location: MLABCELL_X84_Y38_N27
\inst2|Add8~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~9_sumout\ = SUM(( !\inst2|SUM\(29) ) + ( GND ) + ( \inst2|Add8~14\ ))
-- \inst2|Add8~10\ = CARRY(( !\inst2|SUM\(29) ) + ( GND ) + ( \inst2|Add8~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(29),
	cin => \inst2|Add8~14\,
	sumout => \inst2|Add8~9_sumout\,
	cout => \inst2|Add8~10\);

-- Location: MLABCELL_X84_Y38_N30
\inst2|Add8~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~5_sumout\ = SUM(( !\inst2|SUM\(30) ) + ( GND ) + ( \inst2|Add8~10\ ))
-- \inst2|Add8~6\ = CARRY(( !\inst2|SUM\(30) ) + ( GND ) + ( \inst2|Add8~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(30),
	cin => \inst2|Add8~10\,
	sumout => \inst2|Add8~5_sumout\,
	cout => \inst2|Add8~6\);

-- Location: MLABCELL_X84_Y38_N33
\inst2|Add8~130\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~130_cout\ = CARRY(( !\inst2|SUM\(31) ) + ( GND ) + ( \inst2|Add8~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(31),
	cin => \inst2|Add8~6\,
	cout => \inst2|Add8~130_cout\);

-- Location: MLABCELL_X84_Y38_N36
\inst2|Add8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|Add8~1_sumout\ = SUM(( !\inst2|SUM\(31) ) + ( GND ) + ( \inst2|Add8~130_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(31),
	cin => \inst2|Add8~130_cout\,
	sumout => \inst2|Add8~1_sumout\);

-- Location: MLABCELL_X84_Y38_N54
\inst2|ADD[31]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[31]~0_combout\ = (!\inst21|TMPNEG1~q\ & ((!\inst21|TMPNEG0~q\ & (\inst2|SUM\(31))) # (\inst21|TMPNEG0~q\ & ((\inst2|Add8~1_sumout\))))) # (\inst21|TMPNEG1~q\ & (((\inst2|Add8~1_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100001111111000010000111111100001000011111110000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_SUM\(31),
	datad => \inst2|ALT_INV_Add8~1_sumout\,
	combout => \inst2|ADD[31]~0_combout\);

-- Location: MLABCELL_X84_Y38_N48
\inst2|ADD[30]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[30]~1_combout\ = ( \inst2|Add8~5_sumout\ & ( ((\inst2|SUM\(30)) # (\inst21|TMPNEG1~q\)) # (\inst21|TMPNEG0~q\) ) ) # ( !\inst2|Add8~5_sumout\ & ( (!\inst21|TMPNEG0~q\ & (!\inst21|TMPNEG1~q\ & \inst2|SUM\(30))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000000111111111111110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst21|ALT_INV_TMPNEG1~q\,
	datad => \inst2|ALT_INV_SUM\(30),
	dataf => \inst2|ALT_INV_Add8~5_sumout\,
	combout => \inst2|ADD[30]~1_combout\);

-- Location: MLABCELL_X84_Y38_N57
\inst2|ADD[29]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[29]~2_combout\ = (!\inst21|TMPNEG1~q\ & ((!\inst21|TMPNEG0~q\ & (\inst2|SUM\(29))) # (\inst21|TMPNEG0~q\ & ((\inst2|Add8~9_sumout\))))) # (\inst21|TMPNEG1~q\ & (((\inst2|Add8~9_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100001111111000010000111111100001000011111110000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_SUM\(29),
	datad => \inst2|ALT_INV_Add8~9_sumout\,
	combout => \inst2|ADD[29]~2_combout\);

-- Location: LABCELL_X85_Y38_N3
\inst2|ADD[28]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[28]~3_combout\ = (!\inst21|TMPNEG0~q\ & ((!\inst21|TMPNEG1~q\ & (\inst2|SUM\(28))) # (\inst21|TMPNEG1~q\ & ((\inst2|Add8~13_sumout\))))) # (\inst21|TMPNEG0~q\ & (((\inst2|Add8~13_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011100001111010001110000111101000111000011110100011100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(28),
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_Add8~13_sumout\,
	datad => \inst21|ALT_INV_TMPNEG1~q\,
	combout => \inst2|ADD[28]~3_combout\);

-- Location: MLABCELL_X84_Y38_N51
\inst2|ADD[27]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[27]~4_combout\ = (!\inst21|TMPNEG1~q\ & ((!\inst21|TMPNEG0~q\ & (\inst2|SUM\(27))) # (\inst21|TMPNEG0~q\ & ((\inst2|Add8~17_sumout\))))) # (\inst21|TMPNEG1~q\ & (((\inst2|Add8~17_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100001111111000010000111111100001000011111110000100001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_SUM\(27),
	datad => \inst2|ALT_INV_Add8~17_sumout\,
	combout => \inst2|ADD[27]~4_combout\);

-- Location: MLABCELL_X84_Y38_N45
\inst2|ADD[26]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[26]~5_combout\ = ( \inst2|Add8~21_sumout\ & ( ((\inst2|SUM\(26)) # (\inst21|TMPNEG0~q\)) # (\inst21|TMPNEG1~q\) ) ) # ( !\inst2|Add8~21_sumout\ & ( (!\inst21|TMPNEG1~q\ & (!\inst21|TMPNEG0~q\ & \inst2|SUM\(26))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_SUM\(26),
	dataf => \inst2|ALT_INV_Add8~21_sumout\,
	combout => \inst2|ADD[26]~5_combout\);

-- Location: LABCELL_X83_Y39_N39
\inst2|ADD[25]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[25]~6_combout\ = ( \inst21|TMPNEG1~q\ & ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~25_sumout\ ) ) ) # ( !\inst21|TMPNEG1~q\ & ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~25_sumout\ ) ) ) # ( \inst21|TMPNEG1~q\ & ( !\inst21|TMPNEG0~q\ & ( 
-- \inst2|Add8~25_sumout\ ) ) ) # ( !\inst21|TMPNEG1~q\ & ( !\inst21|TMPNEG0~q\ & ( \inst2|SUM\(25) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000001111111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst2|ALT_INV_SUM\(25),
	datad => \inst2|ALT_INV_Add8~25_sumout\,
	datae => \inst21|ALT_INV_TMPNEG1~q\,
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[25]~6_combout\);

-- Location: MLABCELL_X84_Y38_N42
\inst2|ADD[24]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[24]~7_combout\ = ( \inst2|Add8~29_sumout\ & ( ((\inst2|SUM\(24)) # (\inst21|TMPNEG0~q\)) # (\inst21|TMPNEG1~q\) ) ) # ( !\inst2|Add8~29_sumout\ & ( (!\inst21|TMPNEG1~q\ & (!\inst21|TMPNEG0~q\ & \inst2|SUM\(24))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010001000000000001000100001110111111111110111011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datad => \inst2|ALT_INV_SUM\(24),
	dataf => \inst2|ALT_INV_Add8~29_sumout\,
	combout => \inst2|ADD[24]~7_combout\);

-- Location: LABCELL_X85_Y38_N18
\inst2|ADD[23]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[23]~8_combout\ = (!\inst21|TMPNEG1~q\ & ((!\inst21|TMPNEG0~q\ & ((\inst2|SUM\(23)))) # (\inst21|TMPNEG0~q\ & (\inst2|Add8~33_sumout\)))) # (\inst21|TMPNEG1~q\ & (\inst2|Add8~33_sumout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001101100110011000110110011001100011011001100110001101100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst2|ALT_INV_Add8~33_sumout\,
	datac => \inst2|ALT_INV_SUM\(23),
	datad => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[23]~8_combout\);

-- Location: MLABCELL_X87_Y38_N27
\inst2|ADD[22]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[22]~9_combout\ = ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~37_sumout\ ) ) # ( !\inst21|TMPNEG0~q\ & ( (!\inst21|TMPNEG1~q\ & (\inst2|SUM\(22))) # (\inst21|TMPNEG1~q\ & ((\inst2|Add8~37_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011101000111010001110100011100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(22),
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_Add8~37_sumout\,
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[22]~9_combout\);

-- Location: LABCELL_X85_Y38_N27
\inst2|ADD[21]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[21]~10_combout\ = (!\inst21|TMPNEG0~q\ & ((!\inst21|TMPNEG1~q\ & (\inst2|SUM\(21))) # (\inst21|TMPNEG1~q\ & ((\inst2|Add8~41_sumout\))))) # (\inst21|TMPNEG0~q\ & (((\inst2|Add8~41_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011100001111010001110000111101000111000011110100011100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(21),
	datab => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_Add8~41_sumout\,
	datad => \inst21|ALT_INV_TMPNEG1~q\,
	combout => \inst2|ADD[21]~10_combout\);

-- Location: LABCELL_X85_Y38_N9
\inst2|ADD[20]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[20]~11_combout\ = ( \inst2|Add8~45_sumout\ & ( ((\inst21|TMPNEG0~q\) # (\inst2|SUM\(20))) # (\inst21|TMPNEG1~q\) ) ) # ( !\inst2|Add8~45_sumout\ & ( (!\inst21|TMPNEG1~q\ & (\inst2|SUM\(20) & !\inst21|TMPNEG0~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst2|ALT_INV_SUM\(20),
	datac => \inst21|ALT_INV_TMPNEG0~q\,
	dataf => \inst2|ALT_INV_Add8~45_sumout\,
	combout => \inst2|ADD[20]~11_combout\);

-- Location: LABCELL_X83_Y37_N3
\inst2|ADD[19]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[19]~12_combout\ = ( \inst21|TMPNEG1~q\ & ( \inst2|Add8~49_sumout\ ) ) # ( !\inst21|TMPNEG1~q\ & ( (!\inst21|TMPNEG0~q\ & (\inst2|SUM\(19))) # (\inst21|TMPNEG0~q\ & ((\inst2|Add8~49_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101001011111000010100101111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datac => \inst2|ALT_INV_SUM\(19),
	datad => \inst2|ALT_INV_Add8~49_sumout\,
	dataf => \inst21|ALT_INV_TMPNEG1~q\,
	combout => \inst2|ADD[19]~12_combout\);

-- Location: LABCELL_X88_Y35_N21
\inst2|ADD[18]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[18]~13_combout\ = ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~53_sumout\ ) ) # ( !\inst21|TMPNEG0~q\ & ( (!\inst21|TMPNEG1~q\ & (\inst2|SUM\(18))) # (\inst21|TMPNEG1~q\ & ((\inst2|Add8~53_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010011100100111001001110010011100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst2|ALT_INV_SUM\(18),
	datac => \inst2|ALT_INV_Add8~53_sumout\,
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[18]~13_combout\);

-- Location: LABCELL_X83_Y39_N18
\inst2|ADD[17]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[17]~14_combout\ = ( \inst2|Add8~57_sumout\ & ( ((\inst2|SUM\(17)) # (\inst21|TMPNEG0~q\)) # (\inst21|TMPNEG1~q\) ) ) # ( !\inst2|Add8~57_sumout\ & ( (!\inst21|TMPNEG1~q\ & (!\inst21|TMPNEG0~q\ & \inst2|SUM\(17))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000000111111111111110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst21|ALT_INV_TMPNEG0~q\,
	datad => \inst2|ALT_INV_SUM\(17),
	dataf => \inst2|ALT_INV_Add8~57_sumout\,
	combout => \inst2|ADD[17]~14_combout\);

-- Location: LABCELL_X83_Y39_N3
\inst2|ADD[16]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[16]~15_combout\ = ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~61_sumout\ ) ) # ( !\inst21|TMPNEG0~q\ & ( (!\inst21|TMPNEG1~q\ & (\inst2|SUM\(16))) # (\inst21|TMPNEG1~q\ & ((\inst2|Add8~61_sumout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011101000111010001110100011100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(16),
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_Add8~61_sumout\,
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[16]~15_combout\);

-- Location: LABCELL_X83_Y39_N42
\inst2|ADD[15]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[15]~16_combout\ = ( \inst2|Add8~65_sumout\ & ( ((\inst21|TMPNEG1~q\) # (\inst21|TMPNEG0~q\)) # (\inst2|SUM\(15)) ) ) # ( !\inst2|Add8~65_sumout\ & ( (\inst2|SUM\(15) & (!\inst21|TMPNEG0~q\ & !\inst21|TMPNEG1~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000111111111111110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(15),
	datac => \inst21|ALT_INV_TMPNEG0~q\,
	datad => \inst21|ALT_INV_TMPNEG1~q\,
	dataf => \inst2|ALT_INV_Add8~65_sumout\,
	combout => \inst2|ADD[15]~16_combout\);

-- Location: LABCELL_X83_Y39_N27
\inst2|ADD[14]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[14]~17_combout\ = (!\inst21|TMPNEG0~q\ & ((!\inst21|TMPNEG1~q\ & (\inst2|SUM\(14))) # (\inst21|TMPNEG1~q\ & ((\inst2|Add8~69_sumout\))))) # (\inst21|TMPNEG0~q\ & (((\inst2|Add8~69_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000001111111001000000111111100100000011111110010000001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst2|ALT_INV_SUM\(14),
	datac => \inst21|ALT_INV_TMPNEG1~q\,
	datad => \inst2|ALT_INV_Add8~69_sumout\,
	combout => \inst2|ADD[14]~17_combout\);

-- Location: LABCELL_X88_Y35_N57
\inst2|ADD[13]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[13]~18_combout\ = ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~73_sumout\ ) ) # ( !\inst21|TMPNEG0~q\ & ( (!\inst21|TMPNEG1~q\ & ((\inst2|SUM\(13)))) # (\inst21|TMPNEG1~q\ & (\inst2|Add8~73_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111000001011010111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_Add8~73_sumout\,
	datad => \inst2|ALT_INV_SUM\(13),
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[13]~18_combout\);

-- Location: LABCELL_X83_Y37_N30
\inst2|ADD[12]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[12]~19_combout\ = ( \inst2|SUM\(12) & ( ((!\inst21|TMPNEG0~q\ & !\inst21|TMPNEG1~q\)) # (\inst2|Add8~77_sumout\) ) ) # ( !\inst2|SUM\(12) & ( (\inst2|Add8~77_sumout\ & ((\inst21|TMPNEG1~q\) # (\inst21|TMPNEG0~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011100000111000001110000011110001111100011111000111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_Add8~77_sumout\,
	dataf => \inst2|ALT_INV_SUM\(12),
	combout => \inst2|ADD[12]~19_combout\);

-- Location: LABCELL_X85_Y38_N48
\inst2|ADD[11]~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[11]~20_combout\ = (!\inst21|TMPNEG1~q\ & ((!\inst21|TMPNEG0~q\ & (\inst2|SUM\(11))) # (\inst21|TMPNEG0~q\ & ((\inst2|Add8~81_sumout\))))) # (\inst21|TMPNEG1~q\ & (((\inst2|Add8~81_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101001100110011010100110011001101010011001100110101001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_SUM\(11),
	datab => \inst2|ALT_INV_Add8~81_sumout\,
	datac => \inst21|ALT_INV_TMPNEG1~q\,
	datad => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[11]~20_combout\);

-- Location: LABCELL_X85_Y39_N27
\inst2|ADD[10]~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[10]~21_combout\ = ( \inst21|TMPNEG1~q\ & ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~85_sumout\ ) ) ) # ( !\inst21|TMPNEG1~q\ & ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~85_sumout\ ) ) ) # ( \inst21|TMPNEG1~q\ & ( !\inst21|TMPNEG0~q\ & ( 
-- \inst2|Add8~85_sumout\ ) ) ) # ( !\inst21|TMPNEG1~q\ & ( !\inst21|TMPNEG0~q\ & ( \inst2|SUM\(10) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add8~85_sumout\,
	datac => \inst2|ALT_INV_SUM\(10),
	datae => \inst21|ALT_INV_TMPNEG1~q\,
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[10]~21_combout\);

-- Location: LABCELL_X83_Y37_N33
\inst2|ADD[9]~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[9]~22_combout\ = (!\inst21|TMPNEG0~q\ & ((!\inst21|TMPNEG1~q\ & ((\inst2|SUM\(9)))) # (\inst21|TMPNEG1~q\ & (\inst2|Add8~89_sumout\)))) # (\inst21|TMPNEG0~q\ & (((\inst2|Add8~89_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011110001111000001111000111100000111100011110000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_Add8~89_sumout\,
	datad => \inst2|ALT_INV_SUM\(9),
	combout => \inst2|ADD[9]~22_combout\);

-- Location: LABCELL_X83_Y37_N24
\inst2|ADD[8]~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[8]~23_combout\ = ( \inst2|SUM\(8) & ( ((!\inst21|TMPNEG0~q\ & !\inst21|TMPNEG1~q\)) # (\inst2|Add8~93_sumout\) ) ) # ( !\inst2|SUM\(8) & ( (\inst2|Add8~93_sumout\ & ((\inst21|TMPNEG1~q\) # (\inst21|TMPNEG0~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011110001000111111111000100011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datad => \inst2|ALT_INV_Add8~93_sumout\,
	dataf => \inst2|ALT_INV_SUM\(8),
	combout => \inst2|ADD[8]~23_combout\);

-- Location: LABCELL_X83_Y37_N27
\inst2|ADD[7]~24\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[7]~24_combout\ = ( \inst2|SUM\(7) & ( ((!\inst21|TMPNEG0~q\ & !\inst21|TMPNEG1~q\)) # (\inst2|Add8~97_sumout\) ) ) # ( !\inst2|SUM\(7) & ( (\inst2|Add8~97_sumout\ & ((\inst21|TMPNEG1~q\) # (\inst21|TMPNEG0~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011100000111000001110000011110001111100011111000111110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_Add8~97_sumout\,
	dataf => \inst2|ALT_INV_SUM\(7),
	combout => \inst2|ADD[7]~24_combout\);

-- Location: LABCELL_X83_Y39_N30
\inst2|ADD[6]~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[6]~25_combout\ = ( \inst2|Add8~101_sumout\ & ( ((\inst2|SUM\(6)) # (\inst21|TMPNEG1~q\)) # (\inst21|TMPNEG0~q\) ) ) # ( !\inst2|Add8~101_sumout\ & ( (!\inst21|TMPNEG0~q\ & (!\inst21|TMPNEG1~q\ & \inst2|SUM\(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100001111111011111110111111101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datac => \inst2|ALT_INV_SUM\(6),
	dataf => \inst2|ALT_INV_Add8~101_sumout\,
	combout => \inst2|ADD[6]~25_combout\);

-- Location: LABCELL_X83_Y39_N33
\inst2|ADD[5]~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[5]~26_combout\ = ( \inst2|SUM\(5) & ( ((!\inst21|TMPNEG0~q\ & !\inst21|TMPNEG1~q\)) # (\inst2|Add8~105_sumout\) ) ) # ( !\inst2|SUM\(5) & ( (\inst2|Add8~105_sumout\ & ((\inst21|TMPNEG1~q\) # (\inst21|TMPNEG0~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011110001000111111111000100011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst21|ALT_INV_TMPNEG1~q\,
	datad => \inst2|ALT_INV_Add8~105_sumout\,
	dataf => \inst2|ALT_INV_SUM\(5),
	combout => \inst2|ADD[5]~26_combout\);

-- Location: LABCELL_X85_Y38_N57
\inst2|ADD[4]~27\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[4]~27_combout\ = ( \inst2|SUM\(4) & ( ((!\inst21|TMPNEG0~q\ & !\inst21|TMPNEG1~q\)) # (\inst2|Add8~109_sumout\) ) ) # ( !\inst2|SUM\(4) & ( (\inst2|Add8~109_sumout\ & ((\inst21|TMPNEG1~q\) # (\inst21|TMPNEG0~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010101010101000001010101010111110101010101011111010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|ALT_INV_Add8~109_sumout\,
	datac => \inst21|ALT_INV_TMPNEG0~q\,
	datad => \inst21|ALT_INV_TMPNEG1~q\,
	dataf => \inst2|ALT_INV_SUM\(4),
	combout => \inst2|ADD[4]~27_combout\);

-- Location: LABCELL_X88_Y35_N36
\inst2|ADD[3]~28\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[3]~28_combout\ = (!\inst21|TMPNEG1~q\ & ((!\inst21|TMPNEG0~q\ & (\inst2|SUM\(3))) # (\inst21|TMPNEG0~q\ & ((\inst2|Add8~113_sumout\))))) # (\inst21|TMPNEG1~q\ & (((\inst2|Add8~113_sumout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000001111111001000000111111100100000011111110010000001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst2|ALT_INV_SUM\(3),
	datac => \inst21|ALT_INV_TMPNEG0~q\,
	datad => \inst2|ALT_INV_Add8~113_sumout\,
	combout => \inst2|ADD[3]~28_combout\);

-- Location: MLABCELL_X87_Y38_N57
\inst2|ADD[2]~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[2]~29_combout\ = ( \inst21|TMPNEG1~q\ & ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~117_sumout\ ) ) ) # ( !\inst21|TMPNEG1~q\ & ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~117_sumout\ ) ) ) # ( \inst21|TMPNEG1~q\ & ( !\inst21|TMPNEG0~q\ & ( 
-- \inst2|Add8~117_sumout\ ) ) ) # ( !\inst21|TMPNEG1~q\ & ( !\inst21|TMPNEG0~q\ & ( \inst2|SUM\(2) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst2|ALT_INV_SUM\(2),
	datac => \inst2|ALT_INV_Add8~117_sumout\,
	datae => \inst21|ALT_INV_TMPNEG1~q\,
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[2]~29_combout\);

-- Location: MLABCELL_X87_Y38_N12
\inst2|ADD[1]~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[1]~30_combout\ = ( \inst21|TMPNEG1~q\ & ( \inst2|Add8~121_sumout\ ) ) # ( !\inst21|TMPNEG1~q\ & ( (!\inst21|TMPNEG0~q\ & ((\inst2|SUM\(1)))) # (\inst21|TMPNEG0~q\ & (\inst2|Add8~121_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001101100011011000110110001101100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG0~q\,
	datab => \inst2|ALT_INV_Add8~121_sumout\,
	datac => \inst2|ALT_INV_SUM\(1),
	dataf => \inst21|ALT_INV_TMPNEG1~q\,
	combout => \inst2|ADD[1]~30_combout\);

-- Location: LABCELL_X88_Y35_N27
\inst2|ADD[0]~31\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|ADD[0]~31_combout\ = ( \inst21|TMPNEG0~q\ & ( \inst2|Add8~125_sumout\ ) ) # ( !\inst21|TMPNEG0~q\ & ( (!\inst21|TMPNEG1~q\ & ((\inst2|SUM\(0)))) # (\inst21|TMPNEG1~q\ & (\inst2|Add8~125_sumout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001101100011011000110110001101100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|ALT_INV_TMPNEG1~q\,
	datab => \inst2|ALT_INV_Add8~125_sumout\,
	datac => \inst2|ALT_INV_SUM\(0),
	dataf => \inst21|ALT_INV_TMPNEG0~q\,
	combout => \inst2|ADD[0]~31_combout\);

-- Location: LABCELL_X77_Y7_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


