Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--Propagation delay measurement logic
 ENTITY N_AND IS
 generic(N: integer :=32);--Number of bits of the detector
 PORT (	A: IN std_logic_vector(N-1 downto 0);
			B: IN std_logic_vector(N-1 downto 0);
			O: OUT std_logic_vector(N-1 downto 0)
 );
 END ENTITY;

ARCHITECTURE MULTIAND OF N_AND IS
 BEGIN

MULTOP : FOR i IN N-2 DOWNTO 0 GENERATE
    O(i) <= A(i) AND B(i);
END GENERATE; 

END ARCHITECTURE;